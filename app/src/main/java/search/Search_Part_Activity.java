package search;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnDismissListener;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.mobileordering.R;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import addcart.CartDAO;
import addcart.CartDTO;
import alertbox.Alertbox;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import productfamily.ProductFamilyItemAdapter;
import quickorder.Quick_Order_Preview_Activity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;


public class  Search_Part_Activity extends Activity {
    private List<String> Description;
    private List<String> Flag_List;
    private String INPUT_DB_PATH;
    private List<String> Image_List;
    private List<String> PartNoList;
    private String ProductName;
    private ArrayAdapter<String> adapter;
    private FloatingActionButton arrleft;
    private FloatingActionButton arrright;
    private ImageView backImageView;
    private LinearLayout flagtext;
    private Button go;
    private HorizontalScrollView hsv;
    private String item;
    private ProgressDialog loadDialog;
    private List<String> partListList;
    private ListView partListListView;
    private ProductFamilyItemAdapter productFamilyItemAdapter;
    private EditText partNo;
    private List<Bitmap> vehicleImage;
    private List<String> vehicleNameList2;
    private ArrayList<Bitmap> productbitmap;
    private Bitmap bitmap;
    private List<String> vehicleIDList, productIDList;
    private Alertbox alert = new Alertbox(Search_Part_Activity.this);
    private ImageView Cart_Icon;
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private String UserType;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search__part_);

        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();

        hsv = (HorizontalScrollView) findViewById(R.id.horilist);
        arrleft = (FloatingActionButton) findViewById(R.id.left);
        arrright = (FloatingActionButton) findViewById(R.id.right);
        productbitmap = new ArrayList<Bitmap>();
        flagtext = (LinearLayout) findViewById(R.id.flagtxt);
        vehicleImage = new ArrayList<Bitmap>();
        partNo = (EditText) findViewById(R.id.textato);
        Cart_Icon = (ImageView) findViewById(R.id.cart_icon);

        UserType = myshare.getString("usertype", "").toString();
        if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")||UserType.equals("ASC")))
            Cart_Icon.setVisibility(View.VISIBLE);
        else
            Cart_Icon.setVisibility(View.GONE);


        Cart_Icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                CartDAO cartDAO = new CartDAO(getApplicationContext());
                CartDTO cartDTO = cartDAO.GetCartItems();
                if(cartDTO.getPartCodeList() == null)
                {
                    StyleableToast st =
                            new StyleableToast(getApplicationContext(), "Cart is Empty !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else
                {
                    startActivity(new Intent(getApplicationContext(), Quick_Order_Preview_Activity.class).putExtra("from","CartItem"));
                }
                //  startActivity(new Intent(ProductFamilyActivity.this, Cart_Activity.class));
            }
        });
        arrright.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                hsv.smoothScrollTo(hsv.getScrollX() + 1000, hsv.getScrollY());
                Log.v("inside image click", "yeah");
                arrright.setVisibility(View.GONE);
                arrleft.setVisibility(View.VISIBLE);
            }
        });
        arrleft.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                hsv.smoothScrollTo(hsv.getScrollX() - 1000, hsv.getScrollY());
                arrleft.setVisibility(View.GONE);
                arrright.setVisibility(View.VISIBLE);
            }
        });


        go = (Button) findViewById(R.id.go);
        go.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                item = partNo.getText().toString();
                Log.v("on text Changed", item);
                if (item.length() >= 3) {
                    new RetrieveFileAsynforselect().execute();
                } else {
                    partNo.setError("Minimum 3 letters required");
                }
            }
        });
        partListListView = (ListView) findViewById(R.id.partlist_listView);
        backImageView = (ImageView) findViewById(R.id.back);

        backImageView.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {
                onBackPressed();
            }
        });


        final ImageView menu = (ImageView) findViewById(R.id.menu);
        menu.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(Search_Part_Activity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(Search_Part_Activity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(Search_Part_Activity.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(Search_Part_Activity.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(Search_Part_Activity.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(Search_Part_Activity.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(Search_Part_Activity.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(Search_Part_Activity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(Search_Part_Activity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(Search_Part_Activity.this, PriceListActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(Search_Part_Activity.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });


    }


    class RetrieveFileAsynforselect extends AsyncTask<Void, Void, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            loadDialog = new ProgressDialog(Search_Part_Activity.this);
            loadDialog.setMessage("Loading...");
            loadDialog.setProgressStyle(0);
            loadDialog.setCancelable(false);
            // loadDialog.show();
        }

        protected String doInBackground(Void... arg0) {
            SearchDAO searchDAO = new SearchDAO(Search_Part_Activity.this);
            SearchDTO searchDTO = new SearchDTO();
            if (item.isEmpty()) {
                return "notxt";
            }
            searchDTO = searchDAO.retriveForVehicleForshowAll(item);
            vehicleNameList2 = searchDTO.getVehicleName();
            Description = searchDTO.getDescription();
            partListList = searchDTO.getPartno_List();
            Image_List = searchDTO.getService_List();
            ProductName = searchDTO.getProductNameList();
            Flag_List = searchDTO.getFlag();
            vehicleIDList = searchDTO.getVehicleID();
            productIDList = searchDTO.getProductIDList();
            if (vehicleNameList2.isEmpty()) {
                return "Empty";
            }
            return "success";
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            loadDialog.dismiss();
            if (result.equals("Empty")) {
                flagtext.setVisibility(View.GONE);
                alert.showAlertbox("No data found !");

                if (productFamilyItemAdapter != null) {
                    productFamilyItemAdapter.clear();
                    partListListView.setAdapter(productFamilyItemAdapter);
                }
            } else if (result.equals("notxt")) {
                productFamilyItemAdapter.clear();
                partListListView.setAdapter(productFamilyItemAdapter);
            } else {
                if (!Flag_List.isEmpty()) {
                    flagtext.setVisibility(View.VISIBLE);
                }

                productFamilyItemAdapter = new ProductFamilyItemAdapter(Search_Part_Activity.this, vehicleIDList, vehicleNameList2, Description, partListList, ProductName, Flag_List, Image_List, productIDList);
                partListListView.setAdapter(productFamilyItemAdapter);
                arrright.setVisibility(View.VISIBLE);

            }
        }
    }


    private Bitmap downloadImage(String url) {

        try {
            InputStream fileInputStream = getAssets().open(url);

            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inSampleSize = 5;
            bitmap = BitmapFactory.decodeStream(fileInputStream, null, bmOptions);
            fileInputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
            Log.v("Error", e.getMessage());
            return downloadImage("noimagefound");

        }
        return bitmap;
    }

    private void showAlert(String string) {
        // TODO Auto-generated method stub

        alert.showAlertbox(string);
    }

}
