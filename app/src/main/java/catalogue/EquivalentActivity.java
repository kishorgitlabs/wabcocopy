package catalogue;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;

import com.mobileordering.R;

import java.util.ArrayList;
import java.util.List;

import adapter.EquivalentAdapter;
import alertbox.Alertbox;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import pricelist.PriceList;
import productfamily.ProductFamilyActivity;
import search.SearchActivity;
import search.SearchDAO;
import search.SearchDTO;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;

public class EquivalentActivity extends AppCompatActivity {

    private Button mSearch;
    private EditText mEquivalent_part;
    private Alertbox box = new Alertbox(EquivalentActivity.this);
    private ListView mListview;
    private List<String> originalList,equivalentList;
    private ImageView backImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equivalent);

        mSearch = (Button) findViewById(R.id.search);
        mEquivalent_part = (EditText) findViewById(R.id.equivalent_part);
        mListview =(ListView) findViewById(R.id.listview);
        backImageView = (ImageView) findViewById(R.id.back);

        equivalentList = new ArrayList<String>();
        originalList = new ArrayList<String>();



        backImageView.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                onBackPressed();
            }
        });

        final ImageView menu = (ImageView) findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                @SuppressLint("RestrictedApi") Context wrapper = new ContextThemeWrapper(EquivalentActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(EquivalentActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(EquivalentActivity.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(EquivalentActivity.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(EquivalentActivity.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(EquivalentActivity.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(EquivalentActivity.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(EquivalentActivity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(EquivalentActivity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(EquivalentActivity.this, PriceList.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(EquivalentActivity.this);
                                update.checkVersion();
                                break;
                            case R.id.equivalent:
                                startActivity(new Intent(EquivalentActivity.this, EquivalentActivity.class));
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });


        mSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (mEquivalent_part.getText().length() < 3) {
                    box.showAlertbox("Minimum 3 numbers required");
                } else {
                    //searchWord = partword.getText().toString();
                    new RetriveEquivalentPart().execute();
                }

            }
        });


    }


    class RetriveEquivalentPart extends AsyncTask<String, Void, String> {


        private ProgressDialog progerss;

        protected void onPreExecute() {
            super.onPreExecute();

            progerss = ProgressDialog.show(EquivalentActivity.this, "Loading", "Please wait", false, false);



            Log.v("Calling ", "Equivalet kit searchpart");
        }

        protected String doInBackground(String... from) {
            SearchDAO searchDAO = new SearchDAO(EquivalentActivity.this);
            SearchDTO searchDTO = new SearchDTO();
            searchDTO = searchDAO.retriveForEquivalentPart(mEquivalent_part.getText().toString());
            originalList = searchDTO.getoriginalList();
            equivalentList = searchDTO.getequivalentList();


            if (originalList.isEmpty()) {
                return "Empty";
            }
            return "success";
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progerss.dismiss();
            if (result.equals("Empty")) {
                box.showAlertbox("No data found !");

                EquivalentAdapter equivaletAdapter = new EquivalentAdapter(EquivalentActivity.this, originalList, equivalentList);
                mListview.setAdapter(equivaletAdapter);


            } else {
                EquivalentAdapter equivaletAdapter = new EquivalentAdapter(EquivalentActivity.this, originalList, equivalentList);
                mListview.setAdapter(equivaletAdapter);

            }

        }
    }

}
