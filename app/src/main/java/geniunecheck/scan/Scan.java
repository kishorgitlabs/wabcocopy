package geniunecheck.scan;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

//import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeSuccessDialog;
//import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
//import com.shashank.sony.fancydialoglib.Animation;
//import com.shashank.sony.fancydialoglib.FancyAlertDialog;
//import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
//import com.shashank.sony.fancydialoglib.Icon;
import com.mobileordering.R;

import java.util.ArrayList;
import java.util.List;

import alertbox.Alertbox;
import api.APIService;
import geniunecheck.activities.Result;
import geniunecheck.activities.Wabcoemployee;
import geniunecheck.retrofit.RetroClient;
import geniunecheck.retrofit.Retrofitclient2;
import models.genunecheck.CheckGenuine;
import models.genunecheck.CheckUnique;
import models.genunecheck.GenuineLoglist;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Scan extends AppCompatActivity {
    SharedPreferences sharedPreferences;

    RadioGroup rg_check;
    RadioButton rb_pol;
    int selectedId;
    String policies = "", id;
    private TextView textViewName;
    String Serialid = "", UserType = "", ScannedBy = "", Mobile = "", Address = "", customername = "";
    TextView et_uniqueid;
    List<GenuineLoglist> genhist;
    int dcount = 0, gmcount = 0, focount = 0, wscount = 0;
    String Uniqueid, pickid;
    Button submit;
    TextView tv_slid;
    private Alertbox box = new Alertbox(Scan.this);
    private CheckUnique checkUnique;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_scan);
            rg_check = (RadioGroup) findViewById(R.id.scan_rg);
            textViewName = (TextView) findViewById(R.id.textViewName);
            tv_slid = (TextView) findViewById(R.id.edt_serlno);
            et_uniqueid = (TextView) findViewById(R.id.uniqueid);
            submit = (Button) findViewById(R.id.submit);
            Intent i = getIntent();
            Serialid = i.getStringExtra("Serial id");
            tv_slid.setText(Serialid);
            sharedPreferences = this.getSharedPreferences("registration", Context.MODE_PRIVATE);
            UserType = sharedPreferences.getString("Usertype", " ");
            ScannedBy = sharedPreferences.getString("Name", "");
            Mobile = sharedPreferences.getString("Mobile", "");
            id = sharedPreferences.getString("id", "");
            genhist = new ArrayList<>();
            submit.setVisibility(View.GONE);
            Uniqueid = et_uniqueid.getText().toString().trim();

            final ProgressDialog loading =
                    ProgressDialog.show(this, "Loading", "Please wait...", false, false);

            APIService service = Retrofitclient2.getApiService();
            Call<CheckUnique> call = service.check_unique(Serialid);
            call.enqueue(new Callback<CheckUnique>() {
                @Override
                public void onResponse(Call<CheckUnique> call, Response<CheckUnique> response) {
                    loading.dismiss();
                    if (response.body() != null) {
                        if (response.body().getResult().equals("success")) {
                            checkUnique = response.body();
                            et_uniqueid.setText(response.body().getData().getRandomno() + "m");
                            pickid = response.body().getData().getPickid();
                            Address = response.body().getData().getTransaddress();
                            customername = response.body().getData().getCusname();

                            if (!response.body().getData().getRandomno().equals("")) {
                                submit.setVisibility(View.VISIBLE);
                            } else {
                                submit.setVisibility(View.GONE);
                                SaveScannedProduct();
                            }

                           /* if (response.body().getData().getPickid().equals("")
                                    || response.body().getData().getPickid() == null) {
                                showAlert(" Note: This part number is not Invoiced");
                            }
*/

                        } else if (response.body().getResult().equals("Not found")) {
                            SaveScannedProduct();
                        } else {
                            box.showNegativebox(getString(R.string.server_error));
                        }
                    } else {
                        box.showNegativebox(getString(R.string.server_error));
                    }
                }

                @Override
                public void onFailure(Call<CheckUnique> call, Throwable t) {
                    loading.dismiss();
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void SaveScannedProduct() {

        final ProgressDialog loading =
                ProgressDialog.show(this, "Saving", "Please wait...", false, false);


        APIService service = RetroClient.getApiService();
        Call<CheckGenuine> call1 = service.check_genuine("NotFound", Serialid, UserType, id, pickid, customername, Address, ScannedBy);
        call1.enqueue(new Callback<CheckGenuine>() {
            @Override
            public void onResponse(Call<CheckGenuine> call, Response<CheckGenuine> response) {
                loading.dismiss();
                try {

                    LayoutInflater inflater = getLayoutInflater();
                    View alertLayout = inflater.inflate(R.layout.custom_genuine_layout, null);
                    final TextView Msg_txtview = alertLayout.findViewById(R.id.al_msg);
                    final TextView Sl_txtview = alertLayout.findViewById(R.id.sl_no);

                    Msg_txtview.setText("Unique Id not found!");
                    Sl_txtview.setText(Serialid);
                    final AlertDialog.Builder alertDialog = new AlertDialog.Builder(Scan.this);
                    alertDialog.setTitle("WABCO Geniune");
                    alertDialog.setView(alertLayout);
                    alertDialog.setCancelable(false);
                    alertDialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                            onBackPressed();
                        }
                    });

                    alertDialog.show();

                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(Scan.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CheckGenuine> call, Throwable t) {
                try {
                    //  Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                    Toast.makeText(Scan.this, getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    public void submit(View view) {
        Uniqueid = et_uniqueid.getText().toString().trim();
        final ProgressDialog progressDialog =
                ProgressDialog.show(this, "Loading", "Please wait...", false, false);

        if (!Uniqueid.equals("") && !Uniqueid.equals("Records not Found")) {
            try {
                selectedId = rg_check.getCheckedRadioButtonId();
                rb_pol = (RadioButton) findViewById(selectedId);

                if (rb_pol != null)
                    policies = rb_pol.getText().toString().trim();

                if ((policies).equals("Yes")) {
                    try {

                        APIService service = RetroClient.getApiService();
                        Call<CheckGenuine> call = service.check_genuine(Uniqueid, Serialid, UserType, id, pickid, customername, Address, ScannedBy);
                        call.enqueue(new Callback<CheckGenuine>() {
                            @Override
                            public void onResponse(Call<CheckGenuine> call, Response<CheckGenuine> response) {
                                try {

                                    int c = genhist.size();
                                    String count = String.valueOf(c);
                                    if (response.body().getResult().equals("Geniune")) {
                                        et_uniqueid.setText("");
                                        Intent i = new Intent(Scan.this, Result.class);
                                        i.putExtra("Result", "Geniune");
                                        i.putExtra("Serialid", Serialid);
                                        i.putExtra("Uniqueid", Uniqueid);
                                        i.putExtra("picid", pickid);
                                        startActivity(i);
                                        progressDialog.dismiss();

                                    } else if (response.body().getResult().equals("Suspecious")) {

                                        genhist = response.body().getData();

                                        if (genhist.size() != 0) {
                                            for (int i = 0; i < genhist.size(); i++) {
                                                String usertype = genhist.get(i).getUserType();

                                                if (usertype.equals("Dealer")) {
                                                    dcount++;
                                                }
                                                if (usertype.equals("Garage Mechanic")) {
                                                    gmcount++;
                                                }
                                                if (usertype.equals("Fleet Operator")) {
                                                    focount++;
                                                }
                                                if (usertype.equals("Whole Sale Distributor")) {
                                                    wscount++;
                                                }
                                            }
                                        }
                                        String DLcount = String.valueOf(dcount);
                                        String GMcount = String.valueOf(gmcount);
                                        String FLOcount = String.valueOf(focount);
                                        String WScount = String.valueOf(wscount);
                                        Intent i = new Intent(Scan.this, Result.class);
                                        i.putExtra("Result", "Suspecious");
                                        i.putExtra("count", count);
                                        i.putExtra("DLcount", DLcount);
                                        i.putExtra("GMcount", GMcount);
                                        i.putExtra("FLOcount", FLOcount);
                                        i.putExtra("WScount", WScount);
                                        i.putExtra("Serialid", Serialid);
                                        i.putExtra("Uniqueid", Uniqueid);
                                        startActivity(i);
                                        et_uniqueid.setText("");
                                        genhist = response.body().getData();
                                        progressDialog.dismiss();

                                    } else if (response.body().getResult().equals("Highly Suscpcious")) {
                                        genhist = response.body().getData();

                                        if (genhist.size() != 0) {
                                            for (int i = 0; i < genhist.size(); i++) {
                                                String usertype = genhist.get(i).getUserType();

                                                if (usertype.equals("Dealer")) {
                                                    dcount++;
                                                }
                                                if (usertype.equals("Garage Mechanic")) {
                                                    gmcount++;
                                                }
                                                if (usertype.equals("Fleet Operator")) {
                                                    focount++;
                                                }
                                                if (usertype.equals("Whole Sale Distributor")) {
                                                    wscount++;
                                                }
                                            }
                                        }
                                        String DLcount = String.valueOf(dcount);
                                        String GMcount = String.valueOf(gmcount);
                                        String FLOcount = String.valueOf(focount);
                                        String WScount = String.valueOf(wscount);
                                        Intent i = new Intent(Scan.this, Result.class);
                                        i.putExtra("Result", "Highly Suscpcious");
                                        i.putExtra("count", count);
                                        i.putExtra("DLcount", DLcount);
                                        i.putExtra("GMcount", GMcount);
                                        i.putExtra("FLOcount", FLOcount);
                                        i.putExtra("WScount", WScount);
                                        i.putExtra("Serialid", Serialid);
                                        i.putExtra("Uniqueid", Uniqueid);
                                        startActivity(i);
                                        et_uniqueid.setText("");
                                        progressDialog.dismiss();
                                    } else if (response.body().getResult().equals("Already Scanned by You")) {
                                        genhist = response.body().getData();

                                        if (genhist.size() != 0) {
                                            for (int i = 0; i < genhist.size(); i++) {
                                                String usertype = genhist.get(i).getUserType();

                                                if (usertype.equals("Dealer")) {
                                                    dcount++;
                                                }
                                                if (usertype.equals("Garage Mechanic")) {
                                                    gmcount++;
                                                }
                                                if (usertype.equals("Fleet Operator")) {
                                                    focount++;
                                                }
                                                if (usertype.equals("Whole Sale Distributor")) {
                                                    wscount++;
                                                }
                                            }
                                        }
                                        String DLcount = String.valueOf(dcount);
                                        String GMcount = String.valueOf(gmcount);
                                        String FLOcount = String.valueOf(focount);
                                        String WScount = String.valueOf(wscount);
                                        Intent i = new Intent(Scan.this, Result.class);
                                        i.putExtra("Result", "Already Scanned by You");
                                        i.putExtra("count", count);
                                        i.putExtra("DLcount", DLcount);
                                        i.putExtra("GMcount", GMcount);
                                        i.putExtra("FLOcount", FLOcount);
                                        i.putExtra("WScount", WScount);
                                        i.putExtra("Serialid", Serialid);
                                        i.putExtra("Uniqueid", Uniqueid);
                                        startActivity(i);
                                        genhist = response.body().getData();
                                        et_uniqueid.setText("");
                                        progressDialog.dismiss();

                                    } else if (response.body().getResult().equals("Wabco employee") || response.body().getResult().equals("FSR")) {
                                        genhist = response.body().getData();

                                        ArrayList<GenuineLoglist> arrlist = new ArrayList<GenuineLoglist>(genhist);
                                        Intent i = new Intent(Scan.this, Wabcoemployee.class);
                                        i.putExtra("MyClass", arrlist);
                                        i.putExtra("slid", Serialid);
                                        i.putExtra("usrnm", ScannedBy);
                                        i.putExtra("uniqid", et_uniqueid.getText().toString().trim());
                                        i.putExtra("usraddress", Address);
                                        i.putExtra("cusname", customername);
                                        i.putExtra("pickname", pickid);
                                        i.putExtra("Serialid", Serialid);
                                        i.putExtra("Uniqueid", Uniqueid);
                                        startActivity(i);
                                        et_uniqueid.setText("");
                                        progressDialog.dismiss();
                                    } else {
                                        Toast.makeText(Scan.this, "Unique Id not found!!!", Toast.LENGTH_SHORT).show();

                                        progressDialog.dismiss();
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Toast.makeText(Scan.this, "Unique Id not found!!!", Toast.LENGTH_SHORT).show();
                                    progressDialog.dismiss();
                                }
                            }

                            @Override
                            public void onFailure(Call<CheckGenuine> call, Throwable t) {

                                try {
                                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                                    progressDialog.dismiss();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if ((policies).equals("No")) {

                    Intent i = new Intent(Scan.this, Result.class);
                    i.putExtra("Result", "No");
                    startActivity(i);
                    progressDialog.dismiss();

                } else {
                    Toast.makeText(Scan.this, "Please select your choice", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();

                }

            } catch (Exception e) {
                progressDialog.dismiss();
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "Please Check Your Unique ID", Toast.LENGTH_SHORT).show();
        }
    }


//    public void showAlert(String msg) {
//     /*   new FancyAlertDialog.Builder(this)
//                .setTitle("Success")
//                .setBackgroundColor(Color.parseColor("#303F9F"))  //Don't pass R.color.colorvalue
//                .setMessage("The Part Number Is Not Invoiced")
//                .setPositiveBtnBackground(Color.parseColor("#FF4081"))  //Don't pass R.color.colorvalue
//                .setPositiveBtnText("Okay")
//               //Don't pass R.color.colorvalue
//                .setAnimation(Animation.SIDE)
//               // .isCancellable(true)
//                .setIcon(R.drawable.checked, Icon.Visible)
//                .OnPositiveClicked(new FancyAlertDialogListener() {
//                    @Override
//                    public void OnClick() {
//                        Toast.makeText(getApplicationContext(),"Okay",Toast.LENGTH_SHORT).show();
//                    }
//
//                })
//                .OnNegativeClicked(new FancyAlertDialogListener() {
//                    @Override
//                    public void OnClick() {
//                        Toast.makeText(getApplicationContext(), "Cancel", Toast.LENGTH_SHORT).cancel();
//                    }
//
//                })
//                .build();*/
//
//        new AwesomeSuccessDialog(this)
//                .setTitle("Wabco Genuine")
//                .setMessage("Note: This Part Number Is not Invoiced")
//                .setColoredCircle(R.color.cpb_blue_dark)
//                .setDialogIconAndColor(R.drawable.checked,R.color.white)
//                .setCancelable(true)
//                .setPositiveButtonText(getString(R.string.dialog_yes_button))
//                .setPositiveButtonbackgroundColor(R.color.cpb_blue_dark)
//                .setPositiveButtonTextColor(R.color.white)
//
//                .setPositiveButtonClick(new Closure() {
//                    @Override
//                    public void exec() {
//                        //click
//                    }
//                })
//
//                .show();
//       /*LayoutInflater inflater = getLayoutInflater();
//        View alertLayout = inflater.inflate(R.layout.custom_genuine_layout, null);
//        final TextView Msg_txtview = alertLayout.findViewById(R.id.al_msg);
//        final TextView Sl_txtview = alertLayout.findViewById(R.id.sl_no);
//        Msg_txtview.setText(msg);
//        Sl_txtview.setText(Serialid);
//        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(Scan.this);
//        alertDialog.setTitle("SUCCESS");
//        alertDialog.setTitle( Html.fromHtml("<font color='#FF7F27'>Set IP Address</font>"));
//        alertDialog.setView(alertLayout);
//        alertDialog.setCancelable(false);
//        alertDialog.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                // TODO Auto-generated method stub
//                dialog.dismiss();
//            }
//        });
//
//        alertDialog.show();
//
//
//    }*/

    }



