package geniunecheck.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.mobileordering.R;

import alertbox.Alertbox;
import geniunecheck.scan.Scan;
import home.MainActivity;


public class OriginalParts extends AppCompatActivity {
    //    private IntentIntegrator qrScan;
    final int REQ_CODE = 12;
    Barcode mBarcode;
    //qr code scanner objectwithoutScan
    private IntentIntegrator qrScan;
    private Alertbox box = new Alertbox(OriginalParts.this);
    private RadioButton mWithScan,mWithoutScan;
    private RadioGroup mScan_group;
    private String mSixDigit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_original_parts);
        qrScan = new IntentIntegrator(this);
        mScan_group = (RadioGroup) findViewById(R.id.scan_group);
        mWithScan = (RadioButton) findViewById(R.id.withScan);
        mWithoutScan = (RadioButton) findViewById(R.id.withoutScan);



    }


    public void scan(View view) {

        int selectedId = mScan_group.getCheckedRadioButtonId();

        // find which radioButton is checked by id
        if(selectedId == mWithScan.getId()) {

            qrScan.initiateScan();
            qrScan.setBeepEnabled(true);
            qrScan.setOrientationLocked(false);
            qrScan.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);

        }  else if(selectedId == mWithoutScan.getId()) {

            new MaterialDialog.Builder(OriginalParts.this).title("WABCO Geniune")
                    .content("Enter the last 3 to 5 digit unique ID without m")
                    .autoDismiss(false)
                    .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT)
                    .input("Enter number", "", new MaterialDialog.InputCallback() {
                        @Override
                        public void onInput(MaterialDialog dialog, CharSequence input) {
                            // Do something
                            mSixDigit = input.toString();
                        }
                    }).positiveText("Search").negativeText("Cancel")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            if (dialog.getInputEditText().getText().length() >=3 ) {

                                mSixDigit = dialog.getInputEditText().getText().toString();
                               // mSixDigit = "350024046-C14F18-1-208";
                                dialog.dismiss();
                                Intent a = new Intent(OriginalParts.this, Serial_List_Activity.class);
                                a.putExtra("mSixDigit", mSixDigit);
                                startActivity(a);

                            } else {
                                StyleableToast st = new StyleableToast(OriginalParts.this,
                                        "Please Enter atleast last 3 digit number  unique ID without m", Toast.LENGTH_SHORT);
                                st.setBackgroundColor(getResources().getColor(R.color.red));
                                st.setTextColor(Color.WHITE);
                                st.setMaxAlpha();
                                st.show();
                            }


                        }
                    }).onNegative(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    dialog.dismiss();
                }
            }).show();

        }
        else
        {
            StyleableToast st = new StyleableToast(OriginalParts.this,
                    "Select any scanning option!", Toast.LENGTH_SHORT);
            st.setBackgroundColor(getResources().getColor(R.color.red));
            st.setTextColor(Color.WHITE);
            st.setMaxAlpha();
            st.show();
        }

       /* Intent a = new Intent(OriginalParts.this, Scan.class);
        a.putExtra("Serial id", "350024072-A10D17-1-1116");
        startActivity(a);*/



    }



    @Override
    public void onBackPressed() {
        Intent i = new Intent(OriginalParts.this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);

    }


    //Getting the scan results
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            //if qrcode has nothing in it

            if (result.getContents() == null) {
                Toast.makeText(this, "Scanning failed try again!", Toast.LENGTH_LONG).show();
            }
            else if( result.getContents().toString().split("-").length!=4)
            {
                LayoutInflater inflater = getLayoutInflater();
                View alertLayout = inflater.inflate(R.layout.custom_genuine_layout, null);
                final TextView Msg_txtview = alertLayout.findViewById(R.id.al_msg);
                final TextView Sl_txtview = alertLayout.findViewById(R.id.sl_no);

                Msg_txtview.setText("Scanning failed. \nFound incorrect serial number!\nDo you want scan again ?");
                Sl_txtview.setText(result.getContents().toString());
                Sl_txtview.setTextColor(Color.RED);
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(OriginalParts.this);
                alertDialog.setTitle("WABCO Geniune");
                alertDialog.setCancelable(false);
                alertDialog.setView(alertLayout);
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        qrScan.initiateScan();
                        qrScan.setBeepEnabled(true);
                        qrScan.setOrientationLocked(false);
                        qrScan.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                    }
                });

                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();
                    }
                });
                alertDialog.show();

            }
            else {
                //if qr contains data
                Intent a = new Intent(OriginalParts.this, Scan.class);
                a.putExtra("Serial id", result.getContents().toString());
                startActivity(a);
                Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

}


