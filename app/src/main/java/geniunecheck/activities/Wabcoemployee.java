package geniunecheck.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mobileordering.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import api.APIService;
import geniunecheck.adapter.Wabcoemployeeadpt;
import geniunecheck.retrofit.RetroClient;
import models.genunecheck.GenuineLoglist;
import models.genunecheck.WabcoEmployee;
import models.genunecheck.Wabcoemployeelist;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Wabcoemployee extends AppCompatActivity {
    ArrayList<GenuineLoglist> myList;
    List<Wabcoemployeelist> wabcolist;
    String Serialid, Uniqid, ScannedBy;
    TextView tv_slid, tv_unid, tv_uname, tv_addres, tv_customername;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    Wabcoemployeeadpt adapter;
    private ArrayList<String> sino;
    int count = 1;
    HashMap<String, String> menuItems;
    ListView listView;
    ArrayList<HashMap<String, String>> Scanlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wabcoemployee);

        recyclerView = (RecyclerView) findViewById(R.id.card_recycler_view);
        tv_slid = (TextView) findViewById(R.id.edt_serlno);
        tv_unid = (TextView) findViewById(R.id.edt_uniqid);
        tv_uname = (TextView) findViewById(R.id.edt_username);
        tv_addres = (TextView) findViewById(R.id.edt_branch);
        tv_customername = (TextView) findViewById(R.id.edt_cusname);
        listView = (ListView) findViewById(R.id.gridlist);
        sino = new ArrayList<>();
        wabcolist= new ArrayList<>();
        menuItems = new HashMap<String, String>();
        Scanlist = new ArrayList<>();
        Intent i = getIntent();
        Serialid = i.getStringExtra("slid");
        Uniqid = i.getStringExtra("uniqid");
        ScannedBy = i.getStringExtra("usrnm");
        tv_slid.setText(Serialid);
        tv_unid.setText(Uniqid);
        tv_uname.setText(ScannedBy);
        tv_customername.setText(i.getStringExtra("cusname"));
        tv_addres.setText(i.getStringExtra("usraddress"));
        myList = (ArrayList<GenuineLoglist>) getIntent().getSerializableExtra("MyClass");

        int twosame = 1;
        int onesame = 1;
        int sameless = 1;

        for (int i2 = 0; i2 < myList.size(); i2++) {
            for (int j = 1; j < myList.size(); j++) {
                if (myList.get(i2).getCity().equals(myList.get(j).getCity())) {
                    if (myList.get(i2).getType().equals(myList.get(j).getUserType())) {
                        menuItems.put("City", myList.get(i2).getCity());
                        menuItems.put("Usertype", myList.get(i2).getUserType());
                        menuItems.put("Username", myList.get(i2).getScannedBy());
                        menuItems.put("Count", String.valueOf(twosame++));
                    } else {
                        menuItems.put("City", myList.get(j).getCity());
                        menuItems.put("Usertype", myList.get(j).getUserType());
                        menuItems.put("Username", myList.get(i2).getScannedBy());
                        menuItems.put("Count", String.valueOf(onesame));
                    }
                } else {
                    menuItems.put("City", myList.get(j).getCity());
                    menuItems.put("Usertype", myList.get(j).getUserType());
                    menuItems.put("Username", myList.get(i2).getScannedBy());
                    menuItems.put("Count", String.valueOf(sameless));
                }
            }
            Scanlist.add(menuItems);
        }

        getdetails();
//        ListAdapter adapter = new SimpleAdapter(
//                Wabcoemployee.this, Scanlist,
//                R.layout.wabcoemployeeadapter, new String[]{"Usertype", "City", "Count"
//        }, new int[]{R.id.utype, R.id.date, R.id.type
//        });
//        listView.setAdapter(adapter);
    }

    public void getdetails() {
        try {
        final ProgressDialog progressDialog = new ProgressDialog(Wabcoemployee.this,
                R.style.Progress);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.show();

            APIService service = RetroClient.getApiService();
            Call<WabcoEmployee> call = service.wabcoemp(Uniqid, Serialid);
            call.enqueue(new Callback<WabcoEmployee>() {
                @Override
                public void onResponse(Call<WabcoEmployee> call, Response<WabcoEmployee> response) {
                    try {
                        if (response.body().getResult().equals("success")) {


                        wabcolist=response.body().getData();
                        if (wabcolist.size() > 0) {
                            for (int i1 = 1; i1 <= wabcolist.size(); i1++) {
                                String sio = String.valueOf(count);
                                sino.add(sio);
                                count++;
                            }
                        }
                        layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                        recyclerView.setLayoutManager(layoutManager);
                        adapter = new Wabcoemployeeadpt(getApplicationContext(), wabcolist, sino);
                        recyclerView.setAdapter(adapter);
                            progressDialog.dismiss();
                        } else {
                            Toast.makeText(Wabcoemployee.this, "Please try again!!!", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(Wabcoemployee.this, "Please try again!!!", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<WabcoEmployee> call, Throwable t) {


                    Toast.makeText(getApplicationContext(), "Please try again!!!", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();


        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(Wabcoemployee.this ,OriginalParts.class);
        startActivity(i);

    }
}