package geniunecheck.activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
/*import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;*/
import com.mobileordering.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import alertbox.Alertbox;
import api.APIService;
import api.ApiUtils;
import geniunecheck.retrofit.RetroClient;
import models.genunecheck.UpdateUser;
import models.genunecheck.UserData;
import models.quickorder.response.OrderResponse;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class GenuineCheck extends AppCompatActivity {
    ProgressDialog progressDialog;
    String Mobile;
    String Name, Usertype;
    EditText et_mob, et_name;
    String mOtp;
    Pinview et_otp;
    String strrr, Msg, id;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private MaterialSpinner usertype;
    private ArrayList<String> usertypeList;
    private String SelectedUser;
    private ProgressDialog loading;
    private Alertbox box = new Alertbox(GenuineCheck.this);
    TextView tv_note, tvhin_note;

    String note = "<font color= #4b4b4b>This OTP is required only for the first time genuine checking. \n" +
            "Please change the user type if required.</font> <font color=#dc002e>Once you change it, it cannot be modified further</font>";
    String hinnote = "<font color= #4b4b4b>यह OTP केवल पहली बार वास्तविक जाँच के लिए आवश्यक है कृपया आवश्यक होने पर उपयोगकर्ता का प्रकार बदलें</font> <font color=#dc002e>एक बार जब आप इसे बदलते हैं, तो इसे और भी संशोधित नहीं किया जा सकता है</font>";
    // private SmsVerifyCatcher smsVerifyCatcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_genuine_check);

        et_mob = (EditText) findViewById(R.id.phoneedit);
        et_name = (EditText) findViewById(R.id.nameedit);
        et_otp = (Pinview) findViewById(R.id.pinview);
        tv_note = (TextView) findViewById(R.id.engnote);
        tvhin_note = (TextView) findViewById(R.id.hinnote);
        tv_note.setText(Html.fromHtml(note));
        tvhin_note.setText(Html.fromHtml(hinnote));
        et_mob.setText(Mobile);
        et_name.setText(Name);
        progressDialog = new ProgressDialog(GenuineCheck.this);
        usertype = (MaterialSpinner) findViewById(R.id.usertypeedit);
        sharedPreferences = getSharedPreferences("registration", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        usertypeList = new ArrayList<String>();
        usertypeList.add("Select Usertype");

        usertypeList.add("Whole Sale Distributor");
        usertypeList.add("Dealer");
        usertypeList.add("OEM Dealer");
        usertypeList.add("Garage Mechanic");
        usertypeList.add("Fleet Operator");
        usertypeList.add("Wabco employee");
        usertypeList.add("FSR");

        usertype.setItems(usertypeList);
        // usertype.setSelectedIndex(2);
        usertype.setPadding(30, 0, 0, 0);
        usertype.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {

                SelectedUser = item.toString();
                Log.v("User", SelectedUser);
                // Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();
            }
        });


        // usertype.setSelectedIndex(sharedPreferences.getInt("userposition", 0));
        usertype.setText(sharedPreferences.getString("usertype", ""));
        usertype.setBackground(getResources().getDrawable(R.drawable.autotextback));
        et_mob.setText(sharedPreferences.getString("phone", ""));
        et_name.setText(sharedPreferences.getString("name", ""));
        id = sharedPreferences.getString("id", "");


        //init SmsVerifyCatcher
        /* smsVerifyCatcher = new SmsVerifyCatcher(GenuineCheck.this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                String code = parseCode(message);//Parse verification code
                 et_otp.setValue(code);//set code in edit text

                if(et_otp.getValue().length()==4)
                    CheckInternet();
                else   Toast.makeText(GenuineCheck.this, "Invalid OTP", Toast.LENGTH_SHORT).show();

                //then you can send verification code to server
            }
        });

        //set phone number filter if needed
       // smsVerifyCatcher.setFilter(sharedPreferences.getString("phone", ""));
        smsVerifyCatcher.setPhoneNumberFilter("BL-CCPIND");
*/


    }

    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{4}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }

    public void otp(View view) {
        try {
            Mobile = et_mob.getText().toString().trim();
            Name = et_name.getText().toString().trim();
            Usertype = usertype.getText().toString().trim();
            //  new SimpleOTPGenerator();
            mOtp = SimpleOTPGenerator.random(4);
            editor.putString("CurrentOTP", mOtp).commit();
            if (!mOtp.equals("")) {
                NetworkConnection isnet = new NetworkConnection(GenuineCheck.this);
                if (isnet.CheckInternet()) {
                    new sendOTPtoDatabase().execute();
                } else {
                    box.showAlertbox(getResources().getString(R.string.nointernetmsg));
                }

            }
//        Toast.makeText(this, "You Have Received OTP ", Toast.LENGTH_SHORT).show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void scaning(View view) {
        if (!et_otp.getValue().isEmpty()) {

            mOtp = sharedPreferences.getString("CurrentOTP", "");
            if (!(et_otp.getValue().toString().length() == 4)) {

                StyleableToast st = new StyleableToast(GenuineCheck.this,
                        "Invalid OTP", Toast.LENGTH_SHORT);
                st.setBackgroundColor(getResources().getColor(R.color.red));
                st.setTextColor(Color.WHITE);
                st.setMaxAlpha();
                st.show();
            } else if (!(et_otp.getValue().equals(mOtp))) {
                StyleableToast st = new StyleableToast(GenuineCheck.this,
                        "Invalid OTP", Toast.LENGTH_SHORT);
                st.setBackgroundColor(getResources().getColor(R.color.red));
                st.setTextColor(Color.WHITE);
                st.setMaxAlpha();
                st.show();

            } else {
                CheckInternet();
            }

        } else {
            StyleableToast st = new StyleableToast(GenuineCheck.this,
                    "Enter Your OTP", Toast.LENGTH_SHORT);
            st.setBackgroundColor(getResources().getColor(R.color.red));
            st.setTextColor(Color.WHITE);
            st.setMaxAlpha();
            st.show();
        }

    }

    private void CheckInternet() {
        // TODO Auto-generated method stub
        NetworkConnection isnet = new NetworkConnection(GenuineCheck.this);
        if (isnet.CheckInternet()) {
            UpdateUserInformation();
        } else {
            box.showAlertbox(getResources().getString(R.string.nointernetmsg));
        }
    }

    private void UpdateUserInformation() {

        try {

            loading = ProgressDialog.show(GenuineCheck.this, "WABCO", "Loading...", false, false);

            APIService service = RetroClient.getApiService();
            Call<UpdateUser> call = service.UpdateUser(id, Name, Mobile, mOtp, Usertype);
            call.enqueue(new Callback<UpdateUser>() {
                @Override
                public void onResponse(Call<UpdateUser> call, Response<UpdateUser> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("success")) {
                            SuccessUpdation(response.body().getData());
                        } else {
                            Toast.makeText(GenuineCheck.this, "Invalid OTP", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(GenuineCheck.this, "Please try again!!!", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<UpdateUser> call, Throwable t) {
                    loading.dismiss();
                    Toast.makeText(GenuineCheck.this, "Please try again!!!", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void SuccessUpdation(UserData userData) {

        Mobile = et_mob.getText().toString().trim();
        Name = et_name.getText().toString().trim();
        Usertype = usertype.getText().toString().trim();


        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(GenuineCheck.this);
        alertDialog.setMessage("OTP validation is successfull !");
        alertDialog.setTitle("WABCO");
        alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // TODO Auto-generated method stub

                dialog.dismiss();

                editor.putString("Mobile", Mobile);
                editor.putString("Name", Name);
                editor.putString("Usertype", Usertype);
                editor.putBoolean("isScanned", true);
                editor.apply();

                Intent mainIntent = new Intent(GenuineCheck.this, OriginalParts.class);
                mainIntent.putExtra("Mobile", Mobile);
                mainIntent.putExtra("Name", Name);
                mainIntent.putExtra("Usertype", Usertype);
                mainIntent.putExtra("ID", id);
                //  mainIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(mainIntent);


            }
        });
        alertDialog.show();


    }

    private class sendOTPtoDatabase extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
            progressDialog.setMessage("Loading...");
        }

        @Override
        protected String doInBackground(String... strings) {
            int i = 0;

            Msg = "Dear " + Name + ", Your OTP is " + mOtp + " Please enter this OTP to verify your mobile number.";

            try {
//                URL url = new URL("http://sms.brainmagic.info/sendunicodesms?uname=WABCO&pwd=wabco123&senderid=WINSMS&msg=" + Msg + "&to=" + Mobile + "&route=T");
//                URL url = new URL("http://sms.brainmagic.info/sendsms?uname=Z2VudWluZQ&pwd=Z2VudWluZUAxMjM&senderid=IHTWAB&to" + URLEncoder.encode(Mobile, "UTF-8") + "&msg=" + URLEncoder.encode(Msg, "UTF-8") + "&route=T&peid=1701159135033405399&tempid=0)");
//                Log.e("URL", url.toString());

//                strrr = "http://sms.brainmagic.info/sendsms?uname=Z2VudWluZQ&pwd=Z2VudWluZUAxMjM&senderid=IHTWAB&msg=" + URLEncoder.encode(Msg, "UTF-8") + "&to=" + URLEncoder.encode(Mobile, "UTF-8") + "&route=T";

                strrr = "http://sms.brainmagic.info/sendsms?uname=Z2VudWluZQ&pwd=Z2VudWluZUAxMjM&senderid=IHTWAB&to=" + URLEncoder.encode(Mobile, "UTF-8") + "&msg=" + URLEncoder.encode(Msg, "UTF-8") + "&route=T&peid=1701159135033405399&tempid=0";

                Log.v("URL", strrr);
                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet(strrr);
                HttpResponse response = client.execute(request);

                return "success";
            } catch (Exception e) {
                e.printStackTrace();
                return "error";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            if (s.equals("success")) {
                sendOTPtoServer();
            } else {
                Toast.makeText(GenuineCheck.this, getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
            }
        }


    }

    private void sendOTPtoServer() {

        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();

        // Creating object for our interface
        APIService api = retrofit.create(APIService.class);
        Call<OrderResponse> user = api.SentOTP(id, mOtp);
        user.enqueue(new Callback<OrderResponse>() {

            @Override
            public void onResponse(Call<OrderResponse> list, Response<OrderResponse> response) {
                if (response.isSuccessful()) {
                    OrderResponse user = response.body();
                    progressDialog.dismiss();
                    Log.v("Result", response.body().getResult());
                    if (response.body().getResult().equals("Success")) {
                        Toast.makeText(GenuineCheck.this, "OTP sent to your mobile number.", Toast.LENGTH_SHORT).show();
                    } else if (response.body().getResult().equals("NotFound")) {
                        Toast.makeText(GenuineCheck.this, getResources().getString(R.string.server_error), Toast.LENGTH_SHORT).show();
                    } else {
                        box.showAlertbox(getResources().getString(R.string.server_error));
                    }
                } else {
                    progressDialog.dismiss();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            }


            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                progressDialog.dismiss();
                t.printStackTrace();
                box.showAlertbox(getResources().getString(R.string.server_error));
            }
        });

    }

    private static class SimpleOTPGenerator {


        private SimpleOTPGenerator() {
        }

        private static String random(int size) {

            StringBuilder generatedToken = new StringBuilder();
            try {
                SecureRandom number = SecureRandom.getInstance("SHA1PRNG");
                // Generate 20 integers 0..20
                for (int i = 0; i < size; i++) {
                    generatedToken.append(number.nextInt(9));
                }
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            return generatedToken.toString();
        }
    }

    private boolean isValidMobile(String phone) {
        return phone.length() == 10;
        //return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //  smsVerifyCatcher.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //  smsVerifyCatcher.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //   smsVerifyCatcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

}

