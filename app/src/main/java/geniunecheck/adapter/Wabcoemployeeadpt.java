package geniunecheck.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mobileordering.R;

import java.util.ArrayList;
import java.util.List;

import models.genunecheck.Wabcoemployeelist;

/**
 * Created by vibin on 22-Nov-17.
 */

public class Wabcoemployeeadpt extends RecyclerView.Adapter<Wabcoemployeeadpt.ViewHolder> {
//    ArrayList<GenuineLoglist> myList = new ArrayList<>();
    ArrayList<String> sino = new ArrayList<>();
   List<Wabcoemployeelist> Scanlist= new ArrayList<>();;
    int count = 1;
    Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView ed_uname, ed_utype, ed_date, ed_scantype, ed_silno;

        public ViewHolder(View view) {
            super(view);
            ed_uname = (TextView) view.findViewById(R.id.uname);
            ed_utype = (TextView) view.findViewById(R.id.utype);

            ed_date = (TextView) view.findViewById(R.id.date);
            ed_scantype = (TextView) view.findViewById(R.id.type);
            ed_silno = (TextView) view.findViewById(R.id.sino1);

        }
    }

    public Wabcoemployeeadpt(Context context, List<Wabcoemployeelist> Scanlist, ArrayList<String> sino
    ) {
        this.context = context;
//        this.myList = myList;
        this.sino = sino;
        this.Scanlist = Scanlist;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.wabcoemployeeadapter, viewGroup, false);
        context = view.getContext();
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        viewHolder.ed_silno.setText(sino.get(i));
//        viewHolder.ed_uname.setText(myList.get(i).getUserType());

        if(Scanlist.get(i).getUserType().equals("Wabco employee"))
        {
            viewHolder.ed_utype.setText("WABCO Employee");
        }
        else
        {
            viewHolder.ed_utype.setText(Scanlist.get(i).getUserType());
        }


        viewHolder.ed_date.setText(Scanlist.get(i).getCity());
        viewHolder.ed_scantype.setText(Scanlist.get(i).getCount());



//        Picasso.with(context).load(android_versions.get(i).getAndroid_image_url()).resize(120, 60).into(viewHolder.img_android);
    }

    @Override
    public int getItemCount() {
        return Scanlist.size();
    }


}


