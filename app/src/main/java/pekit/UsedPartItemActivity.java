package pekit;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnDismissListener;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.mobileordering.R;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import addcart.CartDAO;
import addcart.CartDTO;
import alertbox.Alertbox;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import notification.NotificationActivity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import quickorder.Quick_Order_Preview_Activity;
import search.SearchActivity;
import vehiclemake.VehicleDAO;
import vehiclemake.VehicleDTO;
import vehiclemake.VehicleMakeActivity;
import vehiclemake.VehiclePartItemActivity_Adapter;
import wabco.Network_Activity;

;

public class UsedPartItemActivity extends Activity {


    String INPUT_DB_PATH;
    private TextView Title;
    ImageView arrleft;
    ImageView arrright;
    private LinearLayout flagtext;
    private TextView headText1;
    private TextView headText2;
    private TextView headText3;
    HorizontalScrollView hsv;
    private ListView listpart;
    private ProgressDialog loadDialog;
    int[] logoimages;
    int position;
    private SharedPreferences preferences;
    private List<String> productName;
    Boolean stats;
    int value;
    private ImageView vehicleBackImageView;
    private List<String> vehicleDescriptionList;
    public List<String> vehicleFlagList;
    private String vehicleID;
    private List<String> vehicleImageList, productIDList;
    private List<String> vehicleName;
    private List<String> vehiclePartNoList;
    private String vehicleProductFamilyID;
    public List<Bitmap> productbitmap;
    private Bitmap bitmap;
    public List<String> vehicleIDList;
    private String partno;
    Alertbox box = new Alertbox(UsedPartItemActivity.this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private ImageView Cart_Icon;
    private String UserType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_used_part_item);
        this.partno = getIntent().getStringExtra("part");


        productbitmap = new ArrayList<Bitmap>();

        productName = new ArrayList<String>();
        vehicleName = new ArrayList<String>();

        this.flagtext = (LinearLayout) findViewById(R.id.flagtxt);

        this.hsv = (HorizontalScrollView) findViewById(R.id.horilist);
        this.arrleft = (ImageView) findViewById(R.id.left);
        this.arrright = (ImageView) findViewById(R.id.right);
        this.listpart = (ListView) findViewById(R.id.listView);
        //this.Title = (TextView) findViewById(R.id.tittle);

        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();
        Cart_Icon = (ImageView) findViewById(R.id.cart_icon);

        UserType = myshare.getString("usertype", "").toString();
        if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")))
            Cart_Icon.setVisibility(View.VISIBLE);
        else
            Cart_Icon.setVisibility(View.GONE);


        new RetrievePartItemAsyn().execute(new String[]{this.vehicleProductFamilyID});


        this.arrright.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                hsv.smoothScrollTo(hsv.getScrollX() + 1000, hsv.getScrollY());
                Log.v("inside image click", "yeah");
                arrright.setVisibility(View.GONE);
                arrleft.setVisibility(View.VISIBLE);
            }
        });
        this.arrleft.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                hsv.smoothScrollTo(hsv.getScrollX() - 1000, hsv.getScrollY());
                arrleft.setVisibility(View.GONE);
                arrright.setVisibility(View.VISIBLE);
            }
        });

        Cart_Icon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                CartDAO cartDAO = new CartDAO(getApplicationContext());
                CartDTO cartDTO = cartDAO.GetCartItems();
                if (cartDTO.getPartCodeList() == null) {
                    StyleableToast st =
                            new StyleableToast(getApplicationContext(), "Cart is Empty !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                } else {
                    startActivity(new Intent(getApplicationContext(), Quick_Order_Preview_Activity.class)
                            .putExtra("from", "CartItem"));
                }
                // startActivity(new Intent(ProductFamilyActivity.this, Cart_Activity.class));
            }
        });

        final ImageView menu = (ImageView) findViewById(R.id.menu);
        menu.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(UsedPartItemActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(UsedPartItemActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(UsedPartItemActivity.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(UsedPartItemActivity.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(UsedPartItemActivity.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(UsedPartItemActivity.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(UsedPartItemActivity.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(UsedPartItemActivity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(UsedPartItemActivity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(UsedPartItemActivity.this, PriceListActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(UsedPartItemActivity.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });


        this.vehicleBackImageView = (ImageView) findViewById(R.id.back);
        this.vehicleBackImageView.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {
                onBackPressed();
            }
        });


    }


    class RetrievePartItemAsyn extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            loadDialog = new ProgressDialog(UsedPartItemActivity.this);
            loadDialog.setMessage("Loading...");
            loadDialog.setProgressStyle(0);
            loadDialog.setCancelable(false);
            loadDialog.show();
        }

        protected String doInBackground(String... position) {
            try {
                VehicleDAO connection = new VehicleDAO(UsedPartItemActivity.this);
                VehicleDTO vehicleDAO = new VehicleDTO();
                vehicleDAO = connection.retrieveUsedPartItemList(partno);
                vehicleDAO.getPartNoIDList();
                vehiclePartNoList = vehicleDAO.getPartNoList();
                vehicleIDList = vehicleDAO.getVehiclePartProductNOList();
                vehicleDescriptionList = vehicleDAO.getPartDespcriptionList();
                vehicleImageList = vehicleDAO.getPartImageList();
                productIDList = vehicleDAO.getProductIDList();
                vehicleFlagList = vehicleDAO.getFlagList();
                productName = vehicleDAO.getProductNameList();
                vehicleName = vehicleDAO.getVehicleNameList();

                if (vehicleDescriptionList.isEmpty()) {
                    return "unsuccess";
                }
                return "success";
            } catch (Exception e) {
                Log.v("Error", e.getMessage());
                return "unsuccess";
            }
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            loadDialog.dismiss();
            if (result.equals("success")) {
                if (!vehicleFlagList.isEmpty()) {
                    flagtext.setVisibility(View.VISIBLE);
                }

                listpart.setAdapter(new VehiclePartItemActivity_Adapter(UsedPartItemActivity.this, vehicleIDList, vehiclePartNoList, vehicleDescriptionList, vehicleName, productName, vehicleFlagList, vehicleImageList, productIDList));
                arrright.setVisibility(View.VISIBLE);

            } else {
                flagtext.setVisibility(View.GONE);
                box.showAlertbox("No date found !");
            }
        }
    }

    private Bitmap downloadImage(String url) {

        try {
            InputStream fileInputStream = this.getAssets().open(url);
            Options options = new Options();
            options.inScaled = false;
            options.inSampleSize = 8;
            bitmap = BitmapFactory.decodeStream(fileInputStream);
            fileInputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
            Log.v("Error", e.getMessage());


            return downloadImage("noimagefound");

        }
        return bitmap;
    }


    public void onBackPressed() {
        super.onBackPressed();
    }
}
