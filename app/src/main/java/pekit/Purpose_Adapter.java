package pekit;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mobileordering.R;

public class Purpose_Adapter extends ArrayAdapter<String> {

	private Context context;
	List<String> partnoList;
	public Purpose_Adapter(Context context, List<String> descriptionList) 
	{
		 super(context, R.layout.simple_list_item, descriptionList);
		 this.partnoList = descriptionList;
		 this.context=context;
		 
		// TODO Auto-generated constructor stub
	}
	 public View getView(final int position, View convertView, ViewGroup parent) 
	 {


		 Purpose_Holder contactHolder;
	        if (convertView == null)
	        {
	        	
	        	 contactHolder = new Purpose_Holder();
	        	convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(R.layout.simple_list_item, null);
		           
	            contactHolder.snotxt = (TextView) convertView.findViewById(R.id.purposetxt);
	            contactHolder. snotxt.setText(partnoList.get(position));
	           
	        } 
	        else 
	        {
	            contactHolder = (Purpose_Holder) convertView.getTag();
	        }
	        
	       
	        return convertView;
	    }
	}

 class Purpose_Holder
{
	 
	 TextView snotxt ;
}
