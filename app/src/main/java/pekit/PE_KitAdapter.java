package pekit;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.mobileordering.R;

import java.util.List;

import addcart.Add_TocartActivity;

import static android.content.Context.MODE_PRIVATE;

public class PE_KitAdapter extends ArrayAdapter<String> {
	Context context;
	private List<String> descrip;
	private List<String> partcodeno;
	private List<String> partnoList;
	private List<String> flaglist,priceflagList;
	private SharedPreferences myshare;
	private SharedPreferences.Editor edit;
	private String UserType;


	public PE_KitAdapter(Context context, List<String> partnoList, List<String> partcodeno, List<String> descrip,List<String> flaglist,List<String> priceflagList) {
		super(context, R.layout.activity_pe__kit_txt, partnoList);
		this.context = context;
		this.partnoList = partnoList;
		this.partcodeno = partcodeno;
		this.descrip = descrip;
		this.flaglist = flaglist;
		this.priceflagList = priceflagList;
		myshare = context.getSharedPreferences("registration", MODE_PRIVATE);
		edit = myshare.edit();
		UserType = myshare.getString("usertype","").toString();
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		PEHolder holder;
		if (convertView == null) {
			convertView = ((LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.activity_pe__kit_txt, null);
			holder = new PEHolder();
			holder.snotxt = (TextView) convertView.findViewById(R.id.textView2);
			holder.partnotxt = (TextView) convertView.findViewById(R.id.textView5);
			holder.partcodenotxt = (TextView) convertView.findViewById(R.id.textView8);
			holder.descriptxt = (TextView) convertView.findViewById(R.id.textView3);
			holder.more = (Button) convertView.findViewById(R.id.more);
			holder.purpos = (Button) convertView.findViewById(R.id.purpose);
			holder.addcart = (Button) convertView.findViewById(R.id.addcart);


			if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")||UserType.equals("ASC")) && (priceflagList.get(position)).equals("0"))
			{
				holder.addcart.setVisibility(View.VISIBLE);
			}
			else
			{
				holder.addcart.setVisibility(View.GONE);
			}

			convertView.setTag(holder);
			
			
			
			
			
		} else {
			holder = (PEHolder) convertView.getTag();
		}
		holder.snotxt.setText((position + 1) + ".");
		holder.descriptxt.setText((CharSequence) this.descrip.get(position));
		holder.partnotxt.setText((CharSequence) this.partnoList.get(position));
		holder.partcodenotxt.setText((CharSequence) this.partcodeno.get(position));

		
		if(flaglist.get(position).toString().equals("1"))
		{
			holder.purpos.setText("Used in");
		}
		else {
			holder.purpos.setText("Purpose");
		}


		holder.more.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent vehiclepartnotxtFamily = new Intent(PE_KitAdapter.this.context, BomActivity.class);
				vehiclepartnotxtFamily.putExtra("PartCode", (String) PE_KitAdapter.this.partcodeno.get(position));
				PE_KitAdapter.this.context.startActivity(vehiclepartnotxtFamily);
			}
		});
		holder.purpos.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub


				if(flaglist.get(position).toString().equals("1"))
				{
					Intent vehiclepartnotxtFamily = new Intent(PE_KitAdapter.this.context, UsedActivity.class);
					vehiclepartnotxtFamily.putExtra("PartCode", (String) PE_KitAdapter.this.partcodeno.get(position));
					PE_KitAdapter.this.context.startActivity(vehiclepartnotxtFamily);
				}
				else 
				{
					Intent vehiclepartnotxtFamily = new Intent(PE_KitAdapter.this.context, Purpose_Activity.class);
					vehiclepartnotxtFamily.putExtra("PartCode", (String) PE_KitAdapter.this.partcodeno.get(position));
					PE_KitAdapter.this.context.startActivity(vehiclepartnotxtFamily);
				}


			}
		});

		holder.addcart.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				Intent gotocart = new Intent(context, Add_TocartActivity.class);
				gotocart.putExtra("from", "vehicle");
				gotocart.putExtra("PART_NO", partnoList.get(position));
				gotocart.putExtra("Description", descrip.get(position));
				context.startActivity(gotocart);
			}
		});



		return convertView;
	}
}
