package directory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.StrictMode;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mobileordering.R;

public class DownloadDatabaseFile {
    private static final String FILENAME = "wabco.zip";
    private Activity context;
    private Dialog dialog;
    private String download_file_path;
    private int downloadedSize;
    private Editor editor;
    private SharedPreferences preferences;
    private ProgressBar progressBar;
    private TextView progressValue;
    private int totalSize;

   

    

    public DownloadDatabaseFile(Activity context) {
        this.totalSize = 0;
        this.downloadedSize = 0;
       // this.download_file_path = "http://brainmagic.info/MobileApps/wabco/db/wabco.zip";
        this.context = context;
        this.preferences = context.getSharedPreferences("DOWNLOAD_CHECK", 0);
        this.editor = this.preferences.edit();
       
    }
    
    
    public  boolean exists(String URLName){
		try {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy);
			HttpURLConnection.setFollowRedirects(false);
			// note : you may also need
			//        HttpURLConnection.setInstanceFollowRedirects(false)
			HttpURLConnection con =
					(HttpURLConnection) new URL(URLName.toString()).openConnection();
			con.setRequestMethod("HEAD");
			return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
		}
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

    public void downloadOnlineFile(File filePath,String url) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(url).openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setDoOutput(false);
            httpURLConnection.connect();
            FileOutputStream fileOutput = new FileOutputStream(filePath);
            InputStream inputStream = httpURLConnection.getInputStream();
            this.totalSize = httpURLConnection.getContentLength();
            this.context.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					 progressBar.setMax(DownloadDatabaseFile.this.totalSize);
				}
			});
            byte[] buffer = new byte[AccessibilityNodeInfoCompat.ACTION_NEXT_HTML_ELEMENT];
            while (true) {
                int bufferLength = inputStream.read(buffer);
                if (bufferLength <= 0) {
                    fileOutput.close();
                    this.context.runOnUiThread(new Runnable() {
						
						@Override
						public void run() {
							// TODO Auto-generated method stub
							 try {
					                DownloadDatabaseFile.this.downloadedSize = 0;
					                DownloadDatabaseFile.this.totalSize = 0;
					                new UNZipDownloadFile(DownloadDatabaseFile.this.context).unZipFile();
					              // DownloadDatabaseFile.this.editor.putString("FILE_DATE", new CheckOnlineFileModifiedDate().checkFileDate());
					                DownloadDatabaseFile.this.editor.commit();
					                DownloadDatabaseFile.this.editor.putBoolean("DOWNLOAD_DATA", false);
					                DownloadDatabaseFile.this.editor.commit();
					                DownloadDatabaseFile.this.dialog.dismiss();
					            } catch (Exception e) {
					                DownloadDatabaseFile.this.editor.putBoolean("DOWNLOAD_DATA", true);
					                DownloadDatabaseFile.this.editor.commit();
					            }
						}
					});
                   
                }
                fileOutput.write(buffer, 0, bufferLength);
                this.downloadedSize += bufferLength;
                this.context.runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						// TODO Auto-generated method stub
						 progressBar.setProgress(DownloadDatabaseFile.this.downloadedSize);
				            progressValue.setText("Downloaded " + DownloadDatabaseFile.this.downloadedSize + "KB / " + DownloadDatabaseFile.this.totalSize + "KB (" + ((int) ((double) ((((float) DownloadDatabaseFile.this.downloadedSize) 
				            		/ ((float) DownloadDatabaseFile.this.totalSize)) * 100.0f))) + "%)");
					}
				});
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.v("in download online file", e.getMessage());
        }
    }

    public void showProgress() {
        LayoutParams lp = new LayoutParams();
        this.dialog = new Dialog(this.context);
        this.dialog.requestWindowFeature(1);
        this.dialog.setContentView(R.layout.custom_download_progressdialog);
        this.dialog.setTitle("Download Progress");
        this.dialog.setCancelable(false);
        this.progressValue = (TextView) this.dialog.findViewById(R.id.progress_value_textview);
        this.progressValue.setText("Starting download...");
        ((Button) this.dialog.findViewById(R.id.cancel_button)).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 DownloadDatabaseFile.this.dialog.dismiss();
			}
		});
        this.dialog.show();
        lp.copyFrom(this.dialog.getWindow().getAttributes());
        lp.width = -1;
        this.dialog.getWindow().setAttributes(lp);
        this.progressBar = (ProgressBar) this.dialog.findViewById(R.id.progress_bar);
        this.progressBar.setProgress(0);
        this.progressBar.setProgressDrawable(this.context.getResources().getDrawable(R.drawable.green_progress));
    }
}
