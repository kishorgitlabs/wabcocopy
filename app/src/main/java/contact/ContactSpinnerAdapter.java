package contact;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mobileordering.R;

@SuppressLint({"InflateParams"})
public class ContactSpinnerAdapter extends ArrayAdapter<String> {
    List<String> codeList;
    Context context;

    public ContactSpinnerAdapter(Context context, List<String> codeList) {
        super(context, R.layout.activity_productfamily_spinner_item, codeList);
        this.context = context;
        this.codeList = codeList;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        SpinnerHolder spinnerHolder;
        if (convertView == null) {
            convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(R.layout.activity_productfamily_spinner_item, null);
            spinnerHolder = new SpinnerHolder();
            spinnerHolder.codename = (TextView) convertView.findViewById(R.id.code_textView);
            convertView.setTag(spinnerHolder);
        } else {
            spinnerHolder = (SpinnerHolder) convertView.getTag();
        }
        spinnerHolder.codename.setText((CharSequence) this.codeList.get(position));
        return convertView;
    }
}
