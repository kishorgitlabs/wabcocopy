package contact;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import persistence.DBHelper;


public class ContactDAO {
    private Context context;
    private SQLiteDatabase db;
    private DBHelper dbHelper;

    public ContactDAO(Context context) {
        this.context = context;
        this.dbHelper = new DBHelper(context);
    }

    public void openDatabase() {
        this.db = this.dbHelper.readDataBase();
    }

    public void closeDatabase() {
        if (this.db != null) {
            this.db.close();
        }
    }

    public ContactDTO retriveState(String contacttype) {
        ContactDTO contactDTO = new ContactDTO();
        Cursor cursor = null;
        openDatabase();
        List<String> StateList = new ArrayList<String>();

           // cursor = this.db.rawQuery("select distinct State_Name from address where contact_type ='" + contacttype + "' and Distributor_Name LIKE 'MAS-%' order by State_Name asc", null);

        cursor = this.db.rawQuery("select distinct State_Name from address where contact_type ='" + contacttype + "'  order by State_Name asc", null);


        if (cursor.moveToFirst()) {
            do {
                StateList.add(cursor.getString(cursor.getColumnIndex("State_Name")));
            } while (cursor.moveToNext());
        }
        closeDatabase();
        contactDTO.setStateNameList(StateList);
        return contactDTO;
    }

    public ContactDTO retriveCity(String statename,String contacttype) {
        ContactDTO contactDTO = new ContactDTO();
        openDatabase();
        List<String> CityList = new ArrayList<String>();
        Log.v("Clicked state in city", statename);
       // Cursor cursor = this.db.rawQuery("select distinct City from address where State_Name ='" + statename + "' and Distributor_Name LIKE 'MAS-%' and contact_type ='" + contacttype + "'order by City asc", null);
        Cursor cursor = this.db.rawQuery("select distinct City from address where State_Name ='" + statename + "' and  contact_type ='" + contacttype + "'order by City asc", null);

        if (cursor.moveToFirst()) {
            do {
                CityList.add(cursor.getString(cursor.getColumnIndex("City")));
            } while (cursor.moveToNext());
        }
        closeDatabase();
        contactDTO.setCityNameList(CityList);
        return contactDTO;
    }

    @SuppressWarnings("unchecked")
    public ContactDTO retriveAddress(String query) {
        ContactDTO contactDTO = new ContactDTO();
        openDatabase();
        List<String> CityList = new ArrayList<String>();
        List<String> StateList = new ArrayList<String>();
        List<String> Distributor = new ArrayList<String>();
        List<String> Address = new ArrayList<String>();
        List<String> phone = new ArrayList<String>();
        List<String> Email1 = new ArrayList<String>();
        List<String> Email2 = new ArrayList<String>();
        List<String> contact_type = new ArrayList<String>();
        List<String> name = new ArrayList<String>();
        List<String> distributor_code = new ArrayList<String>();
        @SuppressWarnings("rawtypes")
        List<String> Email3 = new ArrayList();
        Cursor cursor = this.db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                StateList.add(cursor.getString(cursor.getColumnIndex("State_Name")));
                CityList.add(cursor.getString(cursor.getColumnIndex("City")));
                Distributor.add(cursor.getString(cursor.getColumnIndex("Distributor_Name")));
                Address.add(cursor.getString(cursor.getColumnIndex("Address")));
                phone.add(cursor.getString(cursor.getColumnIndex("PhoneNumber")));
                Email1.add(cursor.getString(cursor.getColumnIndex("Mail_id1")));
                Email2.add(cursor.getString(cursor.getColumnIndex("Mail_id2")));
                Email3.add(cursor.getString(cursor.getColumnIndex("Mail_id3")));
                contact_type.add(cursor.getString(cursor.getColumnIndex("contact_type")));
                name.add(cursor.getString(cursor.getColumnIndex("Distributor_Name")));
                distributor_code.add(cursor.getString(cursor.getColumnIndex("wsd")));
            } while (cursor.moveToNext());
        }
        closeDatabase();
        contactDTO.setStateNameList(StateList);
        contactDTO.setCityNameList(CityList);
        contactDTO.setDistributorList(Distributor);
        contactDTO.setAddressList(Address);
        contactDTO.setphoneList(phone);
        contactDTO.setEmail1List(Email1);
        contactDTO.setEmail2List(Email2);
        contactDTO.setEmail3List(Email3);
        contactDTO.setContacttype(contact_type);
        contactDTO.setName(name);
        contactDTO.setDistributor_code(distributor_code);
        return contactDTO;
    }


    public ContactDTO retriveFavoriteDistributor(String query) {
        ContactDTO contactDTO = new ContactDTO();
        openDatabase();
        List<String> name = new ArrayList<String>();
        List<String> Address = new ArrayList<String>();
        List<String> distributor_code = new ArrayList<String>();
        List<String> phone = new ArrayList<String>();
        List<String> email = new ArrayList<String>();

        Cursor cursor = this.db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {

                Address.add(cursor.getString(cursor.getColumnIndex("brainmagic")));
                name.add(cursor.getString(cursor.getColumnIndex("name")));
                distributor_code.add(cursor.getString(cursor.getColumnIndex("id")));
                phone.add(cursor.getString(cursor.getColumnIndex("mobile")));
                email.add(cursor.getString(cursor.getColumnIndex("email")));
            } while (cursor.moveToNext());
        }
        closeDatabase();

        contactDTO.setAddressList(Address);
        contactDTO.setName(name);
        contactDTO.setDistributor_code(distributor_code);
        contactDTO.setphoneList(phone);
        contactDTO.setEmail1List(email);
        return contactDTO;
    }

}
