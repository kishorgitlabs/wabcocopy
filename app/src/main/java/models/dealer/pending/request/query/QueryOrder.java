
package models.dealer.pending.request.query;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class QueryOrder {

    @SerializedName("OrderNumber")
    private String mOrderNumber;
    @SerializedName("Query")
    private String mQuery;
    @SerializedName("QueryDate")
    private String mQueryDate;

    public String getOrderNumber() {
        return mOrderNumber;
    }

    public void setOrderNumber(String OrderNumber) {
        mOrderNumber = OrderNumber;
    }

    public String getQuery() {
        return mQuery;
    }

    public void setQuery(String Query) {
        mQuery = Query;
    }

    public String getQueryDate() {
        return mQueryDate;
    }

    public void setQueryDate(String QueryDate) {
        mQueryDate = QueryDate;
    }

}
