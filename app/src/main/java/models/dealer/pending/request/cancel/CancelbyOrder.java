
package models.dealer.pending.request.cancel;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class CancelbyOrder {

    @SerializedName("cancelledBy")
    private String mCancelledBy;
    @SerializedName("CancelledDate")
    private String mCancelledDate;
    @SerializedName("Reason")
    private String mDealReason;
    @SerializedName("Dealerid")
    private String mDealerid;
    @SerializedName("OrderNumber")
    private String mOrderNumber;

    @SerializedName("Active")
    private String mActive;
    public String getCancelledBy() {
        return mCancelledBy;
    }

    public void setCancelledBy(String cancelledBy) {
        mCancelledBy = cancelledBy;
    }

    public String getCancelledDate() {
        return mCancelledDate;
    }

    public void setCancelledDate(String CancelledDate) {
        mCancelledDate = CancelledDate;
    }

    public String getDealReason() {
        return mDealReason;
    }

    public void setDealReason(String dealReason) {
        mDealReason = dealReason;
    }

    public String getDealerid() {
        return mDealerid;
    }

    public void setDealerid(String Dealerid) {
        mDealerid = Dealerid;
    }

    public String getOrderNumber() {
        return mOrderNumber;
    }

    public void setOrderNumber(String OrderNumber) {
        mOrderNumber = OrderNumber;
    }

    public String getActive() {
        return mActive;
    }

    public void setActive(String mActive) {
        this.mActive = mActive;
    }

}
