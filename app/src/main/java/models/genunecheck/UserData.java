
package models.genunecheck;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class UserData {

    @SerializedName("brainmagic")
    private Object mAddress;
    @SerializedName("city")
    private String mCity;
    @SerializedName("country")
    private String mCountry;
    @SerializedName("date")
    private String mDate;
    @SerializedName("deletestatus")
    private Object mDeletestatus;
    @SerializedName("DeviceType")
    private String mDeviceType;
    @SerializedName("deviceid")
    private String mDeviceid;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("gstnumber")
    private Object mGstnumber;
    @SerializedName("id")
    private Long mId;
    @SerializedName("name")
    private String mName;
    @SerializedName("NewPassword")
    private Object mNewPassword;
    @SerializedName("pannumber")
    private Object mPannumber;
    @SerializedName("password")
    private Object mPassword;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("shopname")
    private Object mShopname;
    @SerializedName("state")
    private String mState;
    @SerializedName("username")
    private Object mUsername;
    @SerializedName("usertype")
    private String mUsertype;

    public Object getAddress() {
        return mAddress;
    }

    public void setAddress(Object address) {
        mAddress = address;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public Object getDeletestatus() {
        return mDeletestatus;
    }

    public void setDeletestatus(Object deletestatus) {
        mDeletestatus = deletestatus;
    }

    public String getDeviceType() {
        return mDeviceType;
    }

    public void setDeviceType(String DeviceType) {
        mDeviceType = DeviceType;
    }

    public String getDeviceid() {
        return mDeviceid;
    }

    public void setDeviceid(String deviceid) {
        mDeviceid = deviceid;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Object getGstnumber() {
        return mGstnumber;
    }

    public void setGstnumber(Object gstnumber) {
        mGstnumber = gstnumber;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public Object getNewPassword() {
        return mNewPassword;
    }

    public void setNewPassword(Object NewPassword) {
        mNewPassword = NewPassword;
    }

    public Object getPannumber() {
        return mPannumber;
    }

    public void setPannumber(Object pannumber) {
        mPannumber = pannumber;
    }

    public Object getPassword() {
        return mPassword;
    }

    public void setPassword(Object password) {
        mPassword = password;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public Object getShopname() {
        return mShopname;
    }

    public void setShopname(Object shopname) {
        mShopname = shopname;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public Object getUsername() {
        return mUsername;
    }

    public void setUsername(Object username) {
        mUsername = username;
    }

    public String getUsertype() {
        return mUsertype;
    }

    public void setUsertype(String usertype) {
        mUsertype = usertype;
    }

}
