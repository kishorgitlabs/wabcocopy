
package models.serviceengineer.pending;

import com.google.gson.annotations.SerializedName;

import java.util.List;

@SuppressWarnings("unused")
public class OrderResult {

    @SerializedName("data")
    private List<OrderResultDetails> mData;
    @SerializedName("result")
    private String mResult;

    public List<OrderResultDetails> getData() {
        return mData;
    }

    public void setData(List<OrderResultDetails> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
