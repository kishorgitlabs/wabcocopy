package brainmagic.distributoraddress;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.mobileordering.R;

import java.util.ArrayList;
import java.util.List;

import adapter.FavoriteDistributorRemoveAdapter;
import adapter.FavoriteDistributorSelectAdapter;
import addcart.CartDAO;
import addcart.CartDTO;
import addcart.Order_ConfirmationActivity;
import alertbox.Alertbox;
import askwabco.AskWabcoActivity;
import contact.ContactDAO;
import contact.ContactDTO;
import directory.WabcoUpdate;
import home.MainActivity;
import models.distributor.DistributorData;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import persistence.DBHelper;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import quickorder.Quick_Order_Preview_Activity;
import search.SearchActivity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;

public class Favorite_DistributorActivity extends AppCompatActivity {

  private Button okay;
  private SharedPreferences dealershare, distributor_share;
  private SharedPreferences.Editor dealeredit, distributor_edit;
  private ProgressDialog loading;
  private ArrayList<DistributorData> distributorModel;
  private String ComingFor, comingFrom;
  private ListView listview;
  private FavoriteDistributorSelectAdapter selectadapter;
  private FavoriteDistributorRemoveAdapter removeadapter;
  private Alertbox box = new Alertbox(Favorite_DistributorActivity.this);
  private SQLiteDatabase db;
  private Cursor c;
  private DBHelper dbHelper;
  private ImageView cart_icon;
  private List<String> removedList;
  private ImageView back;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_favorite__distributor);

    okay = (Button) findViewById(R.id.remove);
    cart_icon = (ImageView) findViewById(R.id.cart_icon);

    distributor_share = getSharedPreferences("distributor_address", MODE_PRIVATE);
    distributor_edit = distributor_share.edit();
    distributor_edit.clear();
    distributor_edit.commit();

    dealershare = getSharedPreferences("registration", MODE_PRIVATE);
    dealeredit = dealershare.edit();

    listview = (ListView) findViewById(R.id.listview);
    removedList = new ArrayList<String>();
    ComingFor = getIntent().getStringExtra("for");
    comingFrom = getIntent().getStringExtra("from");

    if (ComingFor.equals("remove")) {
      okay.setText("Remove Distributor");
      cart_icon.setVisibility(View.GONE);
    } else {
      okay.setText("Select Distributor");
      cart_icon.setVisibility(View.VISIBLE);
    }

    okay.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (ComingFor.equals("remove")) {
          boolean isSelected = false;
          for (int i = 0; i < removeadapter.getCount(); i++) {
            if (distributorModel.get(i).getFavorite()) {
              isSelected = true;
              break;
            }

          }

          if (isSelected)
            DeleteFavoriteList();
          else {
            StyleableToast st = new StyleableToast(Favorite_DistributorActivity.this,
                getResources().getString(R.string.select_distributor), Toast.LENGTH_SHORT);
            st.setBackgroundColor(
                Favorite_DistributorActivity.this.getResources().getColor(R.color.red));
            st.setTextColor(Color.WHITE);
            st.setMaxAlpha();
            st.show();
          }

        } else {

          if (!distributor_share.getString("distributor_name", "").equals("")) {
            startActivity(
                new Intent(Favorite_DistributorActivity.this, Order_ConfirmationActivity.class)
                    .putExtra("from", comingFrom));
          } else {
            StyleableToast st = new StyleableToast(Favorite_DistributorActivity.this,
                getResources().getString(R.string.select_distributor), Toast.LENGTH_SHORT);
            st.setBackgroundColor(
                Favorite_DistributorActivity.this.getResources().getColor(R.color.red));
            st.setTextColor(Color.WHITE);
            st.setMaxAlpha();
            st.show();
          }

        }

      }
    });


    this.back = (ImageView) findViewById(R.id.back);
    this.back.setOnClickListener(new View.OnClickListener() {


      public void onClick(View arg0) {
        onBackPressed();
      }
    });
    final ImageView menu = (ImageView) findViewById(R.id.menu);
    menu.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        Context wrapper = new ContextThemeWrapper(Favorite_DistributorActivity.this, R.style.PopupMenu);
        final PopupMenu pop = new PopupMenu(wrapper, v);
        pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

          public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
              case R.id.home:
                startActivity(new Intent(Favorite_DistributorActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
              case R.id.search:
                startActivity(new Intent(Favorite_DistributorActivity.this, SearchActivity.class));
                break;
              case R.id.notification:
                startActivity(new Intent(Favorite_DistributorActivity.this, NotificationActivity.class));
                break;
              case R.id.vehicle:
                startActivity(new Intent(Favorite_DistributorActivity.this, VehicleMakeActivity.class));
                break;
              case R.id.product:
                startActivity(new Intent(Favorite_DistributorActivity.this, ProductFamilyActivity.class));
                break;
              case R.id.performance:
                startActivity(new Intent(Favorite_DistributorActivity.this, PE_Kit_Activity.class));
                break;
              case R.id.contact:
                startActivity(new Intent(Favorite_DistributorActivity.this, Network_Activity.class));
                break;
              case R.id.askwabco:
                startActivity(new Intent(Favorite_DistributorActivity.this, AskWabcoActivity.class));
                break;
              case R.id.pricelist:
                startActivity(new Intent(Favorite_DistributorActivity.this, PriceListActivity.class));
                break;
              case R.id.update:
                WabcoUpdate update = new WabcoUpdate(Favorite_DistributorActivity.this);
                update.checkVersion();
                break;
            }
            return false;
          }
        });
        pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

          @Override
          public void onDismiss(PopupMenu arg0) {
            // TODO Auto-generated method stub
            pop.dismiss();
          }
        });

        pop.inflate(R.menu.main);
        pop.show();
      }
    });

    cart_icon.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v)
      {
        CartDAO cartDAO = new CartDAO(Favorite_DistributorActivity.this);
        CartDTO cartDTO = cartDAO.GetCartItems();
        if(cartDTO.getPartCodeList() == null)
        {
          StyleableToast st =
                  new StyleableToast(Favorite_DistributorActivity.this, "Cart is Empty !", Toast.LENGTH_SHORT);
          st.setBackgroundColor(Favorite_DistributorActivity.this.getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();
        }else
        {
          startActivity(new Intent(Favorite_DistributorActivity.this, Quick_Order_Preview_Activity.class).putExtra("from","CartItem"));
        }
        //  startActivity(new Intent(ProductFamilyActivity.this, Cart_Activity.class));
      }
    });


    new RetriveDealerAddress().execute("select * from FavoriteDistributor");
  }

  private void DeleteFavoriteList() {
    dbHelper = new DBHelper(Favorite_DistributorActivity.this);
    db = dbHelper.readDataBase();

    List<DistributorData> copy = new ArrayList<DistributorData>(distributorModel);
    for (DistributorData wp : distributorModel)
    {
      if (wp.getFavorite()) {
        copy.remove(wp);
          if (!wp.getId().equals(""))
              db.execSQL("delete from FavoriteDistributor where id = " +wp.getId());
          else
          {
              db.execSQL("delete from FavoriteDistributor where mobile = '"
                      + wp.getMobileNumber() + "' and email = '"
                      + wp.getEmail() + "'");
          }
      }
    }
    distributorModel.clear();
    distributorModel.addAll(copy);

    db.close();
    dbHelper.close();
    removeadapter = new FavoriteDistributorRemoveAdapter(Favorite_DistributorActivity.this, distributorModel);
    removeadapter.notifyDataSetChanged();
    listview.setAdapter(removeadapter);
  }


  class RetriveDealerAddress extends AsyncTask<String, Void, String> {
    protected void onPreExecute() {
      super.onPreExecute();
      loading = ProgressDialog.show(Favorite_DistributorActivity.this, "Loading", "Please wait...",
          false, false);
    }
    protected String doInBackground(String... params) {
      ContactDAO contactDAO = new ContactDAO(Favorite_DistributorActivity.this);
      ContactDTO contactDTO = new ContactDTO();
      Log.v("Select_DistributorActivity  ", params[0]);
      contactDTO = contactDAO.retriveFavoriteDistributor(params[0]);
      distributorModel = new ArrayList<DistributorData>();

      for (int i = 0; i < contactDTO.getDistributor_code().size(); i++) {
        DistributorData distributorData = new DistributorData();
        distributorData.setId(contactDTO.getDistributor_code().get(i));
        distributorData.setName(contactDTO.getName().get(i));
        distributorData.setAddress(contactDTO.getAddress().get(i));
        distributorData.setEmail(contactDTO.getEmail1().get(i));
        distributorData.setMobileNumber(contactDTO.getphone().get(i));
        distributorModel.add(distributorData);
        removedList.add("add");
      }
      contactDAO.closeDatabase();
      return "success";
    }

    @SuppressWarnings("deprecation")
    protected void onPostExecute(String result)
    {
      super.onPostExecute(result);
      loading.dismiss();
      if (distributorModel.size() == 0)
        box.showNegativebox("Favorite distributors not available !");
      else
        LoadDistributors(distributorModel);
    }

  }

  private void LoadDistributors(List<DistributorData> distributorModel) {

    if (ComingFor.equals("remove")) {
      removeadapter =
          new FavoriteDistributorRemoveAdapter(Favorite_DistributorActivity.this, distributorModel);
      removeadapter.notifyDataSetChanged();
      listview.setAdapter(removeadapter);
    } else {
      selectadapter =
          new FavoriteDistributorSelectAdapter(Favorite_DistributorActivity.this, distributorModel);
      selectadapter.notifyDataSetChanged();
      listview.setAdapter(selectadapter);
    }

  }

}
