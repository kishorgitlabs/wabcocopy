package changepassword;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.mobileordering.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import alertbox.Alertbox;
import api.APIService;
import api.ApiUtils;
import askwabco.AskWabcoActivity;
import dealer.account.Dealer_Account_Activity;
import directory.WabcoUpdate;
import home.MainActivity;
import models.loginmodel.Login;
import network.NetworkConnection;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import search.SearchActivity;
import serviceengineer.account.FieldEngineer_Account;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;

public class ChangePasswordActivity extends AppCompatActivity {


    private Button Cancelbtn, Updatebtn;
    private View headview;
    private TextView Tittle;
    private EditText Oldpassword, Newpassword, Confirmpassword;
    private Alertbox box = new Alertbox(ChangePasswordActivity.this);
    private Matcher matcher;
    private Pattern pattern;
    private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z]).{8,20})";
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private String UserID, currentPassword, newPassword;
    private ImageView account, cart_icon, back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();

        headview = findViewById(R.id.header_layout);
        Tittle = (TextView) headview.findViewById(R.id.tittle);
        account = (ImageView) headview.findViewById(R.id.account_icon);
        cart_icon = (ImageView) headview.findViewById(R.id.cart_icon);
        back = (ImageView) headview.findViewById(R.id.back);
        Tittle.setText("Change Password");

        Oldpassword = (EditText) findViewById(R.id.oldpassword);
        Newpassword = (EditText) findViewById(R.id.newpassword);
        Confirmpassword = (EditText) findViewById(R.id.confirmpassword);
        pattern = Pattern.compile(PASSWORD_PATTERN);

        Cancelbtn = (Button) findViewById(R.id.cancel);
        Updatebtn = (Button) findViewById(R.id.update);


        if (myshare.getString("usertype", "").equals("Dealer")
                || myshare.getString("name", "").equals("OEM Dealer")) {

            cart_icon.setVisibility(View.GONE);
        } else {

            cart_icon.setVisibility(View.GONE);
        }


        account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (myshare.getString("usertype", "").equals("Dealer")
                        || myshare.getString("name", "").equals("OEM Dealer")) {
                    startActivity(new Intent(ChangePasswordActivity.this, Dealer_Account_Activity.class));
                } else {

                    startActivity(new Intent(ChangePasswordActivity.this, FieldEngineer_Account.class));
                }


            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Updatebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (Oldpassword.getText().length() == 0) {
                    Oldpassword.setError("Enter your old password !");
                    StyleableToast st = new StyleableToast(ChangePasswordActivity.this, "Enter your Name !",
                            Toast.LENGTH_SHORT);
                    st.setBackgroundColor(ChangePasswordActivity.this.getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();

                } else if (!validate(Newpassword.getText().toString())) {
                    Newpassword.setError(getString(R.string.password_error));
                    StyleableToast st = new StyleableToast(ChangePasswordActivity.this,
                            getString(R.string.password_error), Toast.LENGTH_SHORT);
                    st.setBackgroundColor(ChangePasswordActivity.this.getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                } else if (!Confirmpassword.getText().toString().equals(Newpassword.getText().toString())) {
                    Confirmpassword.setError("Password is mismatching !");
                    StyleableToast st = new StyleableToast(ChangePasswordActivity.this,
                            "Password is mismatching !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(ChangePasswordActivity.this.getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                } else {
                    CheckInternet();
                }


            }
        });

        Cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        this.back = (ImageView) findViewById(R.id.back);
        this.back.setOnClickListener(new View.OnClickListener() {


            public void onClick(View arg0) {
                onBackPressed();
            }
        });
        final ImageView menu = (ImageView) findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(ChangePasswordActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(ChangePasswordActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(ChangePasswordActivity.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(ChangePasswordActivity.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(ChangePasswordActivity.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(ChangePasswordActivity.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(ChangePasswordActivity.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(ChangePasswordActivity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(ChangePasswordActivity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(ChangePasswordActivity.this, PriceListActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(ChangePasswordActivity.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });

    }


    public boolean validate(final String password) {

        matcher = pattern.matcher(password);
        return matcher.matches();

    }

    protected void CheckInternet() {
        // TODO Auto-generated method stub
        NetworkConnection isnet = new NetworkConnection(ChangePasswordActivity.this);
        if (isnet.CheckInternet()) {
            ChangeOldPassword();
        } else {
            box.showAlertbox(getResources().getString(R.string.nointernetmsg));
        }

    }


    private void ChangeOldPassword() {
        final ProgressDialog loading =
                ProgressDialog.show(this, "Login", "Please wait...", false, false);


        UserID = myshare.getString("id", "").toString();
        currentPassword = Oldpassword.getText().toString();
        newPassword = Newpassword.getText().toString();


        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();

        // Creating object for our interface
        APIService api = retrofit.create(APIService.class);

        Call<Login> user = api.ChangePassword(UserID, currentPassword, newPassword);
        user.enqueue(new Callback<Login>() {

            @Override
            public void onResponse(Call<Login> list, Response<Login> response) {
                if (response.isSuccessful()) {
                    Login user = response.body();
                    loading.dismiss();
                    Log.v("Result", response.body().getResult());
                    if (response.body().getResult().equals("Success")) {
                        SuccessLogin();
                    } else if (response.body().getResult().equals("IncorrectPassword")) {
                        FailedLogin();
                    } else {
                        box.showAlertbox(getResources().getString(R.string.server_error));
                    }
                } else {
                    loading.dismiss();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            }


            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                loading.dismiss();
                t.printStackTrace();
                box.showAlertbox(getResources().getString(R.string.server_error));
            }
        });


    }

    private void FailedLogin() {

        box.showAlertbox("Does not match your old password!");
    }

    private void SuccessLogin() {

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(ChangePasswordActivity.this);
        alertDialog.setMessage("Password has been changed successfully !");
        alertDialog.setTitle("WABCO");
        alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // TODO Auto-generated method stub
                edit.putString("password", Newpassword.getText().toString());
                edit.commit();
                dialog.dismiss();


                onBackPressed();
            }
        });
        alertDialog.show();

    }
}

