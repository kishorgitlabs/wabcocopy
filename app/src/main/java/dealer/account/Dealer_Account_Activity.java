package dealer.account;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.PopupMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.viewpagerindicator.CirclePageIndicator;
import com.mobileordering.R;

import java.util.Timer;
import java.util.TimerTask;

import addcart.CartDAO;
import addcart.CartDTO;
import brainmagic.distributoraddress.Favorite_DistributorActivity;
import askwabco.AskWabcoActivity;
import changepassword.ChangePasswordActivity;
import dealer.completed.Dealer_Completed_Order_Activity;
import dealer.pending.Dealer_Pending_Order_Activity;
import dealer.uploadorders.DealerUploadOrdersActivity;
import dealer.wishlist.WishListActivity;
import directory.WabcoUpdate;
import editprofile.EditProfileActivity;
import home.MainActivity;
import home.MyCustomPagerAdapter;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import quickorder.Quick_Order_Preview_Activity;
import search.SearchActivity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;

public class Dealer_Account_Activity extends AppCompatActivity {

    private RelativeLayout Pending_orders_relativelayout, Completed_order_relativelayout, edit_Profile_relativelayout, upload_Orders_relativelayout;
    private RelativeLayout change_Password_layout, Logout_layout, wish_List_layout, fave_List_layout,continue_Oreder;
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private TextView Account_name;
    private ViewPager myPager = null;
    private static int currentPage = 0;
    private static int NUM_PAGES = 3;
    private ImageView Cart_Icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dealer__account);

        CartDAO cartDAO = new CartDAO(Dealer_Account_Activity.this);

        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();

        Pending_orders_relativelayout = (RelativeLayout) findViewById(R.id.pending_orders_relativelayout);
        Completed_order_relativelayout = (RelativeLayout) findViewById(R.id.completed_order_relativelayout);
        edit_Profile_relativelayout = (RelativeLayout) findViewById(R.id.edit_profile_relativelayout);
        upload_Orders_relativelayout = (RelativeLayout) findViewById(R.id.upload_orders_relativelayout);
        change_Password_layout = (RelativeLayout) findViewById(R.id.change_password_layout);
        Logout_layout = (RelativeLayout) findViewById(R.id.logout_layout);
        continue_Oreder = (RelativeLayout) findViewById(R.id.continue_order);

        wish_List_layout = (RelativeLayout) findViewById(R.id.wish_list_layout);
        fave_List_layout = (RelativeLayout) findViewById(R.id.fave_list_layout);
        Account_name =(TextView) findViewById(R.id.account_name);
        Cart_Icon  =(ImageView) findViewById(R.id.cart_icon);

        Account_name.setText("Hello "+myshare.getString("name",""));

        //ShowSelection();
        continue_Oreder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dealer_Account_Activity.this,MainActivity.class));
            }
        });


        Pending_orders_relativelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dealer_Account_Activity.this, Dealer_Pending_Order_Activity.class));

            }
        });

        Completed_order_relativelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dealer_Account_Activity.this, Dealer_Completed_Order_Activity.class));

            }
        });

        edit_Profile_relativelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dealer_Account_Activity.this, EditProfileActivity.class).putExtra("from","account"));

            }
        });

        upload_Orders_relativelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dealer_Account_Activity.this, DealerUploadOrdersActivity.class));

            }
        });

        change_Password_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dealer_Account_Activity.this, ChangePasswordActivity.class));

            }
        });

        wish_List_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dealer_Account_Activity.this, WishListActivity.class));

            }
        });
        Logout_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(Dealer_Account_Activity.this);
                alertDialog.setMessage("Logout successfully !");
                alertDialog.setTitle("WABCO");
                alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(Dealer_Account_Activity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                        edit.putBoolean("islogin", false).commit();

                    }
                });
                alertDialog.show();
            }
        });

        fave_List_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Dealer_Account_Activity.this, Favorite_DistributorActivity.class).putExtra("for","remove"));
            }
        });

        Cart_Icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CartDAO cartDAO = new CartDAO(getApplicationContext());
                CartDTO cartDTO = cartDAO.GetCartItems();
                if(cartDTO.getPartCodeList() == null)
                {
                    StyleableToast st =
                            new StyleableToast(getApplicationContext(), "Cart is Empty !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else
                {
                    startActivity(new Intent(getApplicationContext(), Quick_Order_Preview_Activity.class).putExtra("from","CartItem"));
                }
            }
        });


        MyCustomPagerAdapter adapter = new MyCustomPagerAdapter(Dealer_Account_Activity.this);
        myPager = (ViewPager) findViewById(R.id.viewpager);

        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);

        myPager.setAdapter(adapter);
        myPager.setCurrentItem(0);

        final float density = getResources().getDisplayMetrics().density;
        indicator.setViewPager(myPager);
        indicator.setRadius(5 * density);


        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                myPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);


// Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });


        final ImageView menu = (ImageView) findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(Dealer_Account_Activity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(Dealer_Account_Activity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(Dealer_Account_Activity.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(Dealer_Account_Activity.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(Dealer_Account_Activity.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(Dealer_Account_Activity.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(Dealer_Account_Activity.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(Dealer_Account_Activity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(Dealer_Account_Activity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(Dealer_Account_Activity.this, PriceListActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(Dealer_Account_Activity.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });


    }


    private void ShowSelection() {

        final AlertDialog alertDialog = new AlertDialog.Builder(Dealer_Account_Activity.this).create();

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        alertDialog.setView(dialogView);
        alertDialog.setTitle("WABCO");
        LinearLayout quick = (LinearLayout) dialogView.findViewById(R.id.quick_order);
        LinearLayout catalogue = (LinearLayout) dialogView.findViewById(R.id.catalogue_order);

        quick.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
        catalogue.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
                startActivity(new Intent(Dealer_Account_Activity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|(Intent.FLAG_ACTIVITY_NEW_TASK)));

            }
        });
        alertDialog.show();

    }


}
