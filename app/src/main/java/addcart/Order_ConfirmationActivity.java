package addcart;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.mobileordering.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import adapter.Order_confirm_Adapter;
import alertbox.Alertbox;
import api.APIService;
import api.ApiUtils;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import login.UserLoginActivity;
import models.quickorder.request.Order;
import models.quickorder.request.Order_Parts;
import models.quickorder.request.QuickOrder;
import models.quickorder.response.OrderResponse;
import notification.NotificationActivity;
import okhttp3.OkHttpClient;
import pekit.PE_Kit_Activity;
import persistence.DBHelper;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import quickorder.Quick_Order_Preview_Activity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import search.SearchActivity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;


public class Order_ConfirmationActivity extends AppCompatActivity {

  private Button Confirm;
  private SharedPreferences Distributor_share;
  private SharedPreferences.Editor Distriibutor_edit;
  private TextView Distributor_Address, Distributor_name;
  private ListView listview;
  private ProgressDialog progressDialog;
  private SQLiteDatabase db;
  private Cursor c;
  private DBHelper dbHelper;
  private ArrayList<String> PartNumList, DescriptionList, QuantityList, PartNameList, PartCodeList;
  private ArrayList<Integer> Pricelist, TotalList;
  private String GrandTotal;
  private TextView totalMrp;
  private String ComeFrom;
  private QuickOrder quickOrder = new QuickOrder();
  private SharedPreferences Dealer_share;
  private SharedPreferences.Editor Dealer_edit;
  private Alertbox box = new Alertbox(Order_ConfirmationActivity.this);
  private Order_confirm_Adapter Adapter;
  private CartDTO cartDTO = new CartDTO();
  private String TotalPartsQty;
  private ImageView back;
  private ImageView Cart_Icon;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_order__confirmation);


    ComeFrom = getIntent().getStringExtra("from");

    Confirm = (Button) findViewById(R.id.confirm);
    Distributor_Address = (TextView) findViewById(R.id.distributor_address);
    Distributor_name = (TextView) findViewById(R.id.distributor_name);
    totalMrp = (TextView) findViewById(R.id.totalMrp);
    listview = (ListView) findViewById(R.id.listView);
    Cart_Icon = (ImageView) findViewById(R.id.cart_icon);
    progressDialog = new ProgressDialog(Order_ConfirmationActivity.this);

    PartNumList = new ArrayList<String>();
    DescriptionList = new ArrayList<String>();
    QuantityList = new ArrayList<String>();
    PartNameList = new ArrayList<String>();
    PartCodeList = new ArrayList<String>();
    Pricelist = new ArrayList<Integer>();
    TotalList = new ArrayList<Integer>();

    dbHelper = new DBHelper(Order_ConfirmationActivity.this);

    Distributor_share = getSharedPreferences("distributor_address", MODE_PRIVATE);
    Distriibutor_edit = Distributor_share.edit();

    Dealer_share = getSharedPreferences("registration", MODE_PRIVATE);
    Dealer_edit = Dealer_share.edit();

    Distributor_Address
            .setText(Distributor_share.getString("distributor_address", "No Distributor"));
    Distributor_name.setText(Distributor_share.getString("distributor_name", "No Distributor"));

    Cart_Icon.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        CartDAO cartDAO = new CartDAO(getApplicationContext());
        CartDTO cartDTO = cartDAO.GetCartItems();
        if(cartDTO.getPartCodeList() == null)
        {
          StyleableToast st =
                  new StyleableToast(getApplicationContext(), "Cart is Empty !", Toast.LENGTH_SHORT);
          st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();
        }else
        {
          startActivity(new Intent(getApplicationContext(), Quick_Order_Preview_Activity.class).putExtra("from","CartItem"));
        }
      }
    });

    Confirm.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (!Dealer_share.getBoolean("islogin", false)) {
          startActivity(new Intent(Order_ConfirmationActivity.this, UserLoginActivity.class)
                  .putExtra("from", "order"));
        } else {
          GetPOJOFor_DistributorAndDealers();
          GetPOJOFor_OrderedParts(
                  "select partnumber, partname ,description , quantity, price , quantity*price as total  from "
                          + ComeFrom + " ");
          Post_Order();

        }

      }
    });

    new GetPreviewDetails().execute(
            "select partnumber,partcode, partname ,description , quantity, price , quantity*price as total  from "
                    + ComeFrom + " ");


    this.back = (ImageView) findViewById(R.id.back);
    this.back.setOnClickListener(new View.OnClickListener() {


      public void onClick(View arg0) {
        onBackPressed();
      }
    });
    final ImageView menu = (ImageView) findViewById(R.id.menu);
    menu.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        Context wrapper = new ContextThemeWrapper(Order_ConfirmationActivity.this, R.style.PopupMenu);
        final PopupMenu pop = new PopupMenu(wrapper, v);
        pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

          public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
              case R.id.home:
                startActivity(new Intent(Order_ConfirmationActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
              case R.id.search:
                startActivity(new Intent(Order_ConfirmationActivity.this, SearchActivity.class));
                break;
              case R.id.notification:
                startActivity(new Intent(Order_ConfirmationActivity.this, NotificationActivity.class));
                break;
              case R.id.vehicle:
                startActivity(new Intent(Order_ConfirmationActivity.this, VehicleMakeActivity.class));
                break;
              case R.id.product:
                startActivity(new Intent(Order_ConfirmationActivity.this, ProductFamilyActivity.class));
                break;
              case R.id.performance:
                startActivity(new Intent(Order_ConfirmationActivity.this, PE_Kit_Activity.class));
                break;
              case R.id.contact:
                startActivity(new Intent(Order_ConfirmationActivity.this, Network_Activity.class));
                break;
              case R.id.askwabco:
                startActivity(new Intent(Order_ConfirmationActivity.this, AskWabcoActivity.class));
                break;
              case R.id.pricelist:
                startActivity(new Intent(Order_ConfirmationActivity.this, PriceListActivity.class));
                break;
              case R.id.update:
                WabcoUpdate update = new WabcoUpdate(Order_ConfirmationActivity.this);
                update.checkVersion();
                break;
            }
            return false;
          }
        });
        pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

          @Override
          public void onDismiss(PopupMenu arg0) {
            // TODO Auto-generated method stub
            pop.dismiss();
          }
        });

        pop.inflate(R.menu.main);
        pop.show();
      }
    });



  }


  private void GetPOJOFor_DistributorAndDealers() {

    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    Date date = new Date();
    System.out.println(dateFormat.format(date)); // 2016/11/16 12:08:43

    Order order = new Order();
    order.setOrderDate(dateFormat.format(date));
    order.setOrderedAddress(Dealer_share.getString("brainmagic", "no address"));
    order.setOrderedByName(Dealer_share.getString("name", "no name"));
    order.setOrderedMobile(Dealer_share.getString("phone", "no phone"));
    order.setOrderedPincode(Dealer_share.getString("pincode", "no pincode"));

    if (!Distributor_share.getString("distributor_id", "1L").equals(""))
      // Distributor Id
      order.setDistributorid(Long.parseLong(Distributor_share.getString("distributor_id", "1L")));

    // Dealer
    order.setDealerid(Long.parseLong(Dealer_share.getString("id", "1L")));
    order.setDeliveryAddress(Dealer_share.getString("brainmagic", "no address"));
    order.setDeliveryMobile(Dealer_share.getString("phone", "no phone"));
    order.setDeliveryName(Dealer_share.getString("name", "no name"));
    order.setDeliveryStatus("Pending");
    order.setActive("Y");
    order.setTotalOrderAmount((long) Float.parseFloat(GrandTotal));
    order.setOrderStatus("New");
    order.setTotalOrderedQty(TotalPartsQty);
    quickOrder.setOrder(order);

  }

  private void GetPOJOFor_OrderedParts(String query) {

    Order_Parts[] order_parts;
    db = dbHelper.readDataBase();

    try {
      Log.v("GetPOJOFor_OrderedParts", query);
      c = db.rawQuery(query, null);
      int i = 0;
      order_parts = new Order_Parts[c.getCount()];
      if (c.moveToFirst()) {
        do {
          order_parts[i] = new Order_Parts();
          order_parts[i].setPartName(c.getString(c.getColumnIndex("partname")));
          order_parts[i].setPartNumber(ChangeToHundred(c.getString(c.getColumnIndex("partnumber"))));
          order_parts[i].setDescription(c.getString(c.getColumnIndex("description")));
          order_parts[i].setQuantity(c.getString(c.getColumnIndex("quantity")));
          order_parts[i].setPartPrice(Integer.toString(c.getInt(c.getColumnIndex("price"))));
          order_parts[i].setTotalAmount(Integer.toString(c.getInt(c.getColumnIndex("total"))));
          order_parts[i].setDeliveryStatus("Pending");
          order_parts[i].setActive("Y");

          i++;
        } while (c.moveToNext());
        quickOrder.setOrder_Parts(order_parts);
        c.close();
        db.close();
        dbHelper.close();
      } else {
        c.close();
        db.close();
        dbHelper.close();
      }
    } catch (Exception e) {
      Log.v("Error order parts", e.getMessage());
      c.close();
      db.close();
      dbHelper.close();

    }


  }

  public String ChangeToHundred(String partnumber)
  {
   return partnumber.replace("M ","100");
  }




  private class GetPreviewDetails extends AsyncTask<String, Void, String> {

    private Cursor cursor;


    protected void onPreExecute() {
      super.onPreExecute();
      progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
      progressDialog.setCancelable(false);
      progressDialog.setMessage("Loading...");
      progressDialog.show();
      db = dbHelper.readDataBase();

    }

    @Override
    protected String doInBackground(String... params) {
      // TODO Auto-generated method stub
      Log.v("OFF Line", "SQLITE Table");
      try {
        String query = params[0];
        Log.v("confirm query", query);
        cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
          do {
            PartNameList.add(cursor.getString(cursor.getColumnIndex("partname")));
            PartNumList.add(cursor.getString(cursor.getColumnIndex("partnumber")));
            DescriptionList.add(cursor.getString(cursor.getColumnIndex("description")));
            QuantityList.add(cursor.getString(cursor.getColumnIndex("quantity")));
            Pricelist.add(cursor.getInt(cursor.getColumnIndex("price")));
            TotalList.add(cursor.getInt(cursor.getColumnIndex("total")));
            PartCodeList.add(cursor.getString(cursor.getColumnIndex("partcode")));


          } while (cursor.moveToNext());

          cartDTO.setPartNameList(PartNameList);
          cartDTO.setPartNoList(PartNumList);
          cartDTO.setPartdescriptionList(DescriptionList);
          cartDTO.setPartQuantityList(QuantityList);
          cartDTO.setPartImageList(PartNumList);
          cartDTO.setPartCodeList(PartCodeList);
          cartDTO.setPartTypeList(PartNameList);
          List<String> newPriceList = new ArrayList<String>(Pricelist.size());
          for (Integer myInt : Pricelist) {
            newPriceList.add(String.valueOf(myInt) + ".00");
          }
          cartDTO.setPartPriceList(newPriceList);

          GetGrandTotal();
          GetTotalOrderedQTY();

          cursor.close();
          db.close();
          return "received";
        } else {
          cursor.close();
          db.close();
          dbHelper.close();
          return "nodata";
        }
      } catch (Exception e) {
        cursor.close();
        db.close();
        dbHelper.close();
        Log.v("Error in Incentive", e.getMessage());
        return "notsuccess";
      }
    }

    private void GetTotalOrderedQTY() {

      String query = "select sum(quantity) as TotalQty from " + ComeFrom + "";
      Log.v("TotalQty qurey ", query);
      cursor = db.rawQuery(query, null);
      if (cursor.moveToFirst()) {
        do {
          TotalPartsQty = cursor.getString(cursor.getColumnIndex("TotalQty"));
        } while (cursor.moveToNext());

      }
    }

    private void GetGrandTotal() {

      String query = "select sum(quantity*price) as Total  from " + ComeFrom + "";
      Log.v("GrandTotal qurey ", query);
      cursor = db.rawQuery(query, null);
      if (cursor.moveToFirst()) {
        do {
          GrandTotal = cursor.getString(cursor.getColumnIndex("Total"));
          GrandTotal = GrandTotal + ".00";

        } while (cursor.moveToNext());

      }
    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      progressDialog.dismiss();

      switch (result) {
        case "received":
          Adapter = new Order_confirm_Adapter(Order_ConfirmationActivity.this, PartNameList,
                  PartNumList, DescriptionList, QuantityList, Pricelist, TotalList);
          listview.setAdapter(Adapter);

          totalMrp.setText(getResources().getString(R.string.total_amount) + " "
                  + getResources().getString(R.string.Rs) + " " + GrandTotal);

          break;
        case "nodata":

          final AlertDialog.Builder alertDialog =
                  new AlertDialog.Builder(Order_ConfirmationActivity.this);
          alertDialog.setTitle("Brain Magic");
          alertDialog.setMessage(R.string.no_orders_alert);
          alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
              startActivity(new Intent(Order_ConfirmationActivity.this, MainActivity.class)
                      .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
            }
          });

          alertDialog.show();

          break;
        default:

          break;
      }

    }

  }


  private void Post_Order() {
    final ProgressDialog loading =
            ProgressDialog.show(this, "Placing order", "Please wait...", false, false);
    OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(100, TimeUnit.SECONDS)
            .readTimeout(100,TimeUnit.SECONDS).build();
    Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL).client(client)
            .addConverterFactory(GsonConverterFactory.create()).build();

    APIService api = retrofit.create(APIService.class);
    Call<OrderResponse> quickOrderCall = api.PostOrderDetails(quickOrder);

    quickOrderCall.enqueue(new Callback<OrderResponse>() {
      @Override
      public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
        loading.dismiss();
        if (response.isSuccessful()) {

          Log.v("Result", response.body().getResult());

          if (response.body().getResult().equals("Success")) {
            PlacedSuccessfull(response.body().getData());
          } else {
            PlacedFailed(getString(R.string.save_order_in_offline));
          }

        } else {
          PlacedFailed(getString(R.string.save_order_in_offline));
        }
      }

      @Override
      public void onFailure(Call<OrderResponse> call, Throwable t) {

        loading.dismiss();
        t.printStackTrace();
        PlacedFailed(getString(R.string.save_order_in_offline));
      }
    });
  }

  private void PlacedFailed(String msg) {

    final AlertDialog.Builder alertDialog =
            new AlertDialog.Builder(Order_ConfirmationActivity.this);
    alertDialog.setMessage(msg);
    alertDialog.setTitle("Brain Magic");
    alertDialog.setCancelable(false);
    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
        SaveLocalOrders();
        Delete_QuickOrder();
      }

    });
    alertDialog.setNegativeButton("Try again", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {

        Post_Order();
      }
    });

    alertDialog.show();

  }

  private void SaveLocalOrders() {
    CartDAO cartDAO = new CartDAO(Order_ConfirmationActivity.this);
    cartDAO.AddToLocalOrder(quickOrder, Distributor_share.getString("distributor_name", "No Distributor"),Distributor_share.getString("distributor_address", "No Distributor"));
  }

  private void PlacedSuccessfull(String ordernumber) {
    final AlertDialog.Builder alertDialog =
            new AlertDialog.Builder(Order_ConfirmationActivity.this);
    alertDialog.setMessage(getString(R.string.order_successfull) + ordernumber);
    alertDialog.setTitle("Brain Magic");
    alertDialog.setCancelable(false);
    alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();
      /*  Adapter.clear();
        Adapter.notifyDataSetChanged();
        totalMrp.setText(getResources().getString(R.string.total_amount) + " "
                + getResources().getString(R.string.Rs) + " " + "0.00");*/
        Confirm.setEnabled(false);
        Delete_QuickOrder();
        ShowInfoDialog();
      }

    });
    alertDialog.show();
  }



  private void ShowInfoDialog()
  {
    LayoutInflater layoutInflaterAndroid = LayoutInflater.from(Order_ConfirmationActivity.this);
    View mView = layoutInflaterAndroid.inflate(R.layout.alert_part_nfo_dialog, null);
    AlertDialog.Builder alertDialogBuilderUserInput =
            new AlertDialog.Builder(Order_ConfirmationActivity.this);
    alertDialogBuilderUserInput.setView(mView);
    alertDialogBuilderUserInput.setCancelable(false).setTitle("Brain Magic");

    alertDialogBuilderUserInput.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialogBox, int id) {
        // ToDo get user input here
        dialogBox.dismiss();
        ShowWishListAlert();
      }
    });

    AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
    alertDialogAndroid.show();
  }


  private void ShowWishListAlert() {

    final AlertDialog.Builder alertDialog =
            new AlertDialog.Builder(Order_ConfirmationActivity.this);
    alertDialog.setMessage(R.string.saving_to_wishlist);
    alertDialog.setTitle("Brain Magic");
    alertDialog.setCancelable(false);
    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {
        dialog.dismiss();

        SaveWhislListItems();
      }

    });
    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
      public void onClick(DialogInterface dialog, int id) {

        startActivity(new Intent(Order_ConfirmationActivity.this, MainActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
      }
    });
    alertDialog.show();

  }

  private void SaveWhislListItems() {
    CartDAO cartDAO = new CartDAO(Order_ConfirmationActivity.this);
    cartDAO.addWishListFromQuickOrder(cartDTO);
  }

  private void Delete_QuickOrder()
  {try {
    db = dbHelper.readDataBase();
    db.execSQL("delete from " + ComeFrom + "");
    db.close();
  }catch (Exception ex)
  {
    Log.v("Delete", ex.getMessage());
  }
  }
}
