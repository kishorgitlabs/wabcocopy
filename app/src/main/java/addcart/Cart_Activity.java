/*
package addcart;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.eminayar.panter.DialogType;
import com.eminayar.panter.PanterDialog;
import com.eminayar.panter.interfaces.OnSingleCallbackConfirmListener;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.wabco.brainmagic.wabco.demo.R;

import address.distributoraddress.Select_DistributorActivity;
import butterknife.ButterKnife;
import butterknife.InjectView;

public class Cart_Activity extends AppCompatActivity {

    @InjectView(R.id.continue_order)
    Button continueOrder;
    @InjectView(R.id.checkout)
    Button checkOut;
    @InjectView(R.id.cart_item_layout)
    View cart;
    @InjectView(R.id.wishlist)
    Button wishList;
    @InjectView(R.id.remove)
    Button remove;
    @InjectView(R.id.back)
    ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.inject(this);




        wishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new PanterDialog(Cart_Activity.this)
                        .setHeaderBackground(R.color.lite_white)
                        .setHeaderLogo(R.drawable.logo)
                        .setDialogType(DialogType.SINGLECHOICE)
                        .isCancelable(false)
                        .items(R.array.sample_array, new OnSingleCallbackConfirmListener() {
                            @Override
                            public void onSingleCallbackConfirmed(PanterDialog dialog, int pos, String text) {

                                StyleableToast st = new StyleableToast(Cart_Activity.this, " M 100701 Added to " + text, Toast.LENGTH_SHORT);
                                st.setBackgroundColor(Cart_Activity.this.getResources().getColor(R.color.green));
                                st.setTextColor(Color.WHITE);
                                st.setMaxAlpha();
                                st.show();

                            }
                        })
                        .show();

            }
        });


        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StyleableToast st = new StyleableToast(Cart_Activity.this, " M 100701 removed from cart", Toast.LENGTH_SHORT);
                st.setBackgroundColor(Cart_Activity.this.getResources().getColor(R.color.green));
                st.setTextColor(Color.WHITE);
                st.setMaxAlpha();
                st.show();
            }
        });


        continueOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        checkOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Cart_Activity.this, Select_DistributorActivity.class).putExtra("from", "CartItem"));
            }
        });



    }
}
*/
