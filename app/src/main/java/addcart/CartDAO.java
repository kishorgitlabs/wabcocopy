package addcart;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.kennyc.bottomsheet.BottomSheet;
import com.kennyc.bottomsheet.BottomSheetListener;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.mobileordering.R;

import java.util.ArrayList;
import java.util.List;

import alertbox.Alertbox;
import home.MainActivity;
import models.distributor.DistributorData;
import models.quickorder.request.QuickOrder;
import persistence.DBHelper;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by system01 on 6/7/2017.
 */

public class CartDAO {
  private SharedPreferences myshare;
  private SharedPreferences.Editor edit;
  private Context context;
  private SQLiteDatabase db;
  private DBHelper dbHelper;
  private Alertbox box;
  // CartItem Table Columns
  public static final String CART_TABLE_NAME = "CartItem";
  public static final String COLUMN_PARTID = "partid";
  public static final String COLUMN_PART_NAME = "partname";
  public static final String COLUMN_PART_NUMBER = "partnumber";
  public static final String COLUMN_PART_CODE = "partcode";
  public static final String COLUMN_PART_TYPE = "parttype";
  public static final String COLUMN_QUANTITY = "quantity";
  public static final String COLUMN_DESCRIPTION = "description";
  public static final String COLUMN_PRICE = "price";
  public static final String COLUMN_IMAGENAME = "image";
  public static final String COLUMN_QUICKID = "quickid";

  // WishlistItem Table Columns
  public static final String WISHITEMS_TABLE_NAME = "WishItems";
  // WishlistItem Table Columns
  public static final String WISH_TABLE_NAME = "WishLists";
  public static final String COLUMN_WISH_ID = "WishId";

  public static final String COLUMN_WISH_NAME = "wishname";

  // QuickOrder Table Columns
  public static final String QUICK_TABLE_NAME = "QuickOrder";

  // FAVORITE_DISTRIBUTOR_TABLE_NAME Table Columns
  public static final String FAVORITE_DISTRIBUTOR_TABLE_NAME = "FavoriteDistributor";
  public static final String COLUMN_ID = "id";
  public static final String COLUMN_NAME = "name";
  public static final String COLUMN_MOBILE = "mobile";
  public static final String COLUMN_EMAIL = "email";
  public static final String COLUMN_STATE = "state";
  public static final String COLUMN_CITY = "city";
  public static final String COLUMN_COUNTRY = "country";
  public static final String COLUMN_ADDRESS = "address";


  // Local order table
  public static final String LOCAL_ORDER_TABLE_NAME = "Offlineorders";
  public static final String COLUMN_DEALER_ID = "dealer_id";
  public static final String COLUMN_DELIVERY_NAME = "delivery_name";
  public static final String COLUMN_DELIVERY_MOBILE = "delivery_mobile";
  public static final String COLUMN_DELIVERY_ADDRESS = "delivery_address";
  public static final String COLUMN_ORDERED_NAME = "ordered_name";
  public static final String COLUMN_ORDERED_MOBILE = "ordered_mobile";
  public static final String COLUMN_ORDERED_ADDRESS = "ordered_address";
  public static final String COLUMN_ORDERED_DATE = "ordered_date";
  public static final String COLUMN_ORDERED_STATUS = "ordered_status";
  public static final String COLUMN_DELIVERY_STATUS = "delivery_status";
  public static final String COLUMN_TOTAL_ORDEREDQTY = "ordered_totalqty";
  public static final String COLUMN_ACTIVE = "active";

  public static final String COLUMN_DIS_NAME = "dis_name";
  public static final String COLUMN_DIS_ADDRESS = "dis_address";
  public static final String COLUMN_DISTRIBUTOR_CODE = "distributor_id";


  private ArrayList<String> wishID;
  private boolean defaultNameCreated;


  public CartDAO(Context context) {
    this.context = context;
    this.dbHelper = new DBHelper(context);

    myshare = context.getSharedPreferences("registration", MODE_PRIVATE);
    edit = myshare.edit();
    defaultNameCreated = myshare.getBoolean("defaultnameadded", false);

    CreateCartTable();
     CreateWishTable();
    CreateWishItemsTable();
    CreateQuickOrderTable();
    CreateFavoriteDistributorTable();
    CreateLocalOrderTable();

    box = new Alertbox(context);

  }



  public void openDatabase() {
    this.db = this.dbHelper.readDataBase();
  }

  public void closeDatabase() {
    if (this.db != null) {
      this.db.close();
    }
  }


  public void CreateCartTable() {
    openDatabase();
    db.execSQL("create table if not exists " + CART_TABLE_NAME + " ( " + COLUMN_PARTID
        + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT  ," + COLUMN_PART_NAME + " VARCHAR, "
        + COLUMN_PART_NUMBER + " VARCHAR ," + COLUMN_PART_CODE + " VARCHAR ," + COLUMN_PART_TYPE
        + " VARCHAR ," + COLUMN_PRICE + " INTEGER ," + COLUMN_QUANTITY + " INTEGER ,"
        + COLUMN_DESCRIPTION + " VARCHAR ," + COLUMN_IMAGENAME + " VARCHAR );");

    Log.v("cart table ", "created");
  }

  public void CreateWishTable() {
    openDatabase();
    db.execSQL("create table if not exists " + WISH_TABLE_NAME + " ( " + COLUMN_WISH_ID
        + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT ," + COLUMN_WISH_NAME + " VARCHAR );");
    Log.v("wish table ", "created");


    if (!defaultNameCreated) {
      ContentValues contentValue = new ContentValues();
      contentValue.put(COLUMN_WISH_NAME, myshare.getString("name", ""));
      db.insert(WISH_TABLE_NAME, null, contentValue);
      defaultNameCreated = true;
      edit.putBoolean("defaultnameadded", defaultNameCreated).commit();
    }

  }

  public void CreateWishItemsTable() {
    openDatabase();
    db.execSQL("create table if not exists " + WISHITEMS_TABLE_NAME + " ( " + COLUMN_PARTID
        + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT ," + COLUMN_WISH_ID + " INTEGER,"
        + COLUMN_PART_NAME + " VARCHAR, " + COLUMN_PART_NUMBER + " VARCHAR ," + COLUMN_PART_CODE
        + " VARCHAR ," + COLUMN_PART_TYPE + " VARCHAR ," + COLUMN_PRICE + " INTEGER ,"
        + COLUMN_DESCRIPTION + " VARCHAR ," + COLUMN_IMAGENAME + " VARCHAR );");

    Log.v("wish items table ", "created");



  }

  public void CreateQuickOrderTable() {
    openDatabase();
    db.execSQL("create table if not exists " + QUICK_TABLE_NAME + " ( " + COLUMN_PARTID
        + " INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT  ," + COLUMN_PART_NAME + " VARCHAR, "
        + COLUMN_PART_NUMBER + " VARCHAR ," + COLUMN_PART_CODE + " VARCHAR ," + COLUMN_PART_TYPE
        + " VARCHAR ," + COLUMN_PRICE + " INTEGER ," + COLUMN_QUANTITY + " INTEGER ,"
        + COLUMN_DESCRIPTION + " VARCHAR ," + COLUMN_IMAGENAME + " VARCHAR );");

    Log.v("Quickorder table ", "created");
  }


  private void CreateFavoriteDistributorTable() {

    openDatabase();
    db.execSQL("create table if not exists " + FAVORITE_DISTRIBUTOR_TABLE_NAME + " ( " + COLUMN_ID
        + " VARCHAR ," + COLUMN_NAME + " VARCHAR, " + COLUMN_MOBILE + " VARCHAR ," + COLUMN_EMAIL
        + " VARCHAR ," + COLUMN_STATE + " VARCHAR ," + COLUMN_CITY + " INTEGER ," + COLUMN_COUNTRY
        + " INTEGER ," + COLUMN_ADDRESS + " VARCHAR );");

//    Log.v("FavoriteDistributor table ", "created");

  }

  private void CreateLocalOrderTable() {
    openDatabase();
    db.execSQL("create table if not exists " + LOCAL_ORDER_TABLE_NAME + " ( " + COLUMN_DEALER_ID
        + " VARCHAR ," + COLUMN_DELIVERY_NAME + " VARCHAR, " + COLUMN_DELIVERY_MOBILE + " VARCHAR ,"
        + COLUMN_DELIVERY_ADDRESS + " VARCHAR ," + COLUMN_DELIVERY_STATUS + " VARCHAR ,"
        + COLUMN_ORDERED_NAME + " VARCHAR ," + COLUMN_ORDERED_MOBILE + " VARCHAR ,"
        + COLUMN_ORDERED_ADDRESS + " VARCHAR," + COLUMN_DISTRIBUTOR_CODE + " VARCHAR ,"
        + COLUMN_ORDERED_DATE + " VARCHAR ," + COLUMN_ORDERED_STATUS + " VARCHAR ,"
        + COLUMN_TOTAL_ORDEREDQTY + " VARCHAR ," + COLUMN_PART_NAME + " VARCHAR ,"
        + COLUMN_PART_CODE + " VARCHAR ," + COLUMN_PART_NUMBER + " VARCHAR ," + COLUMN_PART_TYPE
        + " VARCHAR ," + COLUMN_DESCRIPTION + " VARCHAR ," + COLUMN_PRICE + " INTEGER ,"
        + COLUMN_QUANTITY + " INTEGER ," + COLUMN_DIS_NAME + " VARCHAR ," + COLUMN_DIS_ADDRESS
        + " VARCHAR ," + COLUMN_IMAGENAME + " VARCHAR );");

    Log.v("SaveLocal  table ", "created");
  }



  /* Part items functios */

  public String GetPrice(String partno) {
    // TODO Auto-generated method stub
    openDatabase();

    String query = "select mrplist from pricelist where Part_No ='" + partno + "' or Partcode  ='"
        + partno + "'";

    Cursor cursor = this.db.rawQuery(query, null);
    if (cursor.moveToFirst())
    {
      return cursor.getString(cursor.getColumnIndex("mrplist")).replace(",","");
    }
    else {
      return "0";
    }

  }

  public String GetPartCode(String partno) {
    // TODO Auto-generated method stub
    openDatabase();

    String query = "select pricelist from pricelist where Partcode ='" + partno + " '";

    Cursor cursor = this.db.rawQuery(query, null);
    if (cursor.moveToFirst()) {
      return cursor.getString(cursor.getColumnIndex("Partcode"));
    } else {
      return "";
    }

  }

  public String GetCatagory(String partno) {
    // TODO Auto-generated method stub
    openDatabase();
    String query = "select category from pricelist where Part_No ='" + partno + "' or Partcode  ='"
        + partno + "'";
    Cursor cursor = this.db.rawQuery(query, null);
    if (cursor.moveToFirst()) {
      return cursor.getString(cursor.getColumnIndex("category"));
    } else {
      return "";
    }

  }
  /*-----------------------------------------------------------*/

  /* Add to cart fuctions */
  public void addToCart(CartDTO cartDTO, int position) {
    openDatabase();
    try {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_PART_NAME, cartDTO.getPartNameList().get(position));
        contentValues.put(COLUMN_PART_NUMBER, cartDTO.getPartNoList().get(position));
        contentValues.put(COLUMN_PART_TYPE, GetCatagory(cartDTO.getPartNoList().get(position)));
        contentValues.put(COLUMN_PART_CODE, GetPartCode(cartDTO.getPartNoList().get(position)));
        contentValues.put(COLUMN_PRICE, GetPrice(cartDTO.getPartNoList().get(position)));
        contentValues.put(COLUMN_QUANTITY, GetCartQuantity(cartDTO.getPartNoList().get(position)) + 1);
        contentValues.put(COLUMN_DESCRIPTION, cartDTO.getPartdescriptionList().get(position));
        contentValues.put(COLUMN_IMAGENAME, cartDTO.getPartImageList().get(position));

        if (GetCartQuantity(cartDTO.getPartNoList().get(position)) == 0) {
            db.insert(CART_TABLE_NAME, null, contentValues);
            closeDatabase();
        } else {
            UpdateCartQuantity(cartDTO.getPartNoList().get(position),
                    Integer.toString(GetCartQuantity(cartDTO.getPartNoList().get(position))));
        }
    }catch (NumberFormatException e){

    }

    StyleableToast st = new StyleableToast(context,
        cartDTO.getPartNoList().get(position) + " added to cart !", Toast.LENGTH_SHORT);
    st.setBackgroundColor(context.getResources().getColor(R.color.green));
    st.setTextColor(Color.WHITE);
    st.setMaxAlpha();
    st.show();

  }


  public int GetCartQuantity(String partno) {
    // TODO Auto-generated method stub
    openDatabase();
    String query = "select quantity from CartItem where partnumber ='" + partno + "'";
    Cursor cursor = this.db.rawQuery(query, null);
    if (cursor.moveToFirst()) {
      return Integer.parseInt(cursor.getString(cursor.getColumnIndex("quantity")));
    } else {
      return 0;
    }

  }

  public void UpdateCartQuantity(String partno, String qty) {
    // TODO Auto-generated method stub
    qty = Integer.toString(Integer.parseInt(qty) + 1);
    openDatabase();
    ContentValues cv = new ContentValues();
    cv.put(COLUMN_QUANTITY, qty); // these Fields should be your String values of actual column
    // names
    db.update(CART_TABLE_NAME, cv, "partnumber='" + partno + "'", null);

  }
  /*-----------------------------------------------------------*/



  /* Quick order fuctions */
  public long addQuickOrder(CartDTO cartDTO, int qty) {
    openDatabase();
    long success = 0;
    ContentValues contentValues = new ContentValues();
    contentValues.put(COLUMN_PART_NAME, cartDTO.getPartTypeList().get(0));
    contentValues.put(COLUMN_PART_NUMBER, cartDTO.getPartNoList().get(0));
    contentValues.put(COLUMN_PART_CODE, cartDTO.getPartCodeList().get(0));
    contentValues.put(COLUMN_PART_TYPE, GetCatagory(cartDTO.getPartNoList().get(0)));
    contentValues.put(COLUMN_PRICE, GetPrice(cartDTO.getPartNoList().get(0)));
    contentValues.put(COLUMN_QUANTITY, qty);
    contentValues.put(COLUMN_DESCRIPTION, cartDTO.getPartdescriptionList().get(0));
    contentValues.put(COLUMN_IMAGENAME, cartDTO.getPartImageList().get(0));

    if (CheckIsAlreadyAdded(cartDTO.getPartNoList().get(0))) {
      success = db.insert(QUICK_TABLE_NAME, null, contentValues);
      closeDatabase();
    } else {
      UpdateQuantity(cartDTO.getPartNoList().get(0), getItemQty(cartDTO.getPartNoList().get(0)),
          qty);
    }
    return success;

  }



  public boolean CheckIsAlreadyAdded(String partno) {
    // TODO Auto-generated method stub
    openDatabase();
    String query = "select partnumber from QuickOrder where partnumber ='" + partno + "'";
    Cursor cursor = this.db.rawQuery(query, null);
    if (cursor.moveToFirst()) {
      return false;
    } else {
      return true;
    }

  }

  private String getItemQty(String partno) {
    openDatabase();
    String qty = "1";
    String query = "select * from QuickOrder where partnumber ='" + partno + "'";
    Cursor cursor = this.db.rawQuery(query, null);
    if (cursor.moveToFirst()) {
      return Integer.toString(cursor.getInt(cursor.getColumnIndex(COLUMN_QUANTITY)));
    } else {
      return qty;
    }

  }

  public void UpdateQuantity(String partno, String qty, int enteredQty) {
    // TODO Auto-generated method stub
    qty = Integer.toString(Integer.parseInt(qty) + enteredQty);
    openDatabase();
    ContentValues cv = new ContentValues();
    cv.put(COLUMN_QUANTITY, qty); // these Fields should be your String values of actual column
                                  // names
    db.update(QUICK_TABLE_NAME, cv, "partnumber='" + partno + "'", null);

  }
  /*-----------------------------------------------------------*/


  /* Wish list Catalogue fuctions */
  public void addWishList(final CartDTO cartDTO, final int position) {
    openDatabase();
    final ContentValues contentValues = new ContentValues();
    ArrayList<String> wishNames = getWishNames();
    if (wishNames.size() == 0) {
      CreateWishItemsFromNormalCartDialog(cartDTO, position);
    } else {
      new BottomSheet.Builder(context).setSheet(R.menu.grid_sheet).grid().setColumnCount(2)
          .setCancelable(false).setTitle("Wish list options")
          .setListener(new BottomSheetListener() {
            @Override
            public void onSheetShown(@NonNull BottomSheet bottomSheet) {

          }

            @Override
            public void onSheetItemSelected(@NonNull BottomSheet bottomSheet, MenuItem menuItem) {

              if (menuItem.getTitle().equals("Create New")) {
                CreateWishItemsFromNormalCartDialog(cartDTO, position);
              } else {
                ShowWishItemsFromNormalCart(cartDTO, position);
              }


            }

            @Override
            public void onSheetDismissed(@NonNull BottomSheet bottomSheet, @DismissEvent int i) {

          }
          }).show();
    }
  }

  private void CreateWishItemsFromNormalCartDialog(final CartDTO cartDTO, final int position) {

    new MaterialDialog.Builder(context).title("Brain Magic").content("Create new wish name")
        .autoDismiss(false)
        .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT)
        .input("Enter wish name", "", new MaterialDialog.InputCallback() {
          @Override
          public void onInput(MaterialDialog dialog, CharSequence input) {
            // Do something

          }
        }).positiveText("Create new").negativeText("Add to Default")
        .onPositive(new MaterialDialog.SingleButtonCallback() {
          @Override
          public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
            if (dialog.getInputEditText().length() == 0) {
              StyleableToast st =
                  new StyleableToast(context, "Enter wish name !", Toast.LENGTH_SHORT);
              st.setBackgroundColor(context.getResources().getColor(R.color.red));
              st.setTextColor(Color.WHITE);
              st.setMaxAlpha();
              st.show();
            } else {
              if (CreateWishName(dialog.getInputEditText().getText().toString(), cartDTO,
                  position)) {
                dialog.dismiss();
                StyleableToast st = new StyleableToast(context,
                    "Part items added to " + dialog.getInputEditText().getText().toString(),
                    Toast.LENGTH_SHORT);
                st.setBackgroundColor(context.getResources().getColor(R.color.green));
                st.setTextColor(Color.WHITE);
                st.setMaxAlpha();
                st.show();
              } else {
                StyleableToast st = new StyleableToast(context,
                    "Another wishlist already exists in the same name !  ", Toast.LENGTH_SHORT);
                st.setBackgroundColor(context.getResources().getColor(R.color.red));
                st.setTextColor(Color.WHITE);
                st.setMaxAlpha();
                st.show();
              }

            }
          }
        }).onNegative(new MaterialDialog.SingleButtonCallback() {
          @Override
          public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
            dialog.dismiss();

            AddDefaultWishName(myshare.getString("name", ""), cartDTO, position);

            StyleableToast st = new StyleableToast(context,
                "Part items added to " + myshare.getString("name", ""), Toast.LENGTH_SHORT);
            st.setBackgroundColor(context.getResources().getColor(R.color.green));
            st.setTextColor(Color.WHITE);
            st.setMaxAlpha();
            st.show();
          }
        }).show();


  }

  private void ShowWishItemsFromNormalCart(final CartDTO cartDTO, final int position) {
    openDatabase();
    final ContentValues contentValues = new ContentValues();


    new AlertDialog.Builder(context)
        .setSingleChoiceItems(getWishNames().toArray(new String[getWishNames().size()]), 0, null)
        .setTitle("Choose your wish name").setCancelable(false)
        .setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int whichButton) {

            int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();

            // Do something useful withe the position of the selected radio button
            if (GetAlreadyAddedPartNumber(cartDTO.getPartNoList().get(position),
                wishID.get(selectedPosition))) {
              contentValues.put(COLUMN_WISH_ID, wishID.get(selectedPosition));
              contentValues.put(COLUMN_PART_NAME, cartDTO.getPartNameList().get(position));
              contentValues.put(COLUMN_PART_NUMBER, cartDTO.getPartNoList().get(position));
              contentValues.put(COLUMN_PART_CODE,
                  GetPartCode(cartDTO.getPartNoList().get(position)));
              contentValues.put(COLUMN_PART_TYPE,
                  GetCatagory(cartDTO.getPartNoList().get(position)));
              contentValues.put(COLUMN_PRICE, GetPrice(cartDTO.getPartNoList().get(position)));
              contentValues.put(COLUMN_DESCRIPTION, cartDTO.getPartdescriptionList().get(position));
              contentValues.put(COLUMN_IMAGENAME, cartDTO.getPartImageList().get(position));
              db.insert(WISHITEMS_TABLE_NAME, null, contentValues);


              StyleableToast st = new StyleableToast(context,
                  "Part item added to " + getWishNames().get(selectedPosition), Toast.LENGTH_SHORT);
              st.setBackgroundColor(context.getResources().getColor(R.color.green));
              st.setTextColor(Color.WHITE);
              st.setMaxAlpha();
              st.show();
            } else {
              StyleableToast st = new StyleableToast(context,
                  "This part already added to this wish name !  ", Toast.LENGTH_SHORT);
              st.setBackgroundColor(context.getResources().getColor(R.color.red));
              st.setTextColor(Color.WHITE);
              st.setMaxAlpha();
              st.show();
            }



          }
        }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {

        }
        }).show();



    closeDatabase();
  }


  private boolean CreateWishName(String s, final CartDTO cartDTO, final int position) {
    openDatabase();

    ContentValues contentValues = new ContentValues();
    if (CheckAlreadyAddedWishname(s)) {
      ContentValues contentValue = new ContentValues();
      contentValue.put(COLUMN_WISH_NAME, s);
      db.insert(WISH_TABLE_NAME, null, contentValue);

      contentValues.put(COLUMN_WISH_ID, getLastWishID());
      contentValues.put(COLUMN_PART_NAME, cartDTO.getPartNameList().get(position));
      contentValues.put(COLUMN_PART_NUMBER, cartDTO.getPartNoList().get(position));
      contentValues.put(COLUMN_PART_CODE, GetPartCode(cartDTO.getPartNoList().get(position)));
      contentValues.put(COLUMN_PART_TYPE, GetCatagory(cartDTO.getPartNoList().get(position)));
      contentValues.put(COLUMN_PRICE, GetPrice(cartDTO.getPartNoList().get(position)));
      contentValues.put(COLUMN_DESCRIPTION, cartDTO.getPartdescriptionList().get(position));
      contentValues.put(COLUMN_IMAGENAME, cartDTO.getPartImageList().get(position));
      db.insert(WISHITEMS_TABLE_NAME, null, contentValues);
      return true;
    } else {
      /*
       * StyleableToast st = new StyleableToast(context,
       * "Another wishlist already exists in the same name !  ", Toast.LENGTH_SHORT);
       * st.setBackgroundColor(context.getResources().getColor(R.color.red));
       * st.setTextColor(Color.WHITE); st.setMaxAlpha(); st.show();
       */
      return false;
    }

  }

  public boolean AddNewWishName(String s) {
    openDatabase();

    ContentValues contentValues = new ContentValues();
    if (CheckAlreadyAddedWishname(s)) {
      ContentValues contentValue = new ContentValues();
      contentValue.put(COLUMN_WISH_NAME, s);
      db.insert(WISH_TABLE_NAME, null, contentValue);
      return true;
    } else {
      return false;
    }
  }



  public boolean CheckAlreadyAddedWishname(String wishname) {
    // TODO Auto-generated method stub
    openDatabase();
    String query = "select * from WishLists where wishname ='" + wishname + "'";
    Cursor cursor = this.db.rawQuery(query, null);
    if (cursor.moveToFirst()) {
      return false;
    } else {
      return true;
    }

  }

  /*
   * public boolean CheckAlreadyAddedWishItem(String partnumber) { // TODO Auto-generated method
   * stub openDatabase(); String query = "select * from WishItems where partnumber ='" + partnumber
   * + "'"; Cursor cursor = this.db.rawQuery(query, null); if (cursor.moveToFirst()) { return false;
   * } else { return true; }
   * 
   * }
   */

  public boolean GetAlreadyAddedPartNumber(String partnumber, String id) {
    // TODO Auto-generated method stub
    openDatabase();
    String query =
        "select * from WishItems where partnumber ='" + partnumber + "' and WishId =" + id;
    Cursor cursor = this.db.rawQuery(query, null);
    if (cursor.moveToFirst()) {
      return false;
    } else {
      return true;
    }

  }

  public String GetAlreadyAddedWishName(String wishid) {
    // TODO Auto-generated method stub
    openDatabase();
    String query = "select * from WishLists where WishId ='" + wishid + "'";
    Cursor cursor = this.db.rawQuery(query, null);
    if (cursor.moveToFirst()) {
      return cursor.getString(cursor.getColumnIndex(COLUMN_WISH_NAME));
    } else {
      return "";
    }

  }

  private ArrayList<String> getWishNames() {
    ArrayList<String> WishNames = new ArrayList<String>();
    wishID = new ArrayList<String>();

    openDatabase();
    String query = "SELECT  * FROM    " + WISH_TABLE_NAME;
    Cursor cursor = this.db.rawQuery(query, null);
    if (cursor.moveToFirst()) {
      do {
        WishNames.add(cursor.getString(cursor.getColumnIndex(COLUMN_WISH_NAME)));
        wishID.add(cursor.getString(cursor.getColumnIndex(COLUMN_WISH_ID)));
      } while (cursor.moveToNext());
    }
    return WishNames;
  }

  private int getLastWishID() {
    int LastID = 1;
    openDatabase();
    String query = "SELECT * FROM    " + WISH_TABLE_NAME + " WHERE   " + COLUMN_WISH_ID
        + " = (SELECT MAX(" + COLUMN_WISH_ID + ")  FROM " + WISH_TABLE_NAME + ")";
    Cursor cursor = this.db.rawQuery(query, null);
    if (cursor.moveToFirst()) {
      do {
        LastID = cursor.getInt(cursor.getColumnIndex(COLUMN_WISH_ID));
      } while (cursor.moveToNext());
    }
    return LastID;
  }

  private void AddDefaultWishName(String name, final CartDTO cartDTO, final int position) {



    openDatabase();
    ContentValues contentValue = new ContentValues();
    ContentValues contentValues = new ContentValues();

    if (!defaultNameCreated) {
      contentValue.put(COLUMN_WISH_NAME, name);
      db.insert(WISH_TABLE_NAME, null, contentValue);
      defaultNameCreated = true;
      edit.putBoolean("defaultnameadded", defaultNameCreated).commit();
    }

    contentValues.put(COLUMN_WISH_ID, 1);
    contentValues.put(COLUMN_PART_NAME, cartDTO.getPartNameList().get(position));
    contentValues.put(COLUMN_PART_NUMBER, cartDTO.getPartNoList().get(position));
    contentValues.put(COLUMN_PART_CODE, GetPartCode(cartDTO.getPartNoList().get(position)));
    contentValues.put(COLUMN_PART_TYPE, GetCatagory(cartDTO.getPartNoList().get(position)));
    contentValues.put(COLUMN_PRICE, GetPrice(cartDTO.getPartNoList().get(position)));
    contentValues.put(COLUMN_DESCRIPTION, cartDTO.getPartdescriptionList().get(position));
    contentValues.put(COLUMN_IMAGENAME, cartDTO.getPartImageList().get(position));
    db.insert(WISHITEMS_TABLE_NAME, null, contentValues);

  }


  /* Wish list Quick Order fuctions */
  public void addWishListFromQuickOrder(final CartDTO cartDTO) {
    openDatabase();
    ArrayList<String> wishNames = getWishNames();
    if (wishNames.size() == 0) {
      CreateNewWishItemsForQuickOrderDialog(cartDTO, cartDTO.getPartNoList().size());
    } else {

      new BottomSheet.Builder(context).setSheet(R.menu.grid_sheet).grid().setColumnCount(2)
          .setCancelable(false).setTitle("Wish list options")
          .setListener(new BottomSheetListener() {
            @Override
            public void onSheetShown(@NonNull BottomSheet bottomSheet) {

          }

            @Override
            public void onSheetItemSelected(@NonNull BottomSheet bottomSheet, MenuItem menuItem) {

              if (menuItem.getTitle().equals("Create New")) {
                CreateNewWishItemsForQuickOrderDialog(cartDTO, cartDTO.getPartNoList().size());
              } else {
                ShowWishItemsFromQuickOrder(cartDTO, cartDTO.getPartNoList().size());
              }

            }

            @Override
            public void onSheetDismissed(@NonNull BottomSheet bottomSheet, @DismissEvent int i) {

          }
          }).show();

    }
    closeDatabase();

  }


  private void CreateNewWishItemsForQuickOrderDialog(final CartDTO cartDTO, final int size) {

    new MaterialDialog.Builder(context).title("Brain Magic").content("Create new wish name")
        .autoDismiss(false)
        .inputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_AUTO_CORRECT)
        .input("Enter wish name", "", new MaterialDialog.InputCallback() {
          @Override
          public void onInput(MaterialDialog dialog, CharSequence input) {
            // Do something

          }
        }).positiveText("Create new").negativeText("Add to Default")
        .onPositive(new MaterialDialog.SingleButtonCallback() {
          @Override
          public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
            if (dialog.getInputEditText().length() == 0) {

              StyleableToast st =
                  new StyleableToast(context, "Enter wish name !", Toast.LENGTH_SHORT);
              st.setBackgroundColor(context.getResources().getColor(R.color.green));
              st.setTextColor(Color.WHITE);
              st.setMaxAlpha();
              st.show();
            } else {
              if (CreateWishNameFormQuickOrder(dialog.getInputEditText().getText().toString(),
                  cartDTO, size)) {
                ShowComplete("Part items added to " + dialog.getInputEditText().getText().toString()
                    + " wish name");
              } else {
                StyleableToast st = new StyleableToast(context,
                    "Another wishlist already exists in the same name !  ", Toast.LENGTH_SHORT);
                st.setBackgroundColor(context.getResources().getColor(R.color.red));
                st.setTextColor(Color.WHITE);
                st.setMaxAlpha();
                st.show();
              }

            }

          }
        }).onNegative(new MaterialDialog.SingleButtonCallback() {
          @Override
          public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
            dialog.dismiss();
            AddDefaultWishNameFromQuickOrder(myshare.getString("name", ""), cartDTO, size);
            ShowComplete("Part items added to " + myshare.getString("name", ""));
          }
        }).show();
  }

  private boolean CreateWishNameFormQuickOrder(String s, CartDTO cartDTO, int size) {

    openDatabase();
    ContentValues contentValues = new ContentValues();

    if (CheckAlreadyAddedWishname(s)) {

      ContentValues contentValue = new ContentValues();
      contentValue.put(COLUMN_WISH_NAME, s);
      db.insert(WISH_TABLE_NAME, null, contentValue);

      for (int i = 0; i < size; i++) {

        contentValues.put(COLUMN_WISH_ID, getLastWishID());
        contentValues.put(COLUMN_PART_NAME, cartDTO.getPartNameList().get(i));
        contentValues.put(COLUMN_PART_NUMBER, cartDTO.getPartNoList().get(i));
        contentValues.put(COLUMN_PART_CODE, GetPartCode(cartDTO.getPartNoList().get(i)));
        contentValues.put(COLUMN_PART_TYPE, GetCatagory(cartDTO.getPartNoList().get(i)));
        contentValues.put(COLUMN_PRICE, GetPrice(cartDTO.getPartNoList().get(i)));
        contentValues.put(COLUMN_DESCRIPTION, cartDTO.getPartdescriptionList().get(i));
        contentValues.put(COLUMN_IMAGENAME, cartDTO.getPartImageList().get(i));
        db.insert(WISHITEMS_TABLE_NAME, null, contentValues);
      }
      closeDatabase();
      return true;
    } else {
      return false;
    }



  }


  private void AddDefaultWishNameFromQuickOrder(String name, CartDTO cartDTO, int size) {

    openDatabase();
    ContentValues contentValue = new ContentValues();
    ContentValues contentValues = new ContentValues();

    if (!defaultNameCreated) {
      contentValue.put(COLUMN_WISH_NAME, name);
      db.insert(WISH_TABLE_NAME, null, contentValue);
      defaultNameCreated = true;
      edit.putBoolean("defaultnameadded", defaultNameCreated).commit();
    }

    for (int i = 0; i < size; i++) {
      contentValues.put(COLUMN_WISH_ID, 1);
      contentValues.put(COLUMN_PART_NAME, cartDTO.getPartNameList().get(i));
      contentValues.put(COLUMN_PART_NUMBER, cartDTO.getPartNoList().get(i));
      contentValues.put(COLUMN_PART_CODE, GetPartCode(cartDTO.getPartNoList().get(i)));
      contentValues.put(COLUMN_PART_TYPE, GetCatagory(cartDTO.getPartNoList().get(i)));
      contentValues.put(COLUMN_PRICE, GetPrice(cartDTO.getPartNoList().get(i)));
      contentValues.put(COLUMN_DESCRIPTION, cartDTO.getPartdescriptionList().get(i));
      contentValues.put(COLUMN_IMAGENAME, cartDTO.getPartImageList().get(i));
      db.insert(WISHITEMS_TABLE_NAME, null, contentValues);
    }


  }

  private void ShowWishItemsFromQuickOrder(final CartDTO cartDTO, final int size) {
    openDatabase();
    final ContentValues contentValues = new ContentValues();


    new AlertDialog.Builder(context)
        .setSingleChoiceItems(getWishNames().toArray(new String[getWishNames().size()]), 0, null)
        .setTitle("Choose your wish name").setCancelable(false)
        .setPositiveButton(R.string.okay, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int whichButton) {
            dialog.dismiss();
            int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();

            // Do something useful withe the position of the selected radio button

            contentValues.put(COLUMN_WISH_ID, wishID.get(selectedPosition));
            for (int i = 0; i < size; i++) {

              if (GetAlreadyAddedPartNumber(cartDTO.getPartNoList().get(i),
                  wishID.get(selectedPosition))) {

                contentValues.put(COLUMN_WISH_ID, wishID.get(selectedPosition));
                contentValues.put(COLUMN_PART_NAME, cartDTO.getPartNameList().get(i));
                contentValues.put(COLUMN_PART_NUMBER, cartDTO.getPartNoList().get(i));
                contentValues.put(COLUMN_PART_CODE, GetPartCode(cartDTO.getPartNoList().get(i)));
                contentValues.put(COLUMN_PART_TYPE, GetCatagory(cartDTO.getPartNoList().get(i)));
                contentValues.put(COLUMN_PRICE, GetPrice(cartDTO.getPartNoList().get(i)));
                contentValues.put(COLUMN_DESCRIPTION, cartDTO.getPartdescriptionList().get(i));
                contentValues.put(COLUMN_IMAGENAME, cartDTO.getPartImageList().get(i));
                db.insert(WISHITEMS_TABLE_NAME, null, contentValues);
              }
            }

            ShowComplete("Part items added to  " + getWishNames().get(selectedPosition));

          }
        }).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {

        }
        }).show();



    closeDatabase();

  }


  private void ShowComplete(String msg) {

    StyleableToast st = new StyleableToast(context, msg, Toast.LENGTH_SHORT);
    st.setBackgroundColor(context.getResources().getColor(R.color.green));
    st.setTextColor(Color.WHITE);
    st.setMaxAlpha();
    st.show();

    context.startActivity(new Intent(context, MainActivity.class)
        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));


  }


  /*-----------------------------------------------------------*/
  public void addToFavoriteDistributor(List<DistributorData> adapter, int adapterPosition) {
    openDatabase();
    ContentValues contentValues = new ContentValues();
    if (CheckAlreadyAdded(adapter.get(adapterPosition).getAddress())) {
      contentValues.put(COLUMN_ID, adapter.get(adapterPosition).getId());
      contentValues.put(COLUMN_NAME, adapter.get(adapterPosition).getName());
      contentValues.put(COLUMN_MOBILE, adapter.get(adapterPosition).getMobileNumber());
      contentValues.put(COLUMN_EMAIL, adapter.get(adapterPosition).getEmail());
      contentValues.put(COLUMN_STATE, adapter.get(adapterPosition).getState());
      contentValues.put(COLUMN_CITY, adapter.get(adapterPosition).getCity());
      contentValues.put(COLUMN_COUNTRY, adapter.get(adapterPosition).getCountry());
      contentValues.put(COLUMN_ADDRESS, adapter.get(adapterPosition).getAddress());

      db.insert(FAVORITE_DISTRIBUTOR_TABLE_NAME, null, contentValues);
    }
    closeDatabase();
  }

  public boolean CheckAlreadyAdded(String id) {
    // TODO Auto-generated method stub
    openDatabase();
    String query = "select * from FavoriteDistributor where address ='" + id + "'";
    Cursor cursor = this.db.rawQuery(query, null);
    if (cursor.moveToFirst()) {
      return false;
    } else {
      return true;
    }

  }


  public void AddToLocalOrder(QuickOrder quickOrder, String distributor_name, String  distributor_address) {
    openDatabase();
    ContentValues contentValues = new ContentValues();

    for (int i =0; i<quickOrder.getOrder_Parts().length;i++) {

      contentValues.put(COLUMN_DEALER_ID, quickOrder.getOrder().getDealerid());
      contentValues.put(COLUMN_DELIVERY_NAME, quickOrder.getOrder().getDeliveryName());
      contentValues.put(COLUMN_DELIVERY_MOBILE, quickOrder.getOrder().getDeliveryMobile());
      contentValues.put(COLUMN_DELIVERY_ADDRESS,quickOrder.getOrder().getDeliveryAddress());
      contentValues.put(COLUMN_ORDERED_NAME, quickOrder.getOrder().getOrderedByName());
      contentValues.put(COLUMN_ORDERED_MOBILE, quickOrder.getOrder().getOrderedMobile());
      contentValues.put(COLUMN_ORDERED_ADDRESS,quickOrder.getOrder().getOrderedAddress());
      contentValues.put(COLUMN_ORDERED_DATE, quickOrder.getOrder().getOrderDate());
      contentValues.put(COLUMN_ORDERED_STATUS, quickOrder.getOrder().getOrderStatus());
      contentValues.put(COLUMN_DELIVERY_STATUS, quickOrder.getOrder().getDeliveryStatus());
      contentValues.put(COLUMN_TOTAL_ORDEREDQTY, quickOrder.getOrder().getTotalOrderedQty());

      contentValues.put(COLUMN_DISTRIBUTOR_CODE, quickOrder.getOrder().getDistributorid());
      contentValues.put(COLUMN_DIS_NAME, distributor_name);
      contentValues.put(COLUMN_DIS_ADDRESS, distributor_address);

      contentValues.put(COLUMN_PART_NAME, quickOrder.getOrder_Parts()[i].getPartName());
      contentValues.put(COLUMN_PART_CODE, quickOrder.getOrder_Parts()[i].getPartNumber());
      contentValues.put(COLUMN_DESCRIPTION, quickOrder.getOrder_Parts()[i].getDescription());
      contentValues.put(COLUMN_PART_NUMBER, quickOrder.getOrder_Parts()[i].getPartNumber());
      contentValues.put(COLUMN_QUANTITY,quickOrder.getOrder_Parts()[i].getQuantity());
      contentValues.put(COLUMN_PRICE,quickOrder.getOrder_Parts()[i].getPartPrice());

      db.insert(LOCAL_ORDER_TABLE_NAME, null, contentValues);
    }
    closeDatabase();

    ShowComplete("Your order saved locally !");
  }


  public CartDTO GetCartItems() {
    List<String> CartPartID = new ArrayList<String>();
    List<String> CartpartName = new ArrayList<String>();
    List<String> CartPartNumber = new ArrayList<String>();
    List<String> CartPartCode = new ArrayList<String>();
    List<String> CartPartType = new ArrayList<String>();
    List<String> CartQuantity = new ArrayList<String>();
    List<String> CartDescription = new ArrayList<String>();
    List<String> CartPrice = new ArrayList<String>();
    List<String> CartImage = new ArrayList<String>();

    CartDTO cartDTO = new CartDTO();
    openDatabase();
    String query = "select * from " + CART_TABLE_NAME + "";
    Log.v("Query view cartitem", query);
    Cursor cursor = this.db.rawQuery(query, null);
    if (cursor.moveToFirst()) {
      do {

        CartPartID.add(cursor.getString(cursor.getColumnIndex(COLUMN_PARTID)));
        CartpartName.add(cursor.getString(cursor.getColumnIndex(COLUMN_PART_NAME)));
        CartPartNumber.add(cursor.getString(cursor.getColumnIndex(COLUMN_PART_NUMBER)));
        CartPartCode.add(cursor.getString(cursor.getColumnIndex(COLUMN_PART_CODE)));
        CartPartType.add(cursor.getString(cursor.getColumnIndex(COLUMN_PART_TYPE)));
        CartQuantity.add(cursor.getString(cursor.getColumnIndex(COLUMN_QUANTITY)));
        CartDescription.add(cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)));
        CartPrice.add(cursor.getString(cursor.getColumnIndex(COLUMN_PRICE)));
        CartImage.add(cursor.getString(cursor.getColumnIndex(COLUMN_IMAGENAME)));
        Log.v("image Name", cursor.getString(cursor.getColumnIndex(COLUMN_IMAGENAME)));
      } while (cursor.moveToNext());

      cartDTO.setPartIDList(CartPartID);
      cartDTO.setPartCodeList(CartPartCode);
      cartDTO.setPartdescriptionList(CartDescription);
      cartDTO.setPartImageList(CartImage);
      cartDTO.setPartNameList(CartpartName);
      cartDTO.setPartNoList(CartPartNumber);
      cartDTO.setPartTypeList(CartPartType);
      cartDTO.setPartPriceList(CartPrice);
      cartDTO.setPartQuantityList(CartQuantity);

    }
    closeDatabase();
    return cartDTO;

  }

}
