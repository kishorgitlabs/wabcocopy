package distributor.neworders;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobileordering.R;

public class Distributor_NewOrdersActivity extends Activity {


    private TextView Order_number;
    private ImageView Backbtn;
    private View heade_Layout;
    private TextView Tittle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distributor__new_orders);

        Order_number = (TextView) findViewById(R.id.order_number);


        heade_Layout =  findViewById(R.id.header_layout);
        Tittle = (TextView) heade_Layout.findViewById(R.id.tittle);
        Backbtn = (ImageView) heade_Layout.findViewById(R.id.back);
        Tittle.setText("New Orders");



        Order_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(Distributor_NewOrdersActivity.this,Distributor_NewOrders_DetailsActivity.class));
            }
        });

        Backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

}
