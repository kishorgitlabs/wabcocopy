package distributor.pending;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mobileordering.R;



public class Distributor_Pending_Orders_Details_Activity extends AppCompatActivity {


    View header_Layout;
/*
    @InjectView(R.id.card_recycler_view)
    RecyclerView recycler_view;*/

    private TextView Tittle;
    private LinearLayout AccountLayout;
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private String UserType,Accountname;
    private ImageView Backbtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_order_details);

        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();
        UserType = myshare.getString("usertype", "").toString();



        header_Layout =  findViewById(R.id.header_layout);
        Tittle = (TextView) header_Layout.findViewById(R.id.tittle);
        Tittle.setText("Pending Orders Details");

        Backbtn = (ImageView) header_Layout.findViewById(R.id.back);
        Backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


       /* recycler_view.setHasFixedSize(true);
        recycler_view.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recycler_view.setLayoutManager(llm);*/


    }
}
