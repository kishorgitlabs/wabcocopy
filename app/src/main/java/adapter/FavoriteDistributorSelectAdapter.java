package adapter;

import java.util.List;

import com.mobileordering.R;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import models.distributor.DistributorData;

import static android.content.Context.MODE_PRIVATE;


public class FavoriteDistributorSelectAdapter extends ArrayAdapter<DistributorData> {
  private Context context;
  private List<DistributorData> distributorData;
  private int selectedPosition = -1;
  private SharedPreferences distributor_share;
  private SharedPreferences.Editor distributor_edit;

  public FavoriteDistributorSelectAdapter(Context context, List<DistributorData> data) {
    super(context, R.layout.adapter_favorite_distributor,data);
    this.context = context;
    this.distributorData = data;
    distributor_share = context.getSharedPreferences("distributor_address", MODE_PRIVATE);
    distributor_edit = distributor_share.edit();

  }

  public View getView(final int position, View convertView, ViewGroup parent) {

    final DistributorHolder contactHolder;
    if (convertView == null) {
      convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater"))
          .inflate(R.layout.adapter_favorite_distributor, null);
      contactHolder = new DistributorHolder();
      contactHolder.name = (TextView) convertView.findViewById(R.id.dist_name);
      contactHolder.address = (TextView) convertView.findViewById(R.id.dist_address);
      contactHolder.selectdistributor= (CheckBox) convertView.findViewById(R.id.select_distributor);
      convertView.setTag(contactHolder);
    } else {
      contactHolder = (DistributorHolder) convertView.getTag();
    }
    contactHolder.address.setText((CharSequence) distributorData.get(position).getAddress());
    contactHolder.name.setText((CharSequence) distributorData.get(position).getName());


      //check the radio button if both position and selectedPosition matches
      contactHolder.selectdistributor.setChecked(position == selectedPosition);
      //Set the position tag to both radio button and label
      contactHolder.selectdistributor.setTag(position);



      contactHolder.selectdistributor.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          itemCheckChanged(v);

        }
      });





    return convertView;

  }

  private void itemCheckChanged(View v) {

    selectedPosition = (Integer) v.getTag();
    notifyDataSetChanged();
    distributorData.get(selectedPosition).setSelected(true);
    distributor_edit.clear();
    distributor_edit.commit();
    distributor_edit.putString("distributor_id", distributorData.get(selectedPosition).getId().toString());
    distributor_edit.putString("distributor_name", distributorData.get(selectedPosition).getName().toString());
    distributor_edit.putString("distributor_mobile", distributorData.get(selectedPosition).getMobileNumber());
    distributor_edit.putString("distributor_email", distributorData.get(selectedPosition).getEmail());
    distributor_edit.putString("distributor_address", distributorData.get(selectedPosition).getAddress());
    distributor_edit.commit();
  }


  @Override
  public int getCount() {
    return distributorData.size();
  }

  class DistributorHolder {
    TextView name, address;
    CheckBox selectdistributor;
  }


}
