package adapter;

import java.util.ArrayList;

import com.mobileordering.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import dealer.uploadorders.DealerUploadOrdersDetailsActivity;
import persistence.DBHelper;

/*
*
 * Created by system01 on 2/12/2017.
*/


public class Offline_Orderlist_Adapter extends ArrayAdapter<String> {


    private  Context context;
    private ArrayList<String> Dis_namelist,Dis_idlist,Dis_addresslist;
    //public ArrayList<Integer> Wishid,checksid,checkspos;


    public Offline_Orderlist_Adapter(Context context, ArrayList<String> Dis_namelist, ArrayList<String> Dis_idlist, ArrayList<String> Dis_addresslist) {
        super(context, R.layout.adapter_offline_orderlist, Dis_namelist);

        this.context = context;
        this.Dis_namelist = Dis_namelist;
        this.Dis_idlist = Dis_idlist;
        this.Dis_addresslist = Dis_addresslist;


    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null)
        {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.adapter_offline_orderlist, parent, false);


            TextView Dis_name = (TextView) convertView.findViewById(R.id.dis_name);
            TextView Dis_address = (TextView) convertView.findViewById(R.id.dis_address);
            ImageView Delete =  (ImageView) convertView.findViewById(R.id.delete);
            LinearLayout select_layout =  (LinearLayout) convertView.findViewById(R.id.select_layout);

            Dis_name.setText(Dis_namelist.get(position));
            Dis_address.setText(Dis_addresslist.get(position));

            select_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent a = new Intent(context, DealerUploadOrdersDetailsActivity.class);
                    a.putExtra("Dis_id", Dis_idlist.get(position));
                    a.putExtra("Dis_name", Dis_namelist.get(position));
                    a.putExtra("Dis_address", Dis_addresslist.get(position));
                    context.startActivity(a);
                }
            });

            Delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder adb=new AlertDialog.Builder(context);
                    adb.setTitle("Delete ?");
                    adb.setMessage("Are you sure you want to delete " + Dis_namelist.get(position) + " Orders ? ");
                    adb.setNegativeButton("Cancel", null);
                    adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            deletelist(Dis_idlist.get(position));
                            remove(Dis_idlist.get(position));
                            Dis_idlist.remove(position);
                            Dis_namelist.remove(position);
                            Dis_addresslist.remove(position);
                            notifyDataSetChanged();
                        }});
                    adb.show();
                }
            });
            convertView.setTag(convertView);

        }

        return convertView;
    }

    private void deletelist(String orderid)
    {

        SQLiteDatabase db = null;
        DBHelper dbhelper = new DBHelper(context);
        db = dbhelper.readDataBase();
        db.execSQL("delete from Offlineorders where distributor_id = '"+orderid+"'");
        db.close();
    }


}
