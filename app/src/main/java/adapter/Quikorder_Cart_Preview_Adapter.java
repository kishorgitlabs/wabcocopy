package adapter;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.mobileordering.R;

import addcart.CartDTO;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import persistence.DBHelper;


/*Created by system01 on 2/12/2017.*/


public class Quikorder_Cart_Preview_Adapter extends ArrayAdapter<String> {

  private Context context;
  private CartDTO cartDTO;
 // private ArrayList<String> PartNumList, DescriptionList, QuantityList;
 // private ArrayList<Integer> PartIDList, PriceList;


  public Quikorder_Cart_Preview_Adapter(Context context, CartDTO cartDTO) {
    super(context, R.layout.preview_adapter, cartDTO.getPartIDList());
    this.context = context;
    this.cartDTO = cartDTO;

  }


  @Override
  public View getView(final int position, View convertView, ViewGroup parent) {
    // TODO Auto-generated method stub
    convertView = null;

    if (convertView == null) {
      convertView = ((LayoutInflater) context.getSystemService("layout_inflater"))
              .inflate(R.layout.preview_adapter, parent, false);

      ImageView delete = (ImageView) convertView.findViewById(R.id.delete);
      ImageView edit = (ImageView) convertView.findViewById(R.id.update);
      TextView SNo = (TextView) convertView.findViewById(R.id.sno);
      TextView text1 = (TextView) convertView.findViewById(R.id.text1);
      TextView text2 = (TextView) convertView.findViewById(R.id.text2);
      TextView text3 = (TextView) convertView.findViewById(R.id.text3);
      TextView price = (TextView) convertView.findViewById(R.id.price);

      convertView.setTag(convertView);
      SNo.setText(Integer.toString(position + 1));

      text1.setText(cartDTO.getPartNoList().get(position));
      text2.setText(cartDTO.getPartdescriptionList().get(position));
      text3.setText(cartDTO.getPartQuantityList().get(position));
      price.setText(context.getResources().getString(R.string.Rs) + " "
              + cartDTO.getPartPriceList().get(position) + "/-");



      delete.setTag(position);
      delete.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          AlertDialog.Builder adb = new AlertDialog.Builder(context);
          adb.setTitle(R.string.delete_preview);
          adb.setMessage(context.getString(R.string.delete_alert) + cartDTO.getPartdescriptionList().get(position));
          adb.setNegativeButton(R.string.cancel, null);
          adb.setPositiveButton(R.string.okay, new AlertDialog.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
              deletelist(Integer.parseInt(cartDTO.getPartIDList().get(position)));
              cartDTO.getPartCodeList().remove(position);
              cartDTO.getPartIDList().remove(position);
              cartDTO.getPartPriceList().remove(position);
              cartDTO.getPartdescriptionList().remove(position);
              cartDTO.getPartImageList().remove(position);
              cartDTO.getPartNameList().remove(position);
              cartDTO.getPartNoList().remove(position);
              cartDTO.getPartQuantityList().remove(position);
              cartDTO.getPartTypeList().remove(position);
              notifyDataSetChanged();
            }
          });
          adb.show();
        }
      });
      edit.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          showAlertbox(Integer.parseInt(cartDTO.getPartIDList().get(position)), position, cartDTO.getPartNoList().get(position),
          cartDTO.getPartdescriptionList().get(position), cartDTO.getPartQuantityList().get(position));
          notifyDataSetChanged();
        }
      });

    }

    return convertView;
  }

  private void deletelist(int orderid) {

    SQLiteDatabase db = null;
    DBHelper dbhelper = new DBHelper(context);
    db = dbhelper.readDataBase();
    db.execSQL("delete from CartItem where partid = '" + orderid + "'");
    db.close();
  }

  public void showAlertbox(final int id, final int pos, String partno, String description,
                           String quanty) {
    final AlertDialog alertDialog = new AlertDialog.Builder(context).create();

    LayoutInflater inflater = ((Activity) context).getLayoutInflater();
    View dialogView = inflater.inflate(R.layout.update_quickorder, null);
    alertDialog.setView(dialogView);

    TextView Partnumber = (TextView) dialogView.findViewById(R.id.partno);
    TextView Description = (TextView) dialogView.findViewById(R.id.desc);

    final EditText Quantity = (EditText) dialogView.findViewById(R.id.quantity);

    Quantity.setFilters(new InputFilter[] {new InputFilterMinMax("1", "1000")});


    Button Save = (Button) dialogView.findViewById(R.id.save);
    Button Cancel = (Button) dialogView.findViewById(R.id.cancel);

    Partnumber.setText(partno);
    Description.setText(description);
    Quantity.setText(quanty);


    Save.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View arg0) {
        // TODO Auto-generated method stub

        if (Quantity.getText().length() != 0) {
          SQLiteDatabase db = null;
          DBHelper dbhelper = new DBHelper(context);
          db = dbhelper.readDataBase();
          String query = "update CartItem set quantity = '" + Quantity.getText().toString()
                  + "' where partid = '" + id + "'";
          // update Orderpreview set quantity = 5, discount = 10 where id = 1
          db.execSQL(query);
          db.close();
          cartDTO.getPartQuantityList().remove(pos);
          //cartDTO.getPartQuantityList().get(pos).replace(quanty,Quantity.getText()                                                 toString());
          cartDTO.getPartQuantityList().add(pos,Quantity.getText().toString());

         //QuantityList.add(pos, Quantity.getText().toString());
          notifyDataSetChanged();
          alertDialog.dismiss();
        } else {
          StyleableToast st = new StyleableToast(context,
                  context.getResources().getString(R.string.enter_quantity), Toast.LENGTH_SHORT);
          st.setBackgroundColor(context.getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();
        }

      }
    });
    alertDialog.show();
    Cancel.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        alertDialog.dismiss();
      }
    });
  }



}
