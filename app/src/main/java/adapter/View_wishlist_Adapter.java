package adapter;

import java.util.ArrayList;

import com.mobileordering.R;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import dealer.wishlist.WishList_details_Activity;

/*
*
 * Created by system01 on 2/12/2017.
*/


public class View_wishlist_Adapter extends ArrayAdapter<String> {

    private  ArrayList<Integer> totalList;
    private  Context context;
    private ArrayList<String> Wishname,checkname;
    public ArrayList<Integer> Wishid,checksid,checkspos;


    public View_wishlist_Adapter(Context context, ArrayList<String> Wishname,  ArrayList<Integer> Wishid,ArrayList<Integer> checksid,ArrayList<Integer> checkspos,ArrayList<String> checkname) {
        super(context, R.layout.adapter_view_whishlist, Wishname);

        this.context = context;
        this.Wishname = Wishname;
        this.Wishid = Wishid;
        this.checksid= checksid;
        this.checkspos= checkspos;
        this.checkname= checkname;

    }
    @Override
    public int getCount() {
        return Wishid.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null)
        {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.adapter_view_whishlist, parent, false);


            TextView wishlistname = (TextView) convertView.findViewById(R.id.wishlist_name);
            CheckBox Selectwish = (CheckBox)  convertView.findViewById(R.id.selectwishlist);
            LinearLayout select_layout = (LinearLayout)  convertView.findViewById(R.id.select_layout);

            wishlistname.setText(Wishname.get(position));
            if(Wishid.get(position) == 1)
                Selectwish.setVisibility(View.INVISIBLE);

            Selectwish.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(((CheckBox)v).isChecked()){
                        //checkspos.add(position);
                        //checkspos.add(position,Wishid.get(position));
                        checksid.add(Wishid.get(position));
                        checkname.add(Wishname.get(position));
                    }
                    else
                    {
                        checksid.remove(Wishid.get(position));
                        checkname.remove(Wishname.get(position));
                    }

                }
            });

            select_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent a = new Intent(context, WishList_details_Activity.class);
                    a.putExtra("ID", Wishid.get(position));
                    a.putExtra("Listname", Wishname.get(position));
                    context.startActivity(a);
                }
            });


            convertView.setTag(convertView);

        }

        return convertView;
    }




}
