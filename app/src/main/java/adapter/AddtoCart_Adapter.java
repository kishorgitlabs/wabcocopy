package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.mobileordering.R;

import java.io.IOException;
import java.util.Arrays;

import addcart.CartDAO;
import addcart.CartDTO;
import vehiclemake.VehicleDTO;

/**
 * Created by system01 on 5/22/2017.
 */

public class AddtoCart_Adapter extends RecyclerView.Adapter<adapter.AddtoCart_Adapter.CarttoHolderHolder> {


    private  CartDAO cartDAO;
    private  Context context;
    private  VehicleDTO vehicleDTO;
    private  CartDTO cartDTO;
    private  String from;

    public AddtoCart_Adapter(Context context,  VehicleDTO vehicleDAO,String from)
    {
        this.context=context;
        this.vehicleDTO =vehicleDAO;
        this.from =from;
        cartDAO = new CartDAO(context);
    }

    public AddtoCart_Adapter(Context context,  CartDTO cartDTO, String from)
    {
        this.context=context;
        this.cartDTO =cartDTO;
        this.from =from;
        cartDAO = new CartDAO(context);
    }

    @Override
    public AddtoCart_Adapter.CarttoHolderHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_view_list_item, null);
        AddtoCart_Adapter.CarttoHolderHolder rcv = new AddtoCart_Adapter.CarttoHolderHolder(layoutView);
        return rcv;
    }
    @Override
    public void onBindViewHolder(AddtoCart_Adapter.CarttoHolderHolder holder, final int position)
    {

        if(from.equals("pricelist"))
        {
            try {
            holder.partName.setText(cartDTO.getPartdescriptionList().get(position));
            holder.partNumber.setText(cartDTO.getPartNoList().get(position));
            holder.price.setText(context.getResources().getString(R.string.Rs) + " "
                    + cartDTO.getPartPriceList().get(position) + " /-");
           holder.description.setText(cartDTO.getPartdescriptionList().get(position));
        }catch (Exception ex)
        {
            Log.v("Error",ex.getMessage());
        }
           /* if(cartDTO.getPartImageList().get(position).equals(""))
            {
                Picasso.with(context).load("file:///android_asset/noimagefound.jpg").into(holder.partimage);
            }
            else
            {
                Picasso.with(context).load("file:///android_asset/"+ cartDTO.getPartImageList().get(position)+".jpg").into(holder.partimage);
            }*/
            try {
                if(Arrays.asList(context.getResources().getAssets().list("")).contains(cartDTO.getPartImageList().get(position)+".jpg")) {

                    Picasso.with(context).load("file:///android_asset/" + cartDTO.getPartImageList().get(position)+".jpg").error(R.drawable.noimagefound).into(holder.partimage);
                }
                else {
                    Picasso.with(context).load("file:///android_asset/" + "noimagefound.jpg").error(R.drawable.noimagefound).into(holder.partimage);
                }
            } catch (IOException e) {

                e.printStackTrace();
            }

        }
        else
        {
            holder.partNumber.setText(vehicleDTO.getPartNoList().get(position));
            holder.partName.setText(vehicleDTO.getProductNameList().get(position));
            holder.price.setText(context.getResources().getString(R.string.Rs) + " "
                    + vehicleDTO.getPriceList().get(position) + " /-");
            holder.description.setText(vehicleDTO.getPartDespcriptionList().get(position));

           /* if(vehicleDTO.getPartImageList().get(position).equals(""))
            {
                Picasso.with(context).load("file:///android_asset/noimagefound.jpg").into(holder.partimage);
            }
            else
            {
                Picasso.with(context).load("file:///android_asset/"+ vehicleDTO.getPartImageList().get(position)+".jpg").into(holder.partimage);
            }*/

            try {
                if(Arrays.asList(context.getResources().getAssets().list("")).contains(vehicleDTO.getPartImageList().get(position)+".jpg")) {

                    Picasso.with(context).load("file:///android_asset/" + vehicleDTO.getPartImageList().get(position)+".jpg").into(holder.partimage);
                }
                else {
                    Picasso.with(context).load("file:///android_asset/" + "noimagefound.jpg").into(holder.partimage);
                }
            } catch (IOException e) {

                e.printStackTrace();
            }
        }
        holder.cartbtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(from.equals("pricelist")){
                    cartDAO.addToCart(cartDTO,position);
                }
                else {
                    CartDTO cartDTO = new CartDTO();
                    cartDTO.setPartNoList(vehicleDTO.getPartNoList());
                    cartDTO.setPartdescriptionList(vehicleDTO.getPartDespcriptionList());
                    cartDTO.setPartImageList(vehicleDTO.getPartImageList());
                    cartDTO.setPartNameList(vehicleDTO.getProductNameList());
                    cartDAO.addToCart(cartDTO,position);
                }
            }
        });
        holder.wishbtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(from.equals("pricelist")){
                    cartDAO.addWishList(cartDTO, position);
                }
                else {
                    CartDTO cartDTO = new CartDTO();
                    cartDTO.setPartNoList(vehicleDTO.getPartNoList());
                    cartDTO.setPartdescriptionList(vehicleDTO.getPartDespcriptionList());
                    cartDTO.setPartImageList(vehicleDTO.getPartImageList());
                    cartDTO.setPartNameList(vehicleDTO.getProductNameList());
                    cartDAO.addWishList(cartDTO, position);
                }
            }
        });
    }
    @Override
    public int getItemCount()
    {
        if(from.equals("pricelist"))
        {
            return this.cartDTO.getPartNoList().size();
        }else {
            return this.vehicleDTO.getPartNoList().size();
        }
    }
    public static class CarttoHolderHolder extends RecyclerView.ViewHolder {
        protected TextView partNumber;
        protected TextView partName;
        protected TextView description;
        protected TextView price;
        protected ImageView partimage;
        protected Button cartbtn;
        protected Button wishbtn;

        public CarttoHolderHolder(View v)
        {
            super(v);
            partNumber =  (TextView) v.findViewById(R.id.part_number);
            partName = (TextView)  v.findViewById(R.id.part_name);
            price = (TextView)  v.findViewById(R.id.price);
            partimage =(ImageView)  v.findViewById(R.id.part_image);
            description =(TextView)v.findViewById(R.id.description);
            cartbtn = (Button) v.findViewById(R.id.btn_cart);
            wishbtn = (Button) v.findViewById(R.id.btn_wish);
        }
    }
}
