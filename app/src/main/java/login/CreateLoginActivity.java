package login;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.mobileordering.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import alertbox.Alertbox;
import api.APIService;
import api.ApiUtils;
import models.usermodel.User;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.mobileordering.R.id.okay;

public class CreateLoginActivity extends AppCompatActivity {

    private EditText Confirmpassword, username, password;
    private Button Update, cancel;
    private MaterialSpinner state;
    private Alertbox box = new Alertbox(CreateLoginActivity.this);
    String currentDate, SelectedUser, SelectedState;
    private List<String> stateList;
    private String[] drawerResourcesList;
    private User user;
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private AlertDialog alertDialog;
    private Pattern pattern;
    private Matcher matcher;
    private static final String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z]).{8,20})";
    private int SelectedStatePosition;
    private String deviceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_login);

        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();
        pattern = Pattern.compile(PASSWORD_PATTERN);


        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        Confirmpassword = (EditText) findViewById(R.id.confirm_password);
        Update = (Button) findViewById(okay);
        cancel = (Button) findViewById(R.id.cancel);
        username.setText("Username :" + myshare.getString("phone", ""));
        password.setText("Password :" + myshare.getString("phone", ""));
        username.setEnabled(false);
        password.setEnabled(false);
        deviceId = myshare.getString("mobileid", "");

        Update.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

              if (!Confirmpassword.getText().toString().equals(myshare.getString("phone", ""))) {
                    Confirmpassword.setError("Password is mismatching !");
                    StyleableToast st = new StyleableToast(CreateLoginActivity.this,
                            "Password is mismatching !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(CreateLoginActivity.this.getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                } else {
                    CheckInternet();
                }


            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                System.exit(0);
                finish();

            }
        });


    }



    protected void CheckInternet() {
        // TODO Auto-generated method stub
        NetworkConnection isnet = new NetworkConnection(CreateLoginActivity.this);
        if (isnet.CheckInternet()) {
            InsertUser();
        } else {
            box.showAlertbox(getResources().getString(R.string.nointernetmsg));
        }

    }


    public void InsertUser() {
        final ProgressDialog loading =
                ProgressDialog.show(this, "Updating", "Please wait...", false, false);


        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        currentDate = df.format(c.getTime());

        Retrofit retrofit = new Retrofit.Builder().baseUrl(ApiUtils.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).build();

        APIService api = retrofit.create(APIService.class);


        Call<User> userapi = api.CreateLogin(
                myshare.getString("id", ""),
                myshare.getString("name", ""),
                myshare.getString("email", ""),
                myshare.getString("phone", ""),
                myshare.getString("usertype", ""),
                myshare.getString("city", ""),
                myshare.getString("pincode", ""),
                myshare.getString("state", ""),
                myshare.getString("country", ""),
                currentDate, myshare.getString("mobileid", ""),
                myshare.getString("shopname", ""),
                myshare.getString("brainmagic", ""),
                myshare.getString("phone", ""),
                myshare.getString("phone", ""),
                myshare.getString("gstnumber", ""),
                myshare.getString("pannumber", ""));
        // Creating an anonymous callback
        userapi.enqueue(new Callback<User>() {

            @Override
            public void onResponse(Call<User> list, Response<User> response) {
                if (response.isSuccessful()) {
                    user = response.body();
                    loading.dismiss();
                    Log.v("Result", response.body().getResult());

                    if (response.body().getResult().equals("UserUpdated")) {
                        SuccessRegistration(user);
                    } else if (response.body().getResult().equals("Not Updated")) {
                        FailedRegistration();
                    } else {
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));
                    }
                } else {
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                loading.dismiss();
                t.printStackTrace();
                box.showAlertbox(getResources().getString(R.string.slow_internet_connection));
            }
        });
    }


    public void SuccessRegistration(final User user) {
        alertDialog = new AlertDialog.Builder(CreateLoginActivity.this).create();
        alertDialog.setTitle("BrainMagic");
        alertDialog.setMessage("Updation Successful");
        alertDialog.setButton("Okay", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                // TODO Auto-generated method stub
                alertDialog.dismiss();
                edit.putBoolean("isregistration", true);
                edit.putString("id", user.getData().getId());
                edit.putString("state", SelectedState);
                edit.putInt("stateposition", SelectedStatePosition);
                edit.putString("registereddate", currentDate);
                edit.putString("mobileid", deviceId);
                edit.putString("username", myshare.getString("phone", ""));
                edit.putString("password", password.getText().toString());
                edit.putBoolean("islogincreated", true);
                edit.commit();
                onBackPressed();

            }
        });
        alertDialog.show();

    }

    public void FailedRegistration() {
        box.showAlertbox("Your details was not updated please try again !");
    }


}
