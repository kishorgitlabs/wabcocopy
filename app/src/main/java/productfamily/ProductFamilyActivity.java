package productfamily;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnDismissListener;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.mobileordering.R;

import java.util.ArrayList;
import java.util.List;

import addcart.CartDAO;
import addcart.CartDTO;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import pricelist.PriceListActivity;
import quickorder.Quick_Order_Preview_Activity;
import search.SearchActivity;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;


public class ProductFamilyActivity extends Activity {
    private ProgressDialog loadDialog;
    private ProductFamilyAdapter productFamilyAdapter;
    private ListView productFamilyListView;
    public List<String> productIDList;
    private List<String> productNameList;
    public List<String> vehicleIDList;
    public ArrayList<String> vehicleNameList;

     ImageView Cart_Icon;

      ImageView backImageView;
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private String UserType;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productfamily);


        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();

        Cart_Icon = (ImageView) findViewById(R.id.cart_icon);
        backImageView = (ImageView) findViewById(R.id.back);

        UserType = myshare.getString("usertype", "").toString();
        if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")))
            Cart_Icon.setVisibility(View.VISIBLE);
        else
            Cart_Icon.setVisibility(View.GONE);


        backImageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        this.productFamilyListView = (ListView) findViewById(R.id.productfamily_listView);
        new RetrieveFileAsyn().execute();
        
        this.productFamilyListView.setOnItemClickListener(new OnItemClickListener() {

        	public void onItemClick(AdapterView<?> adapterView, View view, int position, long arg3) {
                Intent vehicleProductFamily = new Intent(ProductFamilyActivity.this, ProductFamilyItemActivity.class);
                vehicleProductFamily.putExtra("PRODUCTFAMILY_NAME", (String) ProductFamilyActivity.this.productNameList.get(position));
                vehicleProductFamily.putExtra("PRODUCT_ID", (String) ProductFamilyActivity.this.productIDList.get(position));
                ProductFamilyActivity.this.startActivity(vehicleProductFamily);
            }
		});
    	final ImageView menu = (ImageView)findViewById(R.id.menu);
		menu.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(ProductFamilyActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
				pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {

					public boolean onMenuItemClick(MenuItem item) {
						switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(ProductFamilyActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
						case R.id.search:
							startActivity(new Intent(ProductFamilyActivity.this, SearchActivity.class));
							break;
						case R.id.notification:
							startActivity(new Intent(ProductFamilyActivity.this, NotificationActivity.class));
							break;
						case R.id.vehicle:
							startActivity(new Intent(ProductFamilyActivity.this, VehicleMakeActivity.class));
							break;
						case R.id.product:
							startActivity(new Intent(ProductFamilyActivity.this, ProductFamilyActivity.class));
							break;
						case R.id.performance:
							startActivity(new Intent(ProductFamilyActivity.this, PE_Kit_Activity.class));
							break;
                            case R.id.contact:
                                startActivity(new Intent(ProductFamilyActivity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(ProductFamilyActivity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(ProductFamilyActivity.this, PriceListActivity.class));
                                break;
						case R.id.update:
							WabcoUpdate update = new WabcoUpdate(ProductFamilyActivity.this);
							update.checkVersion();
							break;
						}
						return false;
					}
				});
				pop.setOnDismissListener(new OnDismissListener() {

					@Override
					public void onDismiss(PopupMenu arg0) {
						// TODO Auto-generated method stub
						pop.dismiss();
					}
				});

				pop.inflate(R.menu.main);
				pop.show();
			}
		});

        Cart_Icon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v)
            {
                CartDAO cartDAO = new CartDAO(ProductFamilyActivity.this);
                CartDTO cartDTO = cartDAO.GetCartItems();
                if(cartDTO.getPartCodeList() == null)
                {
                    StyleableToast st =
                            new StyleableToast(ProductFamilyActivity.this, "Cart is Empty !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(ProductFamilyActivity.this.getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else
                {
                    startActivity(new Intent(ProductFamilyActivity.this, Quick_Order_Preview_Activity.class).putExtra("from","CartItem"));
                }
              //  startActivity(new Intent(ProductFamilyActivity.this, Cart_Activity.class));
            }
        });
		
    }
    

    class RetrieveFileAsyn extends AsyncTask<Void, Void, String> {


        protected void onPreExecute() {
            super.onPreExecute();
            ProductFamilyActivity.this.loadDialog = new ProgressDialog(ProductFamilyActivity.this);
            ProductFamilyActivity.this.loadDialog.setMessage("Loading...");
            ProductFamilyActivity.this.loadDialog.setProgressStyle(0);
            ProductFamilyActivity.this.loadDialog.setCancelable(false);
            ProductFamilyActivity.this.loadDialog.show();
        }

        protected String doInBackground(Void... arg0) {
            ProductFamilyDAO productFamilyDAO = new ProductFamilyDAO(ProductFamilyActivity.this);
            ProductFamilyDTO productFamilyDTO = new ProductFamilyDTO();
            productFamilyDTO = productFamilyDAO.retrieveALL();
            ProductFamilyActivity.this.productNameList = productFamilyDTO.getProductNameList();
            ProductFamilyActivity.this.productIDList = productFamilyDTO.getProductIDList();
            if (ProductFamilyActivity.this.productNameList.isEmpty()) {
                return "nodata";
            }
            return "data";
        }

        @SuppressWarnings("deprecation")
		protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ProductFamilyActivity.this.loadDialog.dismiss();
            if (result.equals("data")) {
                ProductFamilyActivity.this.productFamilyAdapter = new ProductFamilyAdapter(ProductFamilyActivity.this, ProductFamilyActivity.this.productNameList);
                ProductFamilyActivity.this.productFamilyListView.setAdapter(ProductFamilyActivity.this.productFamilyAdapter);
                return;
            }
            AlertDialog alertDialog = new Builder(ProductFamilyActivity.this).create();
            alertDialog.setTitle("WABCO");
            alertDialog.setMessage("No date found \n Download Database Please");
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
				
            	 public void onClick(DialogInterface dialog, int which) {
                     dialog.cancel();
                     ProductFamilyActivity.this.onBackPressed();
                 }
			});
            alertDialog.show();
        }
    }

    public ProductFamilyActivity() {
        this.productNameList = new ArrayList<String>();
    }

   
}
