package font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class FontDesignSans extends TextView {
    public static Typeface FONT_NAME;

    public FontDesignSans(Context context) {
        super(context);
        isInEditMode();
    }

    public FontDesignSans(Context context, AttributeSet attrs) {
        super(context, attrs);
        isInEditMode();
    }

    public FontDesignSans(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        isInEditMode();
    }

    public void setTypeface(Typeface tf, int style) {
        if (!isInEditMode()) {
            Typeface normalTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Roboto-Regular.ttf");
            Typeface boldTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/roboto.bold.ttf");
            if (style == 1) {
                super.setTypeface(boldTypeface);
            } else {
                super.setTypeface(normalTypeface);
            }
        }
    }
}
