package vehiclemake;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnDismissListener;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.mobileordering.R;

import java.util.ArrayList;
import java.util.List;

import addcart.CartDAO;
import addcart.CartDTO;
import alertbox.Alertbox;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import quickorder.Quick_Order_Preview_Activity;
import search.SearchActivity;
import wabco.Network_Activity;


public class VehicleProductFamilyActivity extends Activity {
    private TextView navigation_text1;
    private TextView navigation_text2;
    private ProgressDialog loadDialog;
    private List<String> productIDList;
    private List<String> productNameList;
    private ImageView vehicleBackImageView;
    private String vehicleID;
    private VehicleProductFamilyAdapter vehicleMakeAdapter;
    private String vehicleName;
    private ListView vehicleProductFamilyListView;

    Alertbox box = new Alertbox(VehicleProductFamilyActivity.this);

    private String UserType;
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private ImageView cart_icon;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehiclemake_productfamily);

        vehicleID = getIntent().getStringExtra("VEHICLE_ID");
        vehicleName = getIntent().getStringExtra("VEHICLE_NAME");

        navigation_text1 = (TextView) findViewById(R.id.navigation_text1);
        navigation_text2 = (TextView) findViewById(R.id.navigation_text2);
        cart_icon = (ImageView) findViewById(R.id.cart_icon);

        productNameList = new ArrayList<String>();
        productIDList = new ArrayList<String>();
        
        vehicleProductFamilyListView = (ListView) findViewById(R.id.vehicle_listView);

        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();
        
        UserType = myshare.getString("usertype", "").toString();
        if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")||UserType.equals("ASC")))
            cart_icon.setVisibility(View.VISIBLE);
        else
            cart_icon.setVisibility(View.GONE);
        
        
        
        new RetrieveProductFamilyAsyn().execute(new String[]{this.vehicleID});
        vehicleProductFamilyListView.setOnItemClickListener(new OnItemClickListener() {

            public void onItemClick(AdapterView<?> adapterView, View view, int position, long arg3) {
                Intent vehicleProductFamily = new Intent(VehicleProductFamilyActivity.this, VehiclePartItemActivity.class);
                Log.v("Product +++ id", (String) productIDList.get(position));
                vehicleProductFamily.putExtra("VEHICLE_ID", vehicleID);
                vehicleProductFamily.putExtra("PRODUCT_ID", (String) productIDList.get(position));
                vehicleProductFamily.putExtra("VEHICLE_NAME", vehicleName);
                vehicleProductFamily.putExtra("PRODUCT_NAME", (String) productNameList.get(position));
                startActivity(vehicleProductFamily);
            }
        });

        vehicleBackImageView = (ImageView) findViewById(R.id.back);
        vehicleBackImageView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        final ImageView menu = (ImageView) findViewById(R.id.menu);
        menu.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(VehicleProductFamilyActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(VehicleProductFamilyActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(VehicleProductFamilyActivity.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(VehicleProductFamilyActivity.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(VehicleProductFamilyActivity.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(VehicleProductFamilyActivity.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(VehicleProductFamilyActivity.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(VehicleProductFamilyActivity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(VehicleProductFamilyActivity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(VehicleProductFamilyActivity.this, PriceListActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(VehicleProductFamilyActivity.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });



        navigation_text1.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                Intent ho = new Intent(VehicleProductFamilyActivity.this, VehicleMakeActivity.class);
                ho.addFlags(268468224);
                startActivity(ho);
            }
        });

        cart_icon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v)
            {
                CartDAO cartDAO = new CartDAO(getApplicationContext());
                CartDTO cartDTO = cartDAO.GetCartItems();
                if(cartDTO.getPartCodeList() == null)
                {
                    StyleableToast st =
                            new StyleableToast(getApplicationContext(), "Cart is Empty !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else
                {
                    startActivity(new Intent(getApplicationContext(), Quick_Order_Preview_Activity.class).putExtra("from","CartItem"));
                }
            }
        });


    }


    class RetrieveProductFamilyAsyn extends AsyncTask<String, Void, Void> {


        protected void onPreExecute() {
            super.onPreExecute();
            loadDialog = new ProgressDialog(VehicleProductFamilyActivity.this);
            loadDialog.setMessage("Loading...");
            loadDialog.setProgressStyle(0);
            loadDialog.setCancelable(false);
            loadDialog.show();
        }

        protected Void doInBackground(String... position) {
            VehicleDAO connection = new VehicleDAO(VehicleProductFamilyActivity.this);
            VehicleDTO vehicleDAO = new VehicleDTO();
            vehicleDAO = connection.retrieveProductFamilyList(position[0]);
            productIDList = vehicleDAO.getProductIDList();
            productNameList = vehicleDAO.getProductNameList();
            return null;
        }

        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            loadDialog.dismiss();
            vehicleMakeAdapter = new VehicleProductFamilyAdapter(VehicleProductFamilyActivity.this, productNameList);
            vehicleProductFamilyListView.setAdapter(vehicleMakeAdapter);
            navigation_text1.setText(vehicleName);
        }
    }


    public void onBackPressed() {
        super.onBackPressed();
    }
}
