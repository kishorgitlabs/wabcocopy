package vehiclemake;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.mobileordering.R;

import java.util.List;

import addcart.Add_TocartActivity;

import static android.content.Context.MODE_PRIVATE;

public class VehicleServicePartAdapter extends ArrayAdapter<String> {
    Context context;
    private List<String> description;
    private List<String> service_partno,flaglist;
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private String UserType;

    class ViewHolderservice {
        TextView desc;
        TextView servicepartno;
        TextView sno;
        Button addcart;
        ViewHolderservice() {
        }
    }

    public VehicleServicePartAdapter(Context context, List<String> service_partno, List<String> description, List<String> flaglist) {
        super(context, R.layout.activity_vehiclemake_servicepart_text_item, service_partno);
        this.service_partno = service_partno;
        this.description = description;
        this.context = context;
        this.flaglist = flaglist;
        myshare = context.getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();
        UserType = myshare.getString("usertype","").toString();
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderservice viewHolderservice;
        if (convertView == null) {
            convertView = ((LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.activity_vehiclemake_servicepart_text_item, null);
            viewHolderservice = new ViewHolderservice();
            viewHolderservice.sno = (TextView) convertView.findViewById(R.id.sno);
            viewHolderservice.servicepartno = (TextView) convertView.findViewById(R.id.serno);
            viewHolderservice.desc = (TextView) convertView.findViewById(R.id.des);
            viewHolderservice.addcart = (Button) convertView.findViewById(R.id.addcart);
            if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")) && (flaglist.get(position)).equals("0"))
            {
                viewHolderservice.addcart.setVisibility(View.VISIBLE);
            }
            else
            {
                viewHolderservice.addcart.setVisibility(View.GONE);
            }

            convertView.setTag(viewHolderservice);
        } else {
            viewHolderservice = (ViewHolderservice) convertView.getTag();
        }

        viewHolderservice.sno.setText(new StringBuilder(String.valueOf(Integer.toString(position + 1))).append(".").toString());
        viewHolderservice.servicepartno.setText((CharSequence) this.service_partno.get(position));
        viewHolderservice.desc.setText((CharSequence) this.description.get(position));
        viewHolderservice.addcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotocart = new Intent(context, Add_TocartActivity.class);
               gotocart.putExtra("PART_NO", service_partno.get(position));

                gotocart.putExtra("from", "vehicle");
                context.startActivity(gotocart);
            }
        });


        return convertView;
    }

    public int getCount() {
        return this.service_partno.size();
    }
}
