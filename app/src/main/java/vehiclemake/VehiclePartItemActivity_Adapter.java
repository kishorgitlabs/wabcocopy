package vehiclemake;

import static android.content.Context.MODE_PRIVATE;

import java.util.List;

import com.squareup.picasso.Picasso;
import com.mobileordering.R;

import addcart.Add_TocartActivity;
import alertbox.Alertbox;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import wabco.ZoomActivity;


public class VehiclePartItemActivity_Adapter extends ArrayAdapter<String> {

	private Context context;
	private List<String> prodctName;
	private List<String> vehicleDescriptionList;
	private List<String> vehicleFlagList;
	//private VehicleMakeHolder2 vehicleMakeHolder;
	private List<String> vehicleName;
	private List<String> vehiclePartNoList;
	private List<String> product;
	private ImageView expandedImageView;
	private List<String> ProductID;
	private List<String> vehiclePartIDList;

	private SharedPreferences myshare;
	private SharedPreferences.Editor edit;
	private String UserType;

	public VehiclePartItemActivity_Adapter(Context context, List<String> vehiclePartIDList,List<String> vehiclePartNoList, List<String> vehicleDescriptionList, List<String> vehicleName, List<String> prodctName, List<String> vehicleFlagList,List<String> product, List<String> ProductID) {
		super(context, R.layout.activity_vehiclemake_part_item_text_item, vehiclePartNoList);
		//this.vehicleMakeHolder = null;
		this.vehicleDescriptionList = vehicleDescriptionList;
		this.vehiclePartNoList = vehiclePartNoList;
		this.vehiclePartIDList = vehiclePartIDList;
		this.context = context;
		this.vehicleName = vehicleName;
		this.prodctName = prodctName;
		this.vehicleFlagList = vehicleFlagList;
		this.ProductID = ProductID;
		this.product = product;
		Log.v("prodct Name", prodctName.toString());
		Log.v("Vehicle Name ", vehicleName.toString());

		myshare = context.getSharedPreferences("registration", MODE_PRIVATE);
		edit = myshare.edit();

		UserType = myshare.getString("usertype","").toString();
	}
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView = null;

		if (convertView == null) {
			convertView = ((LayoutInflater) this.context.getSystemService("layout_inflater")).inflate(R.layout.activity_vehiclemake_part_item_text_item, null);


			//vehicleMakeHolder = new VehicleMakeHolder2();
			TextView partno = (TextView) convertView.findViewById(R.id.vehicle_partno_textView);
			TextView descrip = (TextView) convertView.findViewById(R.id.vehicle_desp_textView);
			ImageView service  = (ImageView) convertView.findViewById(R.id.vehicle_servicepart);
			ImageView repair = (ImageView) convertView.findViewById(R.id.vehicle_repairkit);
			TextView sno = (TextView) convertView.findViewById(R.id.sno);
			LinearLayout rows = (LinearLayout) convertView.findViewById(R.id.rowlist);
			ImageView logoimage = (ImageView) convertView.findViewById(R.id.customImageVIew1);
			Button addcart = (Button) convertView.findViewById(R.id.addcart);
			if (product.get(position).equals("")) {
				Picasso.with(context).load("file:///android_asset/noimagefound.jpg").error(R.drawable.noimagefound).into(logoimage);
			} else {
				Picasso.with(context).load("file:///android_asset/" + product.get(position) + ".jpg").error(R.drawable.noimagefound).into(logoimage);
			}

			if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")) && (vehicleFlagList.get(position)).equals("0"))
			{
				addcart.setVisibility(View.VISIBLE);
			}
			else
			{
				addcart.setVisibility(View.GONE);
			}
			convertView.setTag(convertView);
			partno.setText((CharSequence) vehiclePartNoList.get(position));
			descrip.setText((CharSequence) vehicleDescriptionList.get(position));
			sno.setText(new StringBuilder(String.valueOf(Integer.toString(position + 1))).append(".").toString());


			if (((String) vehicleFlagList.get(position)).equals("1"))
			{
				partno.setTextColor(context.getResources().getColor(R.color.red));
			}
			else
			{
				partno.setTextColor(Color.BLACK);
			}
			service.setOnClickListener(new OnClickListener() {


				public void onClick(View v) {


						Intent serv = new Intent(context, VehicleServicepartActivity.class);
						serv.putExtra("PART_NO", (String) vehiclePartNoList.get(position));
						serv.putExtra("vehicleName", vehicleName.get(position));
						serv.putExtra("productName", prodctName.get(position));
						serv.putExtra("vehicleIDList", vehiclePartIDList.get(position));
						serv.putExtra("productIDList", ProductID.get(position));
						Log.v("prodctName in service", prodctName.get(position));
						serv.putExtra("Service", "Service Part");
						context.startActivity(serv);



				}
			});
			repair.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					Intent des = new Intent(context, VehicleRepairkitActivity.class);
					des.putExtra("PART_NO", (String) vehiclePartNoList.get(position));
					des.putExtra("vehicleName", vehicleName.get(position));
					des.putExtra("productName", prodctName.get(position));
					des.putExtra("vehicleIDList", vehiclePartIDList.get(position));
					des.putExtra("productIDList", ProductID.get(position));
					des.putExtra("Repair", "Repair Part");
					Log.v("prodctName in Repaire", prodctName.get(position));
					context.startActivity(des);
				}
			});
			expandedImageView =  (ImageView) ((Activity) context).findViewById(R.id.expanded_image);

			logoimage.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {


					if(product.get(position).equals(""))
					{
						//showAlert(vehicleDescriptionList.get(position)+" Image not found ");

						Toast.makeText(context, vehicleDescriptionList.get(position)+" Image not found ", Toast.LENGTH_LONG).show();
					}
					else {
						Intent goZoom = new Intent(context, ZoomActivity.class);
						goZoom.putExtra("ImageName", product.get(position));
						goZoom.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
						context.startActivity(goZoom);
					}
				/*//Picasso.with(context).load("file:///android_asset/"+product.get(position)+".jpg").into(expandedImageView);

				expandedImageView.setVisibility(View.VISIBLE);
				expandedImageView.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						expandedImageView.setVisibility(View.INVISIBLE);
					}
				});*/


				}
			});

			addcart.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent gotocart = new Intent(context, Add_TocartActivity.class);
					gotocart.putExtra("PART_NO", vehiclePartNoList.get(position));
					gotocart.putExtra("vehicleName", vehicleName.get(position));
					gotocart.putExtra("productName", prodctName.get(position));
					gotocart.putExtra("vehicleIDList", vehiclePartIDList.get(position));
					gotocart.putExtra("productIDList", ProductID.get(position));
					gotocart.putExtra("from", "assembly");
					context.startActivity(gotocart);
				}
			});
		}
		else
		{
			//vehicleMakeHolder = (VehicleMakeHolder2) convertView.getTag();
		}


		return convertView;
	}
	
	
	
	private void showAlert(String string) {
		// TODO Auto-generated method stub
		//conadap.clear();
		Alertbox box = new Alertbox(context);
		box.showAlertbox(string);
	}
}

