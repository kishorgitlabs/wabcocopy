package vehiclemake;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnDismissListener;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.mobileordering.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import addcart.CartDAO;
import addcart.CartDTO;
import alertbox.Alertbox;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import network.NetworkConnection;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import quickorder.Quick_Order_Preview_Activity;
import search.SearchActivity;
import wabco.Network_Activity;


public class VehicleMakeActivity extends Activity {
  private ProgressDialog loadDialog;
  private List<Integer> VehicleImageList;
  private ImageView vehicleBackImageView;
  private List<String> vehicleIDList, productIDList;
  private VehicleMakeAdapter vehicleMakeAdapter;
  private ListView vehicleMakeListView;
  private List<String> vehicleNameList;
  private List<Integer> ApplicationPdf;
  private long enqueue;
  private DownloadManager dm;

  ImageView Cart_Icon;

  String urll = "http://demo.brainmagic.info/wabco/";
  Alertbox box = new Alertbox(VehicleMakeActivity.this);
  private SharedPreferences myshare;
  private SharedPreferences.Editor edit;
  private String UserType;

  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_vehiclemake);


    myshare = getSharedPreferences("registration", MODE_PRIVATE);
    edit = myshare.edit();

    Cart_Icon = (ImageView) findViewById(R.id.cart_icon);
    this.vehicleBackImageView = (ImageView) findViewById(R.id.back);

    this.vehicleNameList = new ArrayList<String>();
    this.vehicleIDList = new ArrayList<String>();

    UserType = myshare.getString("usertype", "").toString();
    if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")||UserType.equals("ASC")))
      Cart_Icon.setVisibility(View.VISIBLE);
    else
      Cart_Icon.setVisibility(View.GONE);



    this.vehicleBackImageView.setOnClickListener(new OnClickListener() {

      public void onClick(View arg0) {
        onBackPressed();
      }
    });



    Cart_Icon.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        CartDAO cartDAO = new CartDAO(getApplicationContext());
        CartDTO cartDTO = cartDAO.GetCartItems();
        if (cartDTO.getPartCodeList() == null) {
          StyleableToast st =
              new StyleableToast(getApplicationContext(), "Cart is Empty !", Toast.LENGTH_SHORT);
          st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();
        } else {
          startActivity(new Intent(getApplicationContext(), Quick_Order_Preview_Activity.class)
              .putExtra("from", "CartItem"));
        }
        // startActivity(new Intent(ProductFamilyActivity.this, Cart_Activity.class));
      }
    });



    ((Button) findViewById(R.id.asbapp)).setOnClickListener(new OnClickListener() {

      public void onClick(View v) {


        NetworkConnection net = new NetworkConnection(VehicleMakeActivity.this);
        boolean isnet = net.CheckInternet();
        if (isnet) {

          String com = urll + "ABS Ready.pdf";
          dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
          Log.v("file name", com);
          String url = com.replaceAll(" ", "%20");
          Request request = new Request(Uri.parse(url));
          enqueue = dm.enqueue(request);

          showDownload();
          com = "";
        } else {

          box.showAlertbox(
              "Not able to connect please check your network connection and try again.");

        }

      }
    });

    this.VehicleImageList = Arrays.asList(R.drawable.amw_logo, R.drawable.ashoklayland,
            R.drawable.daimler, R.drawable.lcv,
            R.drawable.mantrucs, R.drawable.mahindra,
            R.drawable.swaraj_mazda, R.drawable.tata,
            R.drawable.volvo);

    this.vehicleMakeListView = (ListView) findViewById(R.id.vehicle_listView);
    new RetrieveFileAsyn().execute();


    final ImageView menu = (ImageView) findViewById(R.id.menu);
    menu.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        Context wrapper = new ContextThemeWrapper(VehicleMakeActivity.this, R.style.PopupMenu);
        final PopupMenu pop = new PopupMenu(wrapper, v);
        pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {

          public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
              case R.id.home:
                startActivity(new Intent(VehicleMakeActivity.this, MainActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
              case R.id.search:
                startActivity(new Intent(VehicleMakeActivity.this, SearchActivity.class));
                break;
              case R.id.notification:
                startActivity(new Intent(VehicleMakeActivity.this, NotificationActivity.class));
                break;
              case R.id.vehicle:
                startActivity(new Intent(VehicleMakeActivity.this, VehicleMakeActivity.class));
                break;
              case R.id.product:
                startActivity(new Intent(VehicleMakeActivity.this, ProductFamilyActivity.class));
                break;
              case R.id.performance:
                startActivity(new Intent(VehicleMakeActivity.this, PE_Kit_Activity.class));
                break;
              case R.id.contact:
                startActivity(new Intent(VehicleMakeActivity.this, Network_Activity.class));
                break;
              case R.id.askwabco:
                startActivity(new Intent(VehicleMakeActivity.this, AskWabcoActivity.class));
                break;
              case R.id.pricelist:
                startActivity(new Intent(VehicleMakeActivity.this, PriceListActivity.class));
                break;
              case R.id.update:
                WabcoUpdate update = new WabcoUpdate(VehicleMakeActivity.this);
                update.checkVersion();
                break;
            }
            return false;
          }
        });
        pop.setOnDismissListener(new OnDismissListener() {

          @Override
          public void onDismiss(PopupMenu arg0) {
            // TODO Auto-generated method stub
            pop.dismiss();
          }
        });

        pop.inflate(R.menu.main);
        pop.show();
      }
    });


  }


  class RetrieveFileAsyn extends AsyncTask<Void, Void, String> {

    protected void onPreExecute() {
      super.onPreExecute();
      loadDialog = new ProgressDialog(VehicleMakeActivity.this);
      loadDialog.setMessage("Loading...");
      loadDialog.setProgressStyle(0);
      loadDialog.setCancelable(false);
      loadDialog.show();
    }

    protected String doInBackground(Void... arg0) {
      VehicleDAO connection = new VehicleDAO(VehicleMakeActivity.this);
      VehicleDTO vehicleDTO = new VehicleDTO();
      vehicleDTO = connection.retrieveALL();
      vehicleIDList = vehicleDTO.getVehicleIDList();
      vehicleNameList = vehicleDTO.getVehicleNameList();
      productIDList = vehicleDTO.getProductIDList();

      if (vehicleIDList.isEmpty()) {
        return "nodata";
      }

      return "data";

    }

    @SuppressWarnings("deprecation")
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      loadDialog.dismiss();
      if (result.equals("data")) {
        vehicleMakeAdapter = new VehicleMakeAdapter(VehicleMakeActivity.this, vehicleNameList,
            VehicleImageList, vehicleIDList, ApplicationPdf);
        vehicleMakeListView.setAdapter(vehicleMakeAdapter);
        return;
      }
      box.showAlertbox("No CancelPartData Found !");

    }
  }


  public void showDownload() {
    Intent i = new Intent();
    i.setAction(DownloadManager.ACTION_VIEW_DOWNLOADS);
    startActivity(i);
  }


}
