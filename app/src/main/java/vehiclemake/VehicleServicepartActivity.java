package vehiclemake;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnDismissListener;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.mobileordering.R;

import java.util.List;

import addcart.CartDAO;
import addcart.CartDTO;
import alertbox.Alertbox;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import quickorder.Quick_Order_Preview_Activity;
import search.SearchActivity;
import wabco.Network_Activity;


public class VehicleServicepartActivity extends Activity {

    HorizontalScrollView hsv;
    private ProgressDialog loadDialog;
    private ListView partlist;
    String partnunmber;
    String productName;
    private ImageView vehicleBackImageView;
    public List<String> vehicleDescriptionList;
    private String vehicleName;
    public List<String> vehiclePartNo;
    public List<String> vehicleRepaireKitNOList,vehicleReparKitFlagList;
    private String vehicleIDList, productIDList;
    Alertbox box = new Alertbox(VehicleServicepartActivity.this);
    ImageView Cart_Icon;
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private String UserType;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehiclemake_servicepart);
        this.hsv = (HorizontalScrollView) findViewById(R.id.horilist);

        this.vehicleBackImageView = (ImageView) findViewById(R.id.back);
        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();

        Cart_Icon = (ImageView) findViewById(R.id.cart_icon);

        UserType = myshare.getString("usertype", "").toString();
        if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")))
            Cart_Icon.setVisibility(View.VISIBLE);
        else
            Cart_Icon.setVisibility(View.GONE);

        this.vehicleBackImageView.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {
                onBackPressed();
            }
        });



        Intent partno = getIntent();
        this.partnunmber = partno.getStringExtra("PART_NO");
        this.vehicleName = partno.getStringExtra("vehicleName");
        this.productName = partno.getStringExtra("productName");
        this.vehicleIDList = partno.getStringExtra("vehicleIDList");
        this.productIDList = partno.getStringExtra("productIDList");

        TextView head = (TextView) findViewById(R.id.head_textView1);
        TextView head2 = (TextView) findViewById(R.id.head_textView2);
        TextView head3 = (TextView) findViewById(R.id.head_textView3);
        head.setText(this.vehicleName);
        head2.setText(this.productName);
        head3.setText(this.partnunmber);

        //Log.v("Part no frm intent", this.partnunmber);
        //Log.v("productName frm intent", this.productName);
        Cart_Icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                CartDAO cartDAO = new CartDAO(getApplicationContext());
                CartDTO cartDTO = cartDAO.GetCartItems();
                if(cartDTO.getPartCodeList() == null)
                {
                    StyleableToast st =
                            new StyleableToast(getApplicationContext(), "Cart is Empty !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else
                {
                    startActivity(new Intent(getApplicationContext(), Quick_Order_Preview_Activity.class).putExtra("from","CartItem"));
                }
                //  startActivity(new Intent(ProductFamilyActivity.this, Cart_Activity.class));
            }
        });




        head.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(VehicleServicepartActivity.this, VehicleMakeActivity.class));
            }
        });

        head2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        final ImageView menu = (ImageView) findViewById(R.id.menu);
        menu.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(VehicleServicepartActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(VehicleServicepartActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(VehicleServicepartActivity.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(VehicleServicepartActivity.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(VehicleServicepartActivity.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(VehicleServicepartActivity.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(VehicleServicepartActivity.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(VehicleServicepartActivity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(VehicleServicepartActivity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(VehicleServicepartActivity.this, PriceListActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(VehicleServicepartActivity.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });


        this.partlist = (ListView) findViewById(R.id.vehicle_listView);
        new RetrievePartItemAsyn().execute(new String[]{this.partnunmber});
    }


    class RetrievePartItemAsyn extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            loadDialog = new ProgressDialog(VehicleServicepartActivity.this);
            loadDialog.setMessage("Loading...");
            loadDialog.setProgressStyle(0);
            loadDialog.setCancelable(false);
            loadDialog.show();
        }

        protected String doInBackground(String... position) {
            VehicleDAO connection = new VehicleDAO(VehicleServicepartActivity.this);
            VehicleDTO vehicleDAO = new VehicleDTO();
            vehicleDAO = connection.retrievesservicepart(position[0], vehicleIDList, productIDList);
            vehiclePartNo = vehicleDAO.getPartNoList();
            vehicleRepaireKitNOList = vehicleDAO.getProductservicepartNOList();
            vehicleDescriptionList = vehicleDAO.getPartDespcriptionList();
            vehicleReparKitFlagList = vehicleDAO.getFlagList();
            if (vehiclePartNo.isEmpty()) {
                return "Empty";
            }
            Log.v("part no servicepart", (String) vehiclePartNo.get(0));
            Log.v("RepaireKitNO ", (String) vehicleRepaireKitNOList.get(0));
            Log.v("Description servicepart", (String) vehicleDescriptionList.get(0));
            return "success";
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            loadDialog.dismiss();
            if (result.equals("Empty")) {
                box.showNegativebox("Service part is Not Available ");
            }
            VehicleServicePartAdapter service = new VehicleServicePartAdapter(VehicleServicepartActivity.this, vehicleRepaireKitNOList, vehicleDescriptionList,vehicleReparKitFlagList);

            partlist.setAdapter(service);
        }
    }


    public void onBackPressed() {
        super.onBackPressed();
    }
}
