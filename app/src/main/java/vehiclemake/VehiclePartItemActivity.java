package vehiclemake;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnDismissListener;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.mobileordering.R;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import addcart.CartDAO;
import addcart.CartDTO;
import alertbox.Alertbox;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import pricelist.PriceListActivity;
import productfamily.ProductFamilyActivity;
import quickorder.Quick_Order_Preview_Activity;
import search.SearchActivity;
import wabco.Network_Activity;

public class VehiclePartItemActivity extends Activity {

    private TextView Title;
    private LinearLayout flagtext;
    private TextView headText1;
    private TextView headText2;
    private TextView headText3;
    private HorizontalScrollView hsv;
    private ListView listpart;
    private ProgressDialog loadDialog;
    private int[] logoimages;

    private List<String> productName;
    private List<String> vehicleName;

    private ImageView vehicleBackImageView;
    private List<String> vehicleDescriptionList;
    public List<String> vehicleFlagList;
    private String vehicleID;
    private List<String> vehicleImageList;
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private List<String> vehiclePartNoList;
    private String vehicleProductFamilyID;
    public List<Bitmap> productbitmap;
    private Bitmap bitmap;
    private List<String> vehicleIDList, productIDList;
    private Alertbox box = new Alertbox(VehiclePartItemActivity.this);
    private TextView addBtn;
    private ImageView cart_icon;
    private String UserType;
    private FloatingActionButton arrleft,arrright;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehiclemake_partitem);
        this.vehicleProductFamilyID = getIntent().getStringExtra("VEHICLE_PRODUCTFAMILY_ID");


        productName = new ArrayList<String>();
        vehicleName = new ArrayList<String>();
        productIDList = new ArrayList<String>();

        vehicleID = getIntent().getStringExtra("VEHICLE_ID");
        vehicleName.add(getIntent().getStringExtra("VEHICLE_NAME"));
        productName.add(getIntent().getStringExtra("PRODUCT_NAME"));
        productIDList.add(getIntent().getStringExtra("PRODUCT_ID"));

        cart_icon = (ImageView) findViewById(R.id.cart_icon);
        arrleft = (FloatingActionButton) findViewById(R.id.left);
        arrright = (FloatingActionButton) findViewById(R.id.right);

        headText1 = (TextView) findViewById(R.id.head_textView1);
        headText2 = (TextView) findViewById(R.id.head_textView2);
        headText3 = (TextView) findViewById(R.id.head_textView3);

        productbitmap = new ArrayList<Bitmap>();
        flagtext = (LinearLayout) findViewById(R.id.flagtxt);
        Title = (TextView) findViewById(R.id.tittle);
        headText1.setText(vehicleName.get(0));
        headText3.setText("Product Family");
        headText3.setText(productName.get(0));
        Title.setText(productName.get(0));
        hsv = (HorizontalScrollView) findViewById(R.id.horilist);
        listpart = (ListView) findViewById(R.id.listView);

        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();


        UserType = myshare.getString("usertype", "").toString();
        if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")||UserType.equals("ASC")))
            cart_icon.setVisibility(View.VISIBLE);
        else
            cart_icon.setVisibility(View.GONE);


        logoimages = new int[]{R.drawable.ashoklayland, R.drawable.tata, R.drawable.lcv, R.drawable.volvo, R.drawable.sawaraj, R.drawable.mantrucs, R.drawable.mahindra, R.drawable.daimler};

        new RetrievePartItemAsyn().execute(new String[]{productIDList.get(0)});




        arrright.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                hsv.smoothScrollTo(hsv.getScrollX() + 1000, hsv.getScrollY());
                Log.v("inside image click", "yeah");
                arrright.setVisibility(View.GONE);
                arrleft.setVisibility(View.VISIBLE);
            }
        });
        arrleft.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                hsv.smoothScrollTo(hsv.getScrollX() - 1000, hsv.getScrollY());
                arrleft.setVisibility(View.GONE);
                arrright.setVisibility(View.VISIBLE);
            }
        });
        final ImageView menu = (ImageView) findViewById(R.id.menu);
        menu.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(VehiclePartItemActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(VehiclePartItemActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(VehiclePartItemActivity.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(VehiclePartItemActivity.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(VehiclePartItemActivity.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(VehiclePartItemActivity.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(VehiclePartItemActivity.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(VehiclePartItemActivity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(VehiclePartItemActivity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(VehiclePartItemActivity.this, PriceListActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(VehiclePartItemActivity.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });


        vehicleBackImageView = (ImageView) findViewById(R.id.back);
        vehicleBackImageView.setOnClickListener(new OnClickListener() {

            public void onClick(View arg0) {
                onBackPressed();
            }
        });
        headText1.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                startActivity(new Intent(VehiclePartItemActivity.this, VehicleMakeActivity.class));
            }
        });
        headText2.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                onBackPressed();
            }
        });


        cart_icon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                CartDAO cartDAO = new CartDAO(getApplicationContext());
                CartDTO cartDTO = cartDAO.GetCartItems();
                if(cartDTO.getPartCodeList() == null)
                {
                    StyleableToast st =
                            new StyleableToast(getApplicationContext(), "Cart is Empty !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else
                {
                    startActivity(new Intent(getApplicationContext(), Quick_Order_Preview_Activity.class).putExtra("from","CartItem"));
                }

            }
        });


    }


    class RetrievePartItemAsyn extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            loadDialog = new ProgressDialog(VehiclePartItemActivity.this);
            loadDialog.setMessage("Loading...");
            loadDialog.setProgressStyle(0);
            loadDialog.setCancelable(false);
            loadDialog.show();
        }

        protected String doInBackground(String... position) {
            try {
                VehicleDAO connection = new VehicleDAO(VehiclePartItemActivity.this);
                VehicleDTO vehicleDAO = new VehicleDTO();
                vehicleDAO = connection.retrievePartItemList(position[0], vehicleID, productName.get(0).toString(), vehicleName.get(0).toString());
                vehicleDAO.getPartNoIDList();
                vehiclePartNoList = vehicleDAO.getPartNoList();
                vehicleIDList = vehicleDAO.getVehiclePartProductNOList();
                vehicleDescriptionList = vehicleDAO.getPartDespcriptionList();
                vehicleImageList = vehicleDAO.getPartImageList();
                vehicleFlagList = vehicleDAO.getFlagList();
                productName = vehicleDAO.getProductNameList();
                vehicleName = vehicleDAO.getVehicleNameList();
                productIDList = vehicleDAO.getProductIDList();

                if (vehicleDescriptionList.isEmpty()) {
                    return "unsuccess";
                }
                return "success";
            } catch (Exception e) {
                Log.v("Error", e.getMessage());
                return "unsuccess";
            }
        }

        @SuppressWarnings("deprecation")
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            loadDialog.dismiss();
            if (result.equals("success")) {

                if (!vehicleFlagList.isEmpty()) {
                    flagtext.setVisibility(View.VISIBLE);
                }

                listpart.setAdapter(new VehiclePartItemActivity_Adapter(VehiclePartItemActivity.this, vehicleIDList, vehiclePartNoList, vehicleDescriptionList, vehicleName, productName, vehicleFlagList, vehicleImageList, productIDList));
                arrright.setVisibility(View.VISIBLE);

            } else {
                box.showAlertbox("No data found !");
                flagtext.setVisibility(View.GONE);
            }
        }
    }

    private Bitmap downloadImage(String url) {

        try {
            InputStream fileInputStream = getAssets().open(url);
            Options options = new Options();
            options.inScaled = false;
            options.inSampleSize = 8;
            bitmap = BitmapFactory.decodeStream(fileInputStream);
            fileInputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
            Log.v("Error", e.getMessage());

            return downloadImage("noimagefound");

        }
        return bitmap;
    }


    public void onBackPressed() {
        super.onBackPressed();
    }
}
