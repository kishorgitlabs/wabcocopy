package pricelist;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.mobileordering.R;

import java.util.ArrayList;
import java.util.List;

import adapter.AutocompleteAdapter;
import addcart.CartDAO;
import addcart.CartDTO;
import alertbox.Alertbox;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import productfamily.ProductFamilyActivity;
import quickorder.Quick_Order_Preview_Activity;
import search.SearchActivity;
import search.SearchDAO;
import search.SearchDTO;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;

import static com.mobileordering.R.id.listView;


public class PriceListActivity extends Activity {


    private List<String> type;
    private Button go;
    private AutoCompleteTextView partword, descword;
    private MaterialSpinner from;
    private FloatingActionButton arrleft, arrright;
    private List<String> PartcodeList, PartnumberList, DescriptionList, Price_List, MrpList, CatagoryList, ImageList, DescriptionListAuto, Partcodeautolist;
    private ArrayAdapter<String> DataAdapter;
    private String Item;
    private ProgressDialog loadDialog;
    private String searchWord;
    Alertbox alert = new Alertbox(PriceListActivity.this);
    private PriceListAdapter priceListAdapter;
    private ListView listview;
    private HorizontalScrollView hsv;
    private ImageView backImageView;
    private String SelectedItem;
    ImageView Cart_Icon;
    private SharedPreferences myshare;
    private SharedPreferences.Editor edit;
    private String UserType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price_list);
        go = (Button) findViewById(R.id.go);

        partword = (AutoCompleteTextView) findViewById(R.id.search_partno);
        descword = (AutoCompleteTextView) findViewById(R.id.search_desc);
        Cart_Icon = (ImageView) findViewById(R.id.cart_icon);

        from = (MaterialSpinner) findViewById(R.id.spinner1);
        from.setBackgroundResource(R.drawable.autotextback);
        arrleft = (FloatingActionButton) findViewById(R.id.left);
        arrright = (FloatingActionButton) findViewById(R.id.right);
        hsv = (HorizontalScrollView) findViewById(R.id.horilist);
        hsv.setVisibility(View.GONE);

        DescriptionListAuto = new ArrayList<String>();
        Partcodeautolist = new ArrayList<String>();


        PartcodeList = new ArrayList<String>();
        PartnumberList = new ArrayList<String>();
        DescriptionList = new ArrayList<String>();
        Price_List = new ArrayList<String>();
        MrpList = new ArrayList<String>();
        CatagoryList = new ArrayList<String>();
        ImageList = new ArrayList<String>();

        listview = (ListView) findViewById(listView);

        myshare = getSharedPreferences("registration", MODE_PRIVATE);
        edit = myshare.edit();

        UserType = myshare.getString("usertype","").toString();
        if ((UserType.equals("Dealer") || UserType.equals("OEM Dealer")))
        {
            Cart_Icon.setVisibility(View.VISIBLE);
        }
        type = new ArrayList<String>();

        type.add("Select part type");
        type.add("All");
        type.add("Assembly");
        type.add("Service Parts");
        type.add("Repair Kits");
        type.add("PE kits");
        type.add("ABS");
//        type.add("ABS Kit");
        type.add("V Belt");
        type.add("Special Tools");


        from.setItems(type);
        from.setPadding(30, 0, 0, 0);
        from.setTextColor(Color.BLACK);


        arrright.setOnClickListener(new View.OnClickListener() {

            @SuppressLint("RestrictedApi")
            public void onClick(View v) {
                hsv.smoothScrollTo(hsv.getScrollX() + 2000, hsv.getScrollY());
                Log.v("inside image click", "yeah");
                arrright.setVisibility(View.GONE);
                arrleft.setVisibility(View.VISIBLE);
            }
        });
        arrleft.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            public void onClick(View v) {
                hsv.smoothScrollTo(hsv.getScrollX() - 2000, hsv.getScrollY());
                arrleft.setVisibility(View.GONE);
                arrright.setVisibility(View.VISIBLE);
            }
        });


        from.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                if (!item.toString().equals("Select Type")) {
                    Item =item.toString();

                    if (!Item.equals("Select part type")) {
                        if (Item.equals("Assembly")) {
                            SelectedItem = "ASSEMBLY PARTS";
                            new GetPartNoAutocomplete().execute(SelectedItem);
                            new GetDescAutocomplete().execute(SelectedItem);
                        } else if (Item.equals("Repair Kits")) {
                            SelectedItem = "REPAIR KITS";
                            new GetPartNoAutocomplete().execute(SelectedItem);
                            new GetDescAutocomplete().execute(SelectedItem);
                        } else if (Item.equals("ABS")) {
                            SelectedItem = "ABS PARTS";
                            new GetPartNoAutocomplete().execute(SelectedItem);
                            new GetDescAutocomplete().execute(SelectedItem);
                        } else if (Item.equals("V Belt")) {
                            SelectedItem = "V BELT";
                            new GetPartNoAutocomplete().execute(SelectedItem);
                            new GetDescAutocomplete().execute(SelectedItem);
                        } else if (Item.equals("Service Parts")) {
                            SelectedItem = "SERVICE PARTS";
                            new GetPartNoAutocomplete().execute(SelectedItem);
                            new GetDescAutocomplete().execute(SelectedItem);
                        } else if(Item.equals("PE kits")) {
                            SelectedItem = "PE KIT";
                            new GetPartNoAutocomplete().execute(SelectedItem);
                            new GetDescAutocomplete().execute(SelectedItem);
                        }else if(Item.equals("Special Tools")) {
                            SelectedItem = "SPECIAL TOOLS";
                            new GetPartNoAutocomplete().execute(SelectedItem);
                            new GetDescAutocomplete().execute(SelectedItem);
                        }
//                        else if (Item.equals("ABS Kit")){
//                            SelectedItem = "ABS KIT";
//                            new GetPartNoAutocomplete().execute(SelectedItem);
//                            new GetDescAutocomplete().execute(SelectedItem);
//                        }
                        else
                        {
                            SelectedItem = "All";
                            new GetPartNoAutocomplete().execute("All");
                            new GetDescAutocomplete().execute("All");
                        }
                    }
                }
            }
        });



        go.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (Item == null || Item.equals("Select part type"))
                {
                    //Toast.makeText(PriceListActivity.this, "Please select part type", Toast.LENGTH_LONG).show();
                    StyleableToast st =
                            new StyleableToast(getApplicationContext(), "Please select part type", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();

                }
                else if(Item == null || Item.equals("All")) {
                    if (partword.getText().length() != 0 && descword.getText().length() != 0) {
//                        alert.showAlertbox("Please enter either part number or description !");
                    }
                    else if (partword.getText().length() == 0) {
                        searchWord = descword.getText().toString();
                        new Retrivesevicesearch().execute("desc");
                    } else {
                        searchWord = partword.getText().toString();
                        new Retrivesevicesearch().execute("part");
                    }
                }
                else
                {
                    if (partword.getText().length() != 0 && descword.getText().length() != 0) {
//                        alert.showAlertbox("Please enter either part number or description !");
                    } else if (partword.getText().length() == 0) {
                        searchWord = descword.getText().toString();
                        new Retrivesevicesearch().execute("desc");
                    } else {
                        searchWord = partword.getText().toString();
                        new Retrivesevicesearch().execute("part");
                    }
                }
            }
        });


        backImageView = (ImageView) findViewById(R.id.back);
        backImageView.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                onBackPressed();
            }
        });


        Cart_Icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                CartDAO cartDAO = new CartDAO(getApplicationContext());
                CartDTO cartDTO = cartDAO.GetCartItems();
                if(cartDTO.getPartCodeList() == null)
                {
                    StyleableToast st =
                            new StyleableToast(getApplicationContext(), "Cart is Empty !", Toast.LENGTH_SHORT);
                    st.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.red));
                    st.setTextColor(Color.WHITE);
                    st.setMaxAlpha();
                    st.show();
                }else
                {
                    startActivity(new Intent(getApplicationContext(), Quick_Order_Preview_Activity.class).putExtra("from","CartItem"));
                }
                //  startActivity(new Intent(ProductFamilyActivity.this, Cart_Activity.class));
            }
        });




        final ImageView menu = (ImageView) findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Context wrapper = new ContextThemeWrapper(PriceListActivity.this, R.style.PopupMenu);
                final PopupMenu pop = new PopupMenu(wrapper, v);
                pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.home:
                                startActivity(new Intent(PriceListActivity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
                            case R.id.search:
                                startActivity(new Intent(PriceListActivity.this, SearchActivity.class));
                                break;
                            case R.id.notification:
                                startActivity(new Intent(PriceListActivity.this, NotificationActivity.class));
                                break;
                            case R.id.vehicle:
                                startActivity(new Intent(PriceListActivity.this, VehicleMakeActivity.class));
                                break;
                            case R.id.product:
                                startActivity(new Intent(PriceListActivity.this, ProductFamilyActivity.class));
                                break;
                            case R.id.performance:
                                startActivity(new Intent(PriceListActivity.this, PE_Kit_Activity.class));
                                break;
                            case R.id.contact:
                                startActivity(new Intent(PriceListActivity.this, Network_Activity.class));
                                break;
                            case R.id.askwabco:
                                startActivity(new Intent(PriceListActivity.this, AskWabcoActivity.class));
                                break;
                            case R.id.pricelist:
                                startActivity(new Intent(PriceListActivity.this, PriceListActivity.class));
                                break;
                            case R.id.update:
                                WabcoUpdate update = new WabcoUpdate(PriceListActivity.this);
                                update.checkVersion();
                                break;
                        }
                        return false;
                    }
                });
                pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

                    @Override
                    public void onDismiss(PopupMenu arg0) {
                        // TODO Auto-generated method stub
                        pop.dismiss();
                    }
                });

                pop.inflate(R.menu.main);
                pop.show();
            }
        });

    }


    class Retrivesevicesearch extends AsyncTask<String, Void, String> {
        protected void onPreExecute() {
            super.onPreExecute();
            loadDialog = new ProgressDialog(PriceListActivity.this);
            loadDialog.setMessage("Loading...");
            loadDialog.setProgressStyle(0);
            loadDialog.setCancelable(false);
            loadDialog.show();
            DescriptionList.clear();
            PartcodeList.clear();
            PartnumberList.clear();
            CatagoryList.clear();
            Price_List.clear();
            MrpList.clear();
            ImageList.clear();
            Log.v("Calling ", "Service Repair kit searchpart");
        }

        protected String doInBackground(String... from) {
            SearchDAO searchDAO = new SearchDAO(PriceListActivity.this);
            SearchDTO searchDTO = new SearchDTO();
            searchDTO = searchDAO.retriveForPriceList(searchWord, SelectedItem,from[0]);
            DescriptionList = searchDTO.getDescriptionList();
            PartcodeList = searchDTO.getpartCodeList();
            PartnumberList = searchDTO.getpartnoList();
            CatagoryList = searchDTO.getCatagoryList();
            Price_List = searchDTO.getPrice_List();
            MrpList = searchDTO.getMrpList();
            ImageList = searchDTO.getImageList();

            if (DescriptionList.isEmpty()) {
                return "Empty";
            }
            return "success";
        }

        @SuppressLint("RestrictedApi")
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            loadDialog.dismiss();
            if (result.equals("Empty")) {
                alert.showAlertbox("No data found !");
                priceListAdapter = new PriceListAdapter(PriceListActivity.this, PartnumberList, PartcodeList, DescriptionList, Price_List, MrpList, CatagoryList, ImageList);
                listview.setAdapter(priceListAdapter);
                hsv.setVisibility(View.GONE);
                arrright.setVisibility(View.GONE);
                arrleft.setVisibility(View.GONE);
            }
            else {
                priceListAdapter = new PriceListAdapter(PriceListActivity.this, PartnumberList, PartcodeList, DescriptionList, Price_List, MrpList, CatagoryList, ImageList);
                listview.setAdapter(priceListAdapter);
                arrright.setVisibility(View.VISIBLE);
                hsv.setVisibility(View.VISIBLE);
            }
        }
    }

    class GetPartNoAutocomplete extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Partcodeautolist.clear();
        }

        protected String doInBackground(String... type) {
            SearchDAO searchDAO = new SearchDAO(PriceListActivity.this);
            SearchDTO searchDTO = new SearchDTO();
            searchDTO = searchDAO.PricePartNoList(type[0]);
            Partcodeautolist = searchDTO.getAutocompletepartcode();
            return "success";
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            /*ArrayAdapter<String> adapter = new ArrayAdapter<String>
                    (PriceListActivity.this, R.layout.simple_spinner_item, PartnumberListAuto);
            partword.setThreshold(0);
            partword.setAdapter(adapter);*/

            AutocompleteAdapter adapter = new AutocompleteAdapter(PriceListActivity.this, R.layout.simple_spinner_item, Partcodeautolist);
            partword.setThreshold(0);
            partword.setAdapter(adapter);


        }
    }

    class GetDescAutocomplete extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            DescriptionListAuto.clear();
        }

        protected String doInBackground(String... type) {
            SearchDAO searchDAO = new SearchDAO(PriceListActivity.this);
            SearchDTO searchDTO = new SearchDTO();
            searchDTO = searchDAO.PriceDescList(type[0]);

            DescriptionListAuto = searchDTO.getAutocompleteDescriptionList();

            return "success";
        }

        protected void onPostExecute(String result) {
            super.onPostExecute(result);

          /*  ArrayAdapter<String> adapter = new ArrayAdapter<String>
                    (PriceListActivity.this, R.layout.simple_spinner_item, DescriptionListAuto);
            descword.setThreshold(2);
            descword.setAdapter(adapter);*/


            AutocompleteAdapter adapter = new AutocompleteAdapter(PriceListActivity.this, R.layout.simple_spinner_item, DescriptionListAuto);
            descword.setThreshold(0);
            descword.setAdapter(adapter);

        }
    }

}
