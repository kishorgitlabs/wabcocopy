package quickorder;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.PopupMenu;
import android.text.InputFilter;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.goodiebag.horizontalpicker.HorizontalPicker;
import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.mobileordering.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import adapter.AutocompleteAdapter;
import adapter.InputFilterMinMax;
import addcart.CartDAO;
import addcart.CartDTO;
import askwabco.AskWabcoActivity;
import directory.WabcoUpdate;
import home.MainActivity;
import home.SpinnerDialog;
import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import models.quickorder.searchautocomplete.SampleModel;
import notification.NotificationActivity;
import pekit.PE_Kit_Activity;
import pricelist.PriceListActivity;
import search.SearchActivity;
import productfamily.ProductFamilyActivity;
import search.SearchDAO;
import search.SearchDTO;
import vehiclemake.VehicleMakeActivity;
import wabco.Network_Activity;


public class Quick_Order_Activity extends Activity {

  private AutoCompleteTextView Part_edit;
  private EditText Desc_edit;
  private EditText Quantity_edit;
  private Button Add_btn;
  private Button Preview_btn;
  private TextView your_Details_text, dealer_Name_text, dealer_Mobile_text, dealer_Address_text;
  private TextView dealer_Name, dealer_Mobile, dealer_Address;
  private ArrayList<String> PartnumberListAuto;
  private String description;
  private SharedPreferences myshare;
  private SharedPreferences.Editor edit;
  private CartDAO cartDAO;
  private TextView tittle;
  private Locale myLocale;
  private String lang;
  private ImageView back;
  private SpinnerDialog spinnerDialog;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_quick__order);

    myshare = getSharedPreferences("registration", MODE_PRIVATE);
    edit = myshare.edit();

    cartDAO = new CartDAO(Quick_Order_Activity.this);
    HorizontalPicker hpText = (HorizontalPicker) findViewById(R.id.hpicker);
    your_Details_text = (TextView) findViewById(R.id.your_Details);
    dealer_Name_text = (TextView) findViewById(R.id.dealer_Name);
    dealer_Mobile_text = (TextView) findViewById(R.id.dealer_Mobile);
    dealer_Address_text = (TextView) findViewById(R.id.dealer_Address);

    dealer_Name = (TextView) findViewById(R.id.dealer_name);
    dealer_Mobile = (TextView) findViewById(R.id.dealer_mobile);
    dealer_Address = (TextView) findViewById(R.id.dealer_address);
    tittle = (TextView) findViewById(R.id.tittle);

    Add_btn = (Button) findViewById(R.id.additem);
    Preview_btn = (Button) findViewById(R.id.preview);


    Part_edit = (AutoCompleteTextView) findViewById(R.id.part_edit);
    Desc_edit = (EditText) findViewById(R.id.desc_edit);
    Quantity_edit = (EditText) findViewById(R.id.quantity_edit);

    Desc_edit.setFocusable(false);
    Desc_edit.setEnabled(false);


    dealer_Name.setText(myshare.getString("shopname", ""));
    dealer_Mobile.setText(myshare.getString("phone", ""));
    dealer_Address.setText(myshare.getString("brainmagic", ""));

    PartnumberListAuto = new ArrayList<String>();


    List<HorizontalPicker.PickerItem> textItems = new ArrayList<>();
    textItems.add(new HorizontalPicker.TextItem("English"));
    textItems.add(new HorizontalPicker.TextItem("Tamil"));
    textItems.add(new HorizontalPicker.TextItem("Hindi"));
    hpText.setItems(textItems, 3);
    new GetPartNoAutocomplete().execute();


    if (myshare.getString("language_code", "").equals("ta")) {
      Locale locale = new Locale("ta");
      Locale.setDefault(locale);
      Configuration conf = getResources().getConfiguration();
      conf.setLocale(locale);
      getBaseContext().getResources().updateConfiguration(conf,
              getBaseContext().getResources().getDisplayMetrics());
      hpText.setSelectedIndex(1);
    } else if (myshare.getString("language_code", "").equals("en")) {
      Locale locale = new Locale("en");
      Locale.setDefault(locale);
      Configuration conf = getResources().getConfiguration();
      conf.setLocale(locale);
      getBaseContext().getResources().updateConfiguration(conf,
              getBaseContext().getResources().getDisplayMetrics());
      hpText.setSelectedIndex(0);
    } else if (myshare.getString("language_code", "").equals("hi")) {
      Locale locale = new Locale("hi");
      Locale.setDefault(locale);
      Configuration conf = getResources().getConfiguration();
      conf.setLocale(locale);
      getBaseContext().getResources().updateConfiguration(conf,
              getBaseContext().getResources().getDisplayMetrics());
      hpText.setSelectedIndex(2);
    }
    else {
      hpText.setSelectedIndex(0);
    }





    hpText.setChangeListener(new HorizontalPicker.OnSelectionChangeListener() {
      @Override
      public void onItemSelect(HorizontalPicker picker, int i) {
        HorizontalPicker.PickerItem selected = picker.getSelectedItem();
        if (selected.getText().equals("Tamil")) {
          setLocale("ta");
          edit.putString("language_code", "ta").commit();
        } else if (selected.getText().equals("English")) {
          setLocale("en");
          edit.putString("language_code", "en").commit();
        } else {
          setLocale("hi");
          edit.putString("language_code", "hi").commit();
        }
      }
    });



    Part_edit.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Desc_edit.setText(GetDescriptionEachPartno(parent.getItemAtPosition(position).toString()));
        // Toast.makeText(Quick_Order_Activity.this, parent.getItemAtPosition(position).toString(),
        // Toast.LENGTH_LONG).show();
        Quantity_edit.setText("1");
      }

    });


      Part_edit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
          @Override
          public void onFocusChange(View v, boolean hasFocus) {
              if(hasFocus) {
                  if(PartnumberListAuto.size()!=0);
//                  spinnerDialog.showSpinerDialog();
              }
          }
      });



    Part_edit.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        spinnerDialog.showSpinerDialog();
      }
    });


    Quantity_edit.setFilters(new InputFilter[] {new InputFilterMinMax("1", "1000")});
    spinnerDialog=new SpinnerDialog(Quick_Order_Activity.this,"Search part numbers");// With No Animation
    spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
      @Override
      public void onClick(String item, int position) {
       // Toast.makeText(Quick_Order_Activity.this, item + "  " + position+"", Toast.LENGTH_SHORT).show();
        Desc_edit.setText(GetDescriptionEachPartno(item));
        Part_edit.setText(item);
        Quantity_edit.setText("1");
      }
    });

    Add_btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (Part_edit.getText().length() == 0)
        {
          StyleableToast st = new StyleableToast(Quick_Order_Activity.this,
              getResources().getString(R.string.enter_part_number), Toast.LENGTH_SHORT);
          st.setBackgroundColor(getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();
        }
        else if(Desc_edit.getText().length() == 0)
        {
          StyleableToast st = new StyleableToast(Quick_Order_Activity.this,
                  getString(R.string.enter_valid_part_number), Toast.LENGTH_SHORT);
          st.setBackgroundColor(getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();
        }

        else if (Quantity_edit.getText().length() == 0) {
          StyleableToast st = new StyleableToast(Quick_Order_Activity.this,
              getResources().getString(R.string.enter_quantity), Toast.LENGTH_SHORT);
          st.setBackgroundColor(getResources().getColor(R.color.red));
          st.setTextColor(Color.WHITE);
          st.setMaxAlpha();
          st.show();
        }
        else {
          AddQuickOrderItems(Part_edit.getText().toString(), Quantity_edit.getText().toString());
        }
      }
    });

    Preview_btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        if(Part_edit.getText().length() != 0 && Quantity_edit.getText().length() != 0) {
          AddQuickOrderItems(Part_edit.getText().toString(), Quantity_edit.getText().toString());
        }
       /* if (dealer_Address.getText().length() == 0) {
          final AlertDialog.Builder alertDialog =
              new AlertDialog.Builder(Quick_Order_Activity.this);
          alertDialog.setTitle("Wabco");
          alertDialog.setMessage(R.string.delivery_address_alert);
          alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
              startActivity(new Intent(Quick_Order_Activity.this, EditProfileActivity.class).putExtra("from","quickorder"));
            }
          });
          alertDialog.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {}
          });
          alertDialog.show();
        } else {
          startActivity(new Intent(Quick_Order_Activity.this, Quick_Order_Preview_Activity.class).putExtra("from","quickorder"));
        }*/
        startActivity(new Intent(Quick_Order_Activity.this, Quick_Order_Preview_Activity.class).putExtra("from","quickorder"));

      }
    });



    this.back = (ImageView) findViewById(R.id.back);
    this.back.setOnClickListener(new View.OnClickListener() {


      public void onClick(View arg0) {
        onBackPressed();
      }
    });
    final ImageView menu = (ImageView) findViewById(R.id.menu);
    menu.setOnClickListener(new View.OnClickListener() {
      public void onClick(View v) {
        Context wrapper = new ContextThemeWrapper(Quick_Order_Activity.this, R.style.PopupMenu);
        final PopupMenu pop = new PopupMenu(wrapper, v);
        pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

          public boolean onMenuItemClick(MenuItem item) {
            switch (item.getItemId()) {
              case R.id.home:
                startActivity(new Intent(Quick_Order_Activity.this, MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
              case R.id.search:
                startActivity(new Intent(Quick_Order_Activity.this, SearchActivity.class));
                break;
              case R.id.notification:
                startActivity(new Intent(Quick_Order_Activity.this, NotificationActivity.class));
                break;
              case R.id.vehicle:
                startActivity(new Intent(Quick_Order_Activity.this, VehicleMakeActivity.class));
                break;
              case R.id.product:
                startActivity(new Intent(Quick_Order_Activity.this, ProductFamilyActivity.class));
                break;
              case R.id.performance:
                startActivity(new Intent(Quick_Order_Activity.this, PE_Kit_Activity.class));
                break;
              case R.id.contact:
                startActivity(new Intent(Quick_Order_Activity.this, Network_Activity.class));
                break;
              case R.id.askwabco:
                startActivity(new Intent(Quick_Order_Activity.this, AskWabcoActivity.class));
                break;
              case R.id.pricelist:
                startActivity(new Intent(Quick_Order_Activity.this, PriceListActivity.class));
                break;
              case R.id.update:
                WabcoUpdate update = new WabcoUpdate(Quick_Order_Activity.this);
                update.checkVersion();
                break;
            }
            return false;
          }
        });
        pop.setOnDismissListener(new PopupMenu.OnDismissListener() {

          @Override
          public void onDismiss(PopupMenu arg0) {
            // TODO Auto-generated method stub
            pop.dismiss();
          }
        });

        pop.inflate(R.menu.main);
        pop.show();
      }
    });




  }

  private void refresh() {
    recreate();
  }


  class GetPartNoAutocomplete extends AsyncTask<String, Void, String> {
    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      PartnumberListAuto.clear();
    }

    protected String doInBackground(String... type) {
      SearchDAO searchDAO = new SearchDAO(Quick_Order_Activity.this);
      SearchDTO searchDTO = new SearchDTO();
      searchDTO = searchDAO.PricePartNoListForQuickOrder();
      PartnumberListAuto = searchDTO.getAutocompletepartnoList();
      return "success";
    }

    protected void onPostExecute(String result) {
      super.onPostExecute(result);

     /* ArrayAdapter<String> adapter = new ArrayAdapter<String>(Quick_Order_Activity.this,
          R.layout.simple_spinner_item, PartnumberListAuto);
      Part_edit.setCompletionHint("Part numbers");
      Part_edit.setThreshold(0);
      Part_edit.setAdapter(adapter);*/

      AutocompleteAdapter adapter = new AutocompleteAdapter(Quick_Order_Activity.this, R.layout.simple_spinner_item, PartnumberListAuto);
      Part_edit.setThreshold(0);
      Part_edit.setAdapter(adapter);

      spinnerDialog.SetItems(PartnumberListAuto);


    }
  }


  private ArrayList<SampleModel> CreatePartNumberList()
  {
    ArrayList<SampleModel> items = new ArrayList<>();
    for(int i=0;i>PartnumberListAuto.size();i++)
    {
      items.add(new SampleModel(PartnumberListAuto.get(i)));
    }


    return items;
  }


  public String GetDescriptionEachPartno(final String partno) {

    SearchDAO searchDAO = new SearchDAO(Quick_Order_Activity.this);
    return searchDAO.PriceDescriptionForQuickOrder(partno);

  }

  private void AddQuickOrderItems(String partno, String qty) {
    CartDTO cartDTO = new CartDTO();
    SearchDAO searchDAO = new SearchDAO(Quick_Order_Activity.this);
    cartDTO = searchDAO.retriveForPriceQuickOrder(partno);
    CartDAO cartDAO = new CartDAO(Quick_Order_Activity.this);
    long success = cartDAO.addQuickOrder(cartDTO, Integer.parseInt(qty));
    if (success != -1) {
      StyleableToast st = new StyleableToast(Quick_Order_Activity.this, getString(R.string.part_added), Toast.LENGTH_SHORT);
      st.setBackgroundColor(getResources().getColor(R.color.green));
      st.setTextColor(Color.WHITE);
      st.setMaxAlpha();
      st.show();
      Part_edit.setText("");
      Quantity_edit.setText("");
      Desc_edit.setText("");
    } else {
      StyleableToast st = new StyleableToast(Quick_Order_Activity.this,
          " Part item already added ! ", Toast.LENGTH_SHORT);
      st.setBackgroundColor(getResources().getColor(R.color.red));
      st.setTextColor(Color.WHITE);
      st.setMaxAlpha();
      st.show();
    }

  }



  public void setLocale(String lang) {
    this.lang = lang;
    myLocale = new Locale(lang);
    Resources res = getResources();
    DisplayMetrics dm = res.getDisplayMetrics();
    Configuration conf = res.getConfiguration();
    conf.setLocale(new Locale(lang.toLowerCase()));
    res.updateConfiguration(conf, dm);
    refresh();
    // Intent refresh = new Intent(this, Quick_Order_Activity.class);
    // startActivity(refresh);
  }


}
